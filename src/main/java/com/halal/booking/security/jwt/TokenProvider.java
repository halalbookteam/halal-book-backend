package com.halal.booking.security.jwt;

import com.halal.booking.property.TokenProperties;
import io.jsonwebtoken.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Base64;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

@Component
public class TokenProvider {
    private final HttpServletRequest request;

    private final Logger log = LoggerFactory.getLogger(TokenProvider.class);

    private static final String AUTHORITIES_KEY = "auth";
    private static final String TIME_KEY = "time";

    private final Base64.Encoder encoder = Base64.getEncoder();

    private String secretKey;

    private final TokenProperties tokenProperties;

    @Autowired
    public TokenProvider(HttpServletRequest request, TokenProperties tokenProperties) {
        this.request = request;
        this.tokenProperties = tokenProperties;
    }

    @PostConstruct
    public void init() {
        this.secretKey = encoder.encodeToString(tokenProperties.getJwtSecret().getBytes(StandardCharsets.UTF_8));
    }

    public String createToken(Authentication authentication) {
        String authorities = authentication.getAuthorities().stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.joining(","));

        long now = System.currentTimeMillis();
        Date validity;
        validity = new Date(now + this.tokenProperties.getTokenValidityInSeconds() * 1000);

        return Jwts.builder()
            .setSubject(authentication.getName())
            .claim(AUTHORITIES_KEY, authorities)
            .signWith(SignatureAlgorithm.HS512, secretKey)
            .setExpiration(validity)
            .compact();
    }

    public Authentication getAuthentication(String token) {
        Claims claims = parseToken(token);

        Collection<? extends GrantedAuthority> authorities =
            Arrays.stream(claims.get(AUTHORITIES_KEY).toString().split(","))
                .map(SimpleGrantedAuthority::new)
                .collect(Collectors.toList());

        User principal = new User(claims.getSubject(), "", authorities);

        return new UsernamePasswordAuthenticationToken(principal, token, authorities);
    }

    public Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(secretKey)
                .parseClaimsJws(token)
                .getBody();
    }

    public String resolveToken(HttpServletRequest request){
        String bearerToken = request.getHeader(JWTConfigurer.AUTHORIZATION_HEADER);
        if (StringUtils.hasText(bearerToken) && bearerToken.startsWith("Bearer ")) {
            return bearerToken.substring(7);
        }
        return null;
    }

    public String getCurrentUserEmail() {
        String token = this.resolveToken(request);
        return token == null ? null : parseToken(token).getSubject();
    }

    public boolean validateToken(String authToken) {
        try {
            Jwts.parser().setSigningKey(secretKey).parseClaimsJws(authToken);
            return true;
        } catch (SignatureException e) {
            log.info("Invalid JWT signature.");
            log.trace("Invalid JWT signature trace: {}", e);
        } catch (MalformedJwtException e) {
            log.info("Invalid JWT sourceId.");
            log.trace("Invalid JWT sourceId trace: {}", e);
        } catch (ExpiredJwtException e) {
            log.info("Expired JWT sourceId.");
            log.trace("Expired JWT sourceId trace: {}", e);
        } catch (UnsupportedJwtException e) {
            log.info("Unsupported JWT sourceId.");
            log.trace("Unsupported JWT sourceId trace: {}", e);
        } catch (IllegalArgumentException e) {
            log.info("JWT sourceId compact of handler are invalid.");
            log.trace("JWT sourceId compact of handler are invalid trace: {}", e);
        }
        return false;
    }

    public String createActivationToken(String email) {
        return Jwts.builder()
                .setSubject(email)
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .compact();
    }

    public String resolveActivationToken(String ticket) {
        return this.parseToken(ticket).getSubject();
    }

    public String createRecoverPasswordToken(com.halal.booking.usermanagement.model.User user) {
        return Jwts.builder()
                .setSubject(user.getEmail())
                .claim(TIME_KEY, user.getForgotPasswordLinkTime())
                .signWith(SignatureAlgorithm.HS512, secretKey)
                .setExpiration(new Date(System.currentTimeMillis() + 1000 * tokenProperties.getTokenValidityInSecondsForForgotPassword()))
                .compact();
    }

    public Long getTime(Claims claims) {
        return (Long) claims.get(TIME_KEY);
    }
}
