package com.halal.booking.external.payment.stripe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.halal.booking.model.entity.AbstractModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "stripe_source")
@Builder
public class StripeSource extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private StripeCustomer customer;

    private String sourceId;

    private String brand;

    private String last4;

    private int expirationMonth;

    private int expirationYear;

    private boolean active = true;

    @JsonProperty(value = "default")
    private boolean isDefault;
}
