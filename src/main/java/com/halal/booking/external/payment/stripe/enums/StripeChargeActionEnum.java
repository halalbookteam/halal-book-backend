package com.halal.booking.external.payment.stripe.enums;

public enum StripeChargeActionEnum {
    AUTHORIZE,
    CAPTURE,
    REFUND,
    PARTIALLY_REFUND,
    FAILED
}
