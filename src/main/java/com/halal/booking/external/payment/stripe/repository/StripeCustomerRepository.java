package com.halal.booking.external.payment.stripe.repository;

import com.halal.booking.external.payment.stripe.entity.StripeCustomer;
import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StripeCustomerRepository extends JpaRepository<StripeCustomer, Long> {
    StripeCustomer findByUser(User user);
}
