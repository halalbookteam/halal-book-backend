package com.halal.booking.external.payment.stripe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.external.payment.stripe.enums.StripeChargeActionEnum;
import com.halal.booking.model.entity.AbstractModel;
import com.halal.booking.model.entity.RoomReservation;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "stripe_charge")
@Builder
public class StripeCharge extends AbstractModel {
    @ManyToOne
    private StripeSource source;

    private String chargeId;

    private String currency;

    private BigDecimal amount;

    private BigDecimal refundedAmount;

    @OneToMany(mappedBy = "charge", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<StripeChargeAction> chargeActionList;

    @OneToMany
    private List<RoomReservation> roomReservationList;

    @Enumerated(EnumType.STRING)
    private StripeChargeActionEnum state;

    @JsonIgnore
    public boolean isFailed() {
        return this.state != null && this.state.equals(StripeChargeActionEnum.FAILED);
    }

    @JsonIgnore
    public StripeChargeAction getLastSuccessfulAction() {
        return Optional.ofNullable(chargeActionList).map(stripeChargeActions -> stripeChargeActions.stream()
                .filter(StripeChargeAction::isSuccessful)
                .max(Comparator.naturalOrder())
                .orElse(null))
            .orElse(null);
    }
}
