package com.halal.booking.external.payment.stripe.repository;

import com.halal.booking.external.payment.stripe.entity.StripeSource;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StripeSourceRepository extends JpaRepository<StripeSource, Long> {
}
