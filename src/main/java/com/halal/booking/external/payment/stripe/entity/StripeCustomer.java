package com.halal.booking.external.payment.stripe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.model.entity.AbstractModel;
import com.halal.booking.usermanagement.model.User;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "stripe_customer")
@Builder
public class StripeCustomer extends AbstractModel {
    @JsonIgnore
    @OneToOne
    private User user;

    @Column(nullable = false)
    private String email;

    @Column(nullable = false)
    private String customerId;

    @OneToMany(mappedBy = "customer")
    @OrderBy("isDefault desc, id asc")
    private List<StripeSource> sources;
}
