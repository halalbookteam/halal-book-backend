package com.halal.booking.external.payment.stripe.repository;

import com.halal.booking.external.payment.stripe.entity.StripeCharge;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StripeChargeRepository extends JpaRepository<StripeCharge, Long> {
}
