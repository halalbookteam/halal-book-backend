package com.halal.booking.external.payment.stripe.service;

import com.halal.booking.exception.BookingException;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.external.payment.stripe.entity.StripeCharge;
import com.halal.booking.external.payment.stripe.entity.StripeChargeAction;
import com.halal.booking.external.payment.stripe.entity.StripeCustomer;
import com.halal.booking.external.payment.stripe.entity.StripeSource;
import com.halal.booking.external.payment.stripe.enums.StripeChargeActionEnum;
import com.halal.booking.external.payment.stripe.repository.StripeChargeRepository;
import com.halal.booking.external.payment.stripe.repository.StripeCustomerRepository;
import com.halal.booking.external.payment.stripe.repository.StripeSourceRepository;
import com.halal.booking.model.dto.Price;
import com.halal.booking.usermanagement.model.User;
import com.stripe.Stripe;
import com.stripe.exception.CardException;
import com.stripe.exception.StripeException;
import com.stripe.model.Card;
import com.stripe.model.Charge;
import com.stripe.model.Customer;
import com.stripe.model.Refund;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.math.BigDecimal;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

@Service
public class StripeService {
    @Value("${external.payment.stripe.apiKey}")
    private String apiKey;

    private StripeCustomerRepository customerRepository;
    private StripeSourceRepository sourceRepository;
    private StripeChargeRepository chargeRepository;

    @Autowired
    public StripeService(StripeCustomerRepository customerRepository, StripeSourceRepository sourceRepository, StripeChargeRepository chargeRepository) {
        this.customerRepository = customerRepository;
        this.sourceRepository = sourceRepository;
        this.chargeRepository = chargeRepository;
    }

    @PostConstruct
    private void init() {
        Stripe.apiKey = this.apiKey;
    }

    private Charge charge(BigDecimal amount, String currency, String sourceId, String customerId, String receiptEmail, boolean capture) throws StripeException {
        Map<String, Object> params = new HashMap<>();
        params.put("amount", amount.multiply(new BigDecimal(100)).toBigInteger());
        params.put("currency", currency);
        if(sourceId != null) {
            params.put("source", sourceId);
        }
        if(receiptEmail != null) {
            params.put("receipt_email", receiptEmail);
        }
        if(customerId != null) {
            params.put("customer", customerId);
        }
        if(!capture) {
            params.put("capture", false);
        }
        return Charge.create(params);
    }

    public void refund(StripeCharge stripeCharge) {
        Map<String, Object> refundParams = new HashMap<>();
        refundParams.put("charge", stripeCharge.getChargeId());

        StripeChargeAction stripeChargeAction = StripeChargeAction.builder()
                .time(new Date())
                .action(StripeChargeActionEnum.REFUND)
                .charge(stripeCharge)
                .build();
        stripeCharge.getChargeActionList().add(stripeChargeAction);

        try {
            Refund refund = Refund.create(refundParams);
            stripeChargeAction.setAmount(new BigDecimal(refund.getAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
            stripeCharge.setState(StripeChargeActionEnum.REFUND);
        } catch (StripeException e) {
            stripeChargeAction.setErrorCode(e.getCode());
            throw new BookingRuntimeException(e, e.getCode());
        }
    }

    public void capture(StripeCharge stripeCharge) {
        this.capture(stripeCharge, null);
    }

    public void capture(StripeCharge stripeCharge, BigDecimal amount) {
        StripeChargeAction stripeChargeAction = StripeChargeAction.builder()
                .time(new Date())
                .action(StripeChargeActionEnum.CAPTURE)
                .charge(stripeCharge)
                .build();
        stripeCharge.getChargeActionList().add(stripeChargeAction);

        try {
            Charge charge = Charge.retrieve(stripeCharge.getChargeId());
            Map<String, Object> captureParams = null;
            if(amount != null) {
                captureParams = new HashMap<>();
                captureParams.put("amount", stripeCharge.getChargeId());
            }
            Charge capture = charge.capture(captureParams);
            stripeCharge.setState(amount == null ? StripeChargeActionEnum.CAPTURE : StripeChargeActionEnum.PARTIALLY_REFUND);
            stripeChargeAction.setAmount(new BigDecimal(capture.getAmount()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
            stripeCharge.setRefundedAmount(new BigDecimal(capture.getAmountRefunded()).divide(new BigDecimal(100), 2, BigDecimal.ROUND_HALF_UP));
        } catch (StripeException e) {
            stripeChargeAction.setErrorCode(e.getCode());
            throw new BookingRuntimeException(e, e.getCode());
        }
    }

    public StripeCharge charge(Price price, StripeSource source, boolean capture) {
        StripeChargeAction stripeChargeAction = StripeChargeAction.builder()
                .action(capture ? StripeChargeActionEnum.CAPTURE : StripeChargeActionEnum.AUTHORIZE)
                .amount(price.getAmount())
                .time(new Date())
                .build();

        StripeCharge stripeCharge = StripeCharge.builder()
                .amount(price.getAmount())
                .refundedAmount(BigDecimal.ZERO)
                .source(source)
                .currency(price.getCurrency())
                .chargeActionList(Collections.singletonList(stripeChargeAction))
                .build();

        stripeChargeAction.setCharge(stripeCharge);
        stripeCharge.setState(StripeChargeActionEnum.FAILED);
        try {
            Charge charge = this.charge(price.getAmount(), price.getCurrency(), source.getSourceId(), source.getCustomer().getCustomerId(), source.getCustomer().getEmail(), capture);
            stripeCharge.setChargeId(charge.getId());
            stripeCharge.setState(stripeChargeAction.getAction());
        } catch (CardException e) {
            stripeCharge.setChargeId(e.getCharge());
            stripeChargeAction.setErrorCode(e.getCode());
        } catch (StripeException e) {
            stripeChargeAction.setErrorCode(e.getCode());
        }

        this.chargeRepository.save(stripeCharge);
        return stripeCharge;
    }

    private Customer createCustomer(String token, String email) throws BookingException {
        Map<String, Object> chargeParams = new HashMap<>();
        chargeParams.put("source", token);
        chargeParams.put("email", email);
        try {
            return Customer.create(chargeParams);
        } catch (StripeException e) {
            throw new BookingException(e, e.getCode());
        }
    }

    public StripeSource createSourceWithCustomer(String sourceId, String email, User user) throws BookingException {
        Customer customer = this.createCustomer(sourceId, email);
        StripeCustomer stripeCustomer = StripeCustomer.builder()
                .customerId(customer.getId())
                .email(customer.getEmail())
                .user(user)
                .build();
        this.customerRepository.save(stripeCustomer);

        try {
            Card card = (Card) customer.getSources().retrieve(customer.getDefaultSource());
            StripeSource stripeSource = saveSource(stripeCustomer, card);
            stripeSource.setDefault(true);
            return stripeSource;
        } catch (StripeException e) {
            throw new BookingException(e, e.getCode());
        }
    }

    public StripeCustomer getCustomer(User user) {
        return this.customerRepository.findByUser(user);
    }

    public StripeSource getSourceById(Long id) {
        return this.sourceRepository.findById(id).orElse(null);
    }

    public StripeSource createSource(String sourceId, StripeCustomer stripeCustomer) throws BookingException {
        try {
            Customer customer = Customer.retrieve(stripeCustomer.getCustomerId());
            Map<String, Object> params = new HashMap<>();
            params.put("source", sourceId);
            Card card = (Card) customer.getSources().create(params);
            return saveSource(stripeCustomer, card);
        } catch (StripeException e) {
            throw new BookingException(e, e.getCode());
        }
    }

    private StripeSource saveSource(StripeCustomer stripeCustomer, Card card) {
        StripeSource stripeSource = StripeSource.builder()
                .active(true)
                .brand(card.getBrand())
                .customer(stripeCustomer)
                .expirationMonth(card.getExpMonth().intValue())
                .expirationYear(card.getExpYear().intValue())
                .last4(card.getLast4())
                .sourceId(card.getId())
                .build();
        return this.sourceRepository.save(stripeSource);
    }

}
