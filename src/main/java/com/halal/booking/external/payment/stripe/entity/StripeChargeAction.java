package com.halal.booking.external.payment.stripe.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.external.payment.stripe.enums.StripeChargeActionEnum;
import com.halal.booking.model.entity.AbstractModel;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "stripe_charge_action")
@Builder
public class StripeChargeAction extends AbstractModel implements Comparable<StripeChargeAction> {
    @JsonIgnore
    @ManyToOne
    private StripeCharge charge;

    @Enumerated(EnumType.STRING)
    private StripeChargeActionEnum action;

    private BigDecimal amount;

    private Date time;

    private String errorCode;

    @Override
    public int compareTo(StripeChargeAction o) {
        return this.time.compareTo(o.getTime());
    }

    public boolean isSuccessful() {
        return this.errorCode != null;
    }
}
