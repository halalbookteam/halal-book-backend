package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlText;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ErrorDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String code;
    @JacksonXmlText
    private String message;
}
