package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class PerDayRateDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String stayDate;
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal baseRate;
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal hotelServiceFees;

}
