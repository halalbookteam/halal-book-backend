package com.halal.booking.external.channelmanager.reseliva.service;

import com.halal.booking.enums.ChannelManagerEnum;
import com.halal.booking.enums.PropertyStatusEnum;
import com.halal.booking.enums.ReservationStatusEnum;
import com.halal.booking.external.channelmanager.reseliva.dto.*;
import com.halal.booking.external.channelmanager.reseliva.enums.BookTypeEnum;
import com.halal.booking.external.channelmanager.reseliva.exception.ReselivaException;
import com.halal.booking.model.dto.Price;
import com.halal.booking.model.entity.*;
import com.halal.booking.repository.HotelRepository;
import com.halal.booking.repository.RoomRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class ReselivaService {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final SimpleDateFormat DATE_TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");

    @Value("${external.channelManager.reseliva.username}")
    private String username;
    @Value("${external.channelManager.reseliva.password}")
    private String password;
    @Value("${external.channelManager.reseliva.bookingInformURL}")
    private String bookingInformURL;

    private HotelRepository hotelRepository;
    private RoomRateRepository roomRateRepository;

    @Autowired
    public ReselivaService(HotelRepository hotelRepository, RoomRateRepository roomRateRepository) {
        this.hotelRepository = hotelRepository;
        this.roomRateRepository = roomRateRepository;
    }

    public ProductAvailRateRetrievalResponseDTO fetchHotelInfo(AuthenticationDTO authentication, Long hotelId) {
        if (!this.checkAuthentication(authentication)) {
            return ProductAvailRateRetrievalResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Authentication failed").build()
            )).build();
        }
        Hotel hotel = this.hotelRepository.findByIdAndChannelManager(hotelId, ChannelManagerEnum.RESELIVA);
        if (hotel == null) {
            return ProductAvailRateRetrievalResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Hotel not found").build()
            )).build();
        }
        return ProductAvailRateRetrievalResponseDTO.builder()
                .productList(new ProductListDTO(hotel)).build();
    }

    private boolean checkAuthentication(AuthenticationDTO authentication) {
        return this.username.equals(authentication.getUsername()) && this.password.equals(authentication.getPassword());
    }

    @Transactional
    public AvailRateUpdateResponseDTO updateRates(AvailRateUpdateRequestDTO requestDTO) {
        if (!this.checkAuthentication(requestDTO.getAuthentication())) {
            return AvailRateUpdateResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Authentication failed").build()
            )).build();
        }
        Hotel hotel = this.hotelRepository.findByIdAndChannelManager(requestDTO.getHotel().getId(), ChannelManagerEnum.RESELIVA);
        if (hotel == null) {
            return AvailRateUpdateResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Hotel not found").build()
            )).build();
        }
        try {
            for (AvailRateUpdateDTO availRateUpdateDTO : requestDTO.getRateUpdateList()) {
                this.updateRates(hotel, availRateUpdateDTO);
            }
        } catch (ReselivaException e) {
            return AvailRateUpdateResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().code(e.getCode()).message(e.getMessage()).build()
            )).build();
        }
        return AvailRateUpdateResponseDTO.builder().success(true).build();
    }

    private void updateRates(Hotel hotel, AvailRateUpdateDTO availRateUpdateDTO) throws ReselivaException {
        for (RoomTypeDTO roomTypeDTO : availRateUpdateDTO.getRoomTypeList()) {
            Room room = hotel.getRoomById(roomTypeDTO.getId());
            if (room == null) {
                throw new ReselivaException("3203", "The following RoomTypeIDs do not belong to the given hotel: " + roomTypeDTO.getId() + ".");
            }
            if (roomTypeDTO.getRatePlanDTOList() == null || roomTypeDTO.getRatePlanDTOList().size() != 1) {
                throw new ReselivaException("", "Not expected number of rate plans for the room type. RoomTypeId: " + roomTypeDTO.getId() +
                        ". Expected number of rate plan: 1, actual number of rate plan: " + (roomTypeDTO.getRatePlanDTOList() == null ? 0 : roomTypeDTO.getRatePlanDTOList().size()));
            }
            RatePlanDTO ratePlanDTO = roomTypeDTO.getRatePlanDTOList().get(0);
            if (!room.getId().equals(ratePlanDTO.getId())) {
                throw new ReselivaException("3204", "The following RatePlanIDs do not belong to the given hotel: " + ratePlanDTO.getId() + ".");
            }
            DateRangeDTO dateRange = availRateUpdateDTO.getDateRange();
            List<RoomRate> rateList = this.roomRateRepository.findRatesByRoomAndDateBetween(room, dateRange.getFromAsDate(), dateRange.getToAsDate());
            Integer minLOS = ratePlanDTO.getMinLOS();
            Price basePrice = ratePlanDTO.getPrice();
            rateList.forEach(roomRate -> {
                InventoryDTO inventory = roomTypeDTO.getInventory();
                if(inventory != null) {
                    roomRate.setAvailableRoomCount(inventory.getTotalInventoryAvailable());
                }
                if (minLOS != null) {
                    roomRate.setMinNightCount(minLOS);
                }
                if (basePrice != null) {
                    roomRate.setBasePrice(basePrice);
                }
            });
        }
    }

    public ProductAvailRateRetrievalResponseDTO fetchRates(ProductAvailRateRetrievalRequestDTO request) {
        if (!this.checkAuthentication(request.getAuthentication())) {
            return ProductAvailRateRetrievalResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Authentication failed").build()
            )).build();
        }
        Hotel hotel = this.hotelRepository.findById(request.getHotel().getId()).orElse(null);
        if (hotel == null) {
            return ProductAvailRateRetrievalResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().message("Hotel not found").build()
            )).build();
        }
        try {
            List<AvailRateDTO> availRateList = new ArrayList<>();
            for (DateRangeDTO dateRangeDTO : request.getDateRangeList()) {
                Iterator<Date> dates = dateRangeDTO.getDateList().iterator();
                Map<Date, List<RoomRate>> ratesByDate = this.roomRateRepository.findRatesByRoom_HotelAndDateBetweenOrderByDate(hotel, dateRangeDTO.getFromAsDate(), dateRangeDTO.getToAsDate()).stream().collect(Collectors.groupingBy(RoomRate::getDate));
                while (dates.hasNext()) {
                    Date currentDate = dates.next();
                    AvailRateDTO availRateDTO = AvailRateDTO.builder()
                            .date(formatDate(currentDate)).build();
                    List<RoomRate> rateList = ratesByDate.get(currentDate);
                    if (rateList != null && !rateList.isEmpty()) {
                        availRateDTO.setRoomTypeList(rateList.stream().map(roomRate -> {
                            Room room = roomRate.getRoom();
                            Price basePrice = roomRate.getBasePrice();
                            return RoomTypeDTO.builder()
                                    .id(room.getId())
                                    .closed(room.getStatus().equals(PropertyStatusEnum.Passive))
                                    .inventory(InventoryDTO.builder().totalInventoryAvailable(roomRate.getAvailableRoomCount()).build())
                                    .ratePlanDTOList(Collections.singletonList(RatePlanDTO.builder()
                                            .id(room.getId())
                                            .closed(room.getStatus().equals(PropertyStatusEnum.Passive))
                                            .rate(RateDTO.builder()
                                                    .currency(basePrice.getCurrency())
                                                    .perDay(PerDayDTO.builder().rate(basePrice.getAmount()).build())
                                                    .build())
                                            .restrictionList(Collections.singletonList(RestrictionDTO.builder()
                                                    .minLOS(roomRate.getMinNightCount())
                                                    .build()))
                                            .build()))
                                    .build();
                        }).collect(Collectors.toList()));
                    }
                    availRateList.add(availRateDTO);
                }
            }
            return ProductAvailRateRetrievalResponseDTO.builder()
                    .availRateList(availRateList).build();
        } catch (ReselivaException e) {
            return ProductAvailRateRetrievalResponseDTO.builder().errorList(Collections.singletonList(
                    ErrorDTO.builder().code(e.getCode()).message(e.getMessage()).build()
            )).build();
        }
    }

    private String formatDate(Date date) {
        return DATE_FORMAT.format(date);
    }

    private String formatDateTime(Date dateTime) {
        return DATE_TIME_FORMAT.format(dateTime);
    }

    public void sendBooking(Reservation reservation) {
        RestTemplate restTemplate = new RestTemplate();
        BookingRetrievalDTO request = createRequest(reservation);
        System.out.println("***************REQUEST*******************");
        System.out.println(request);
        System.out.println("****************************************");
        BookingRetrievalDTO response = restTemplate.postForEntity(this.bookingInformURL, request, BookingRetrievalDTO.class).getBody();
        System.out.println("***************RESPONSE*******************");
        System.out.println(response);
        System.out.println("****************************************");
    }

    private BookingRetrievalDTO createRequest(Reservation reservation) {
        Map<Date, List<RoomRate>> ratesByDate = this.roomRateRepository.findRatesByRoom_HotelAndDateBetweenOrderByDate(
                reservation.getHotel(), reservation.getCheckIn(), reservation.getLastNight()).stream().collect(Collectors.groupingBy(RoomRate::getDate));
        List<RoomStayDTO> roomStayList = new ArrayList<>();
        reservation.getRoomReservations().forEach(roomReservation -> {
            RoomAlternative alternative = roomReservation.getAlternative();
            RoomStayDTO.RoomStayDTOBuilder roomStayDTOBuilder = RoomStayDTO.builder()
                    .roomTypeID(alternative.getRoomId())
                    .ratePlanID(alternative.getRoomId())
                    .stayDate(StayDateDTO.builder()
                            .arrival(formatDate(reservation.getCheckIn()))
                            .departure(formatDate(reservation.getCheckOut()))
                            .build());
            final BigDecimal[] totalAmount = {BigDecimal.ZERO};
            RoomStayDTO roomStay = reservation.getStatus().equals(ReservationStatusEnum.CANCELLED) ? roomStayDTOBuilder.build() : roomStayDTOBuilder
                    .perDayRates(PerDayRatesDTO.builder()
                            .currency(reservation.getHotel().getCurrency())
                            .perDayRateList(ratesByDate.keySet().stream().map(date -> {
                                BigDecimal amount = ratesByDate.get(date).iterator().next().getBasePrice().getAmount();
                                totalAmount[0] = totalAmount[0].add(amount);
                                return PerDayRateDTO.builder()
                                        .stayDate(formatDate(date))
                                        .baseRate(amount)
                                        .build();
                            }).collect(Collectors.toList()))
                            .build())
                    .totalPrice(TotalPriceDTO.builder()
                            .currency(reservation.getHotel().getCurrency())
                            .amountAfterTaxes(totalAmount[0])
                            .amountOfTaxes(BigDecimal.ZERO)
                            .build())
                    .build();
            roomStayList.add(roomStay);
        });
        BookingDTO.BookingDTOBuilder bookingDTOBuilder = BookingDTO.builder()
                .id(reservation.getId())
                .hotelDTO(HotelDTO.builder()
                        .id(reservation.getHotelId()).build())
                .roomStayList(roomStayList);
        BookingDTO booking = reservation.getStatus().equals(ReservationStatusEnum.CANCELLED) ? bookingDTOBuilder.build() : bookingDTOBuilder
                .primaryGuest(GuestDTO.builder()
                        .name(NameDTO.builder()
                                .givenName(reservation.getPerson().getName())
                                .surname(reservation.getPerson().getLastName())
                                .build())
                        .phone(PhoneDTO.builder()
                                .number(reservation.getPerson().getPhoneNumber())
                                .build())
                        .email(reservation.getPerson().getEmail())
                        .build())
                .specialRequest(reservation.getSpecialRequests())
                .build();
        if (reservation.getStatus().equals(ReservationStatusEnum.CANCELLED)) {
            booking.setCancelDateTime(formatDateTime(reservation.getCancellationTime()));
            booking.setType(BookTypeEnum.Cancel);
        }
        else {
            booking.setCreateDateTime(formatDateTime(reservation.getCreationTime()));
            booking.setType(BookTypeEnum.Book);
        }
        return BookingRetrievalDTO.builder()
                .bookingList(Collections.singletonList(booking)).build();
    }
}
