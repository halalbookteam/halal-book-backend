package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class AvailRateDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String date;
    @JacksonXmlProperty(localName = "RoomType")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<RoomTypeDTO> roomTypeList;
}
