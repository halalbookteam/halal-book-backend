package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.math.BigDecimal;

@Data
@Builder
public class TotalPriceDTO {
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal amountAfterTaxes;
    @JacksonXmlProperty(isAttribute = true)
    private BigDecimal amountOfTaxes;
    @JacksonXmlProperty(isAttribute = true)
    private String currency;
}
