package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
@Builder
@JsonRootName("AvailRateUpdateRQ")
public class AvailRateUpdateRequestDTO {
    @JsonProperty("Authentication")
    @Valid
    private AuthenticationDTO authentication;
    @JsonProperty("Hotel")
    @Valid
    private HotelDTO hotel;
    @JacksonXmlProperty(localName = "AvailRateUpdate")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<AvailRateUpdateDTO> rateUpdateList;
}
