package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
@JsonRootName("Authentication")
public class AuthenticationDTO {
    @NotNull
    private String username;
    @NotNull
    private String password;
}
