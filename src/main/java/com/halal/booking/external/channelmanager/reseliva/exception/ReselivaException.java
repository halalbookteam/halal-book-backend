package com.halal.booking.external.channelmanager.reseliva.exception;

public class ReselivaException extends Throwable {
    private String code;
    private String message;

    public ReselivaException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
