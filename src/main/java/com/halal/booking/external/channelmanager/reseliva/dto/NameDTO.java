package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NameDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String givenName;
    @JacksonXmlProperty(isAttribute = true)
    private String surname;
}
