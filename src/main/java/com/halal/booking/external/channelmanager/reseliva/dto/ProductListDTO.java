package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.halal.booking.model.entity.Hotel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
@Builder
@AllArgsConstructor
@JsonRootName("ProductList")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductListDTO {
    @JsonProperty("Hotel")
    private HotelDTO hotel;
    @JacksonXmlProperty(localName = "RoomType")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<RoomTypeDTO> roomTypeList;

    public ProductListDTO(Hotel hotel) {
        this.hotel = HotelDTO.builder()
                .id(hotel.getId())
                .currency(hotel.getCurrency())
                .pricingModel("PerRoom")
                .build();
        this.roomTypeList = hotel.getRooms().stream().map(RoomTypeDTO::new).collect(Collectors.toList());
    }
}
