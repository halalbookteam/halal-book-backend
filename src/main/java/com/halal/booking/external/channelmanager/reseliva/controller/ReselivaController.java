package com.halal.booking.external.channelmanager.reseliva.controller;

import com.halal.booking.external.channelmanager.reseliva.dto.*;
import com.halal.booking.external.channelmanager.reseliva.service.ReselivaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "api/reseliva", produces = "application/xml")
public class ReselivaController {
    private ReselivaService reselivaService;

    @Autowired
    public ReselivaController(ReselivaService reselivaService) {
        this.reselivaService = reselivaService;
    }

    @PostMapping("fetchHotelDetail")
    public ProductAvailRateRetrievalResponseDTO fetchHotelDetail(@RequestBody ProductAvailRateRetrievalRequestDTO request) {
        return reselivaService.fetchHotelInfo(request.getAuthentication(), request.getHotel().getId());
    }

    @PostMapping("updateRates")
    public AvailRateUpdateResponseDTO updateRates(@RequestBody AvailRateUpdateRequestDTO request) {
        return reselivaService.updateRates(request);
    }

    @PostMapping("fetchRates")
    public ProductAvailRateRetrievalResponseDTO fetchRates(@RequestBody ProductAvailRateRetrievalRequestDTO request) {
        return reselivaService.fetchRates(request);
    }

    @PostMapping("testBookings")
    public BookingRetrievalDTO fetchRates(@RequestBody BookingRetrievalDTO request) {
        return request;
    }
}
