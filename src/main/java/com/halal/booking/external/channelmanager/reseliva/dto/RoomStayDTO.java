package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoomStayDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Long roomTypeID;
    @JacksonXmlProperty(isAttribute = true)
    private Long ratePlanID;
    @JsonProperty("StayDate")
    private StayDateDTO stayDate;
    @JsonProperty("GuestCount")
    private GuestCountDTO guestCount;
    @JsonProperty("PerDayRates")
    private PerDayRatesDTO perDayRates;
    @JsonProperty("Total")
    private TotalPriceDTO totalPrice;
}
