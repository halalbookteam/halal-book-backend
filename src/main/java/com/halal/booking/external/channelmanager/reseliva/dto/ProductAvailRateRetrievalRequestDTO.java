package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("ProductAvailRateRetrievalRQ")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductAvailRateRetrievalRequestDTO {
    @JsonProperty("Authentication")
    @Valid
    private AuthenticationDTO authentication;
    @JsonProperty("Hotel")
    @Valid
    private HotelDTO hotel;
    @JacksonXmlElementWrapper(localName = "ParamSet")
    @JacksonXmlProperty(localName = "AvailRateRetrieval")
    private List<DateRangeDTO> dateRangeList;
}
