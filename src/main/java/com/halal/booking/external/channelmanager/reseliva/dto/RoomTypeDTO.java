package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.halal.booking.model.entity.Room;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RoomTypeDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Long id;
    @JacksonXmlProperty(isAttribute = true)
    private String code;
    @JacksonXmlProperty(isAttribute = true)
    private String name;
    @JacksonXmlProperty(isAttribute = true)
    private String status;
    @JacksonXmlProperty(isAttribute = true)
    private Integer capacity;
    @JacksonXmlProperty(localName = "capacity_child", isAttribute = true)
    private Integer capacityChild;
    @JacksonXmlProperty(localName = "RatePlan")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<RatePlanDTO> ratePlanDTOList;
    @JacksonXmlProperty(isAttribute = true)
    private Boolean closed;
    @JacksonXmlProperty(localName = "Inventory", isAttribute = true)
    private InventoryDTO inventory;

    public RoomTypeDTO(Room room) {
        String name = room.getDefaultTranslation().getName();
        final int[] maxAdult = {0};
        final int[] maxChild = {0};
        this.id = room.getId();
        this.code = name;
        this.name = name;
        this.status = room.getStatus().name();
        this.ratePlanDTOList = new ArrayList<>();
        this.ratePlanDTOList.add(RatePlanDTO.builder().id(room.getId()).name(name).code(name).build());
        room.getAlternatives().forEach(alternative -> {
            maxAdult[0] = Integer.max(alternative.getMaxAdult(), maxAdult[0]);
            maxChild[0] = Integer.max(alternative.getMaxChild(), maxChild[0]);
        });
        this.capacity = maxAdult[0];
        this.capacityChild = maxChild[0];
    }

    @JsonIgnore
    public boolean isRoomClosed() {
        return this.closed != null && this.closed;
    }
}
