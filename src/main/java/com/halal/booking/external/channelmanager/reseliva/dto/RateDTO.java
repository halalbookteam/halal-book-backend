package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RateDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String currency;
    @JsonProperty("PerDay")
    private PerDayDTO perDay;
}
