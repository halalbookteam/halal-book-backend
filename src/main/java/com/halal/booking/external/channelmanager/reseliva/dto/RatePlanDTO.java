package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.halal.booking.model.dto.Price;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RatePlanDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Long id;
    @JacksonXmlProperty(isAttribute = true)
    private String code;
    @JacksonXmlProperty(isAttribute = true)
    private String name;
    @JacksonXmlProperty(isAttribute = true)
    private Boolean closed;
    @JsonProperty("Rate")
    private RateDTO rate;
    @JacksonXmlProperty(localName = "Restrictions")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<RestrictionDTO> restrictionList;

    @JsonIgnore
    public Integer getMinLOS() {
        return this.restrictionList == null ? null : this.restrictionList.stream()
                .filter(restrictionDTO -> restrictionDTO.getMinLOS() != null)
                .map(RestrictionDTO::getMinLOS).findFirst().orElse(null);
    }

    @JsonIgnore
    public Price getPrice() {
        if(this.rate == null) {
            return null;
        }
        return Price.builder().currency(this.rate.getCurrency()).amount(this.rate.getPerDay().getRate()).build();
    }
}
