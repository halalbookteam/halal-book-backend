package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonRootName("AvailRateUpdateRS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class AvailRateUpdateResponseDTO {
    @JsonProperty("Success")
    private Boolean success;
    @JacksonXmlProperty(localName = "Error")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ErrorDTO> errorList;
}
