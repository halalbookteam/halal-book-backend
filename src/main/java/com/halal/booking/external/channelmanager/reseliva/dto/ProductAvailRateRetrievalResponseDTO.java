package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonRootName("ProductAvailRateRetrievalRS")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProductAvailRateRetrievalResponseDTO {
    @JsonProperty("ProductList")
    private ProductListDTO productList;
    @JacksonXmlProperty(localName = "Error")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<ErrorDTO> errorList;
    @JacksonXmlProperty(localName = "AvailRate")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<AvailRateDTO> availRateList;
}
