package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class GuestDTO {
    @JsonProperty("Name")
    private NameDTO name;
    @JsonProperty("Phone")
    private PhoneDTO phone;
    @JsonProperty("Email")
    private String email;
}
