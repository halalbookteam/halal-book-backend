package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.external.channelmanager.reseliva.exception.ReselivaException;
import lombok.Builder;
import lombok.Data;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Data
@Builder
public class DateRangeDTO {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private String from;
    private String to;

    public Date getFromAsDate() throws ReselivaException {
        return getAsDate(this.from);
    }

    public Date getToAsDate() throws ReselivaException {
        return getAsDate(this.to);
    }

    private Date getAsDate(String dateStr) throws ReselivaException {
        try {
            return DATE_FORMAT.parse(dateStr);
        } catch (ParseException e) {
            throw new ReselivaException("", "Date range couldn't be parsed");
        }
    }

    @JsonIgnore
    public List<Date> getDateList() throws ReselivaException {
        Date toDate = this.getToAsDate();
        int dayInMillis = 24 * 60 * 60 * 1000;
        List<Date> list = new ArrayList<>();
        for (Date currentDate = this.getFromAsDate(); !currentDate.after(toDate); currentDate = new Date(currentDate.getTime() + dayInMillis)) {
            list.add(currentDate);
        }
        return list;
    }
}
