package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PerDayRatesDTO {
    @JacksonXmlProperty(isAttribute = true)
    private String currency;
    @JacksonXmlProperty(localName = "PerDayRate")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<PerDayRateDTO> perDayRateList;
}
