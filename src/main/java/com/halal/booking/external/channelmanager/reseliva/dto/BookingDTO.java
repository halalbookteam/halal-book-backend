package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import com.halal.booking.external.channelmanager.reseliva.enums.BookTypeEnum;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BookingDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Long id;
    @JacksonXmlProperty(isAttribute = true)
    private BookTypeEnum type;
    @JacksonXmlProperty(isAttribute = true)
    private String createDateTime;
    @JacksonXmlProperty(isAttribute = true)
    private String modifyDateTime;
    @JacksonXmlProperty(isAttribute = true)
    private String cancelDateTime;
    @JsonProperty("Hotel")
    private HotelDTO hotelDTO;
    @JacksonXmlProperty(localName = "RoomStay")
    @JacksonXmlElementWrapper(useWrapping = false)
    private List<RoomStayDTO> roomStayList;
    @JsonProperty("PrimaryGuest")
    private GuestDTO primaryGuest;
    @JsonProperty("SpecialRequest")
    private String specialRequest;
}
