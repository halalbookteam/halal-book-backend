package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InventoryDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Integer totalInventoryAvailable;
}
