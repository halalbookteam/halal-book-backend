package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonRootName;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlElementWrapper;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
@JsonRootName("BookingRetrievalRS")
public class BookingRetrievalDTO {
    @JacksonXmlProperty(localName = "Booking")
    @JacksonXmlElementWrapper(localName = "Bookings")
    private List<BookingDTO> bookingList;
}
