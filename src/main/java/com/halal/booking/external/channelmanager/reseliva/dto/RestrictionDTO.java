package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class RestrictionDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Integer minLOS;
    @JacksonXmlProperty(isAttribute = true)
    private Integer maxLOS;
    @JacksonXmlProperty(isAttribute = true)
    private Boolean closedToArrival;
    @JacksonXmlProperty(isAttribute = true)
    private Boolean closedToDeparture;
}
