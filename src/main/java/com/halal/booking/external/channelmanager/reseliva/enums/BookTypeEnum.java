package com.halal.booking.external.channelmanager.reseliva.enums;

public enum BookTypeEnum {
    Book,
    Modify,
    Cancel
}
