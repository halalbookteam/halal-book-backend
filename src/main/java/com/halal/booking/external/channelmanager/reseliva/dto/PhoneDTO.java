package com.halal.booking.external.channelmanager.reseliva.dto;

import com.fasterxml.jackson.dataformat.xml.annotation.JacksonXmlProperty;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PhoneDTO {
    @JacksonXmlProperty(isAttribute = true)
    private Integer countryCode;
    @JacksonXmlProperty(isAttribute = true)
    private Integer cityAreaCode;
    @JacksonXmlProperty(isAttribute = true)
    private String number;
}
