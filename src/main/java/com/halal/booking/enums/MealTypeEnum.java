package com.halal.booking.enums;

public enum MealTypeEnum {
    BREAKFAST,
    LUNCH,
    DINNER
}
