package com.halal.booking.enums;

public enum ActionTypeEnum {
    RESERVATION_STATE_CHANGED,
    RESERVATION_MODIFIED,
    REVIEW_CHANGED,
}
