package com.halal.booking.enums;

import java.util.Arrays;

public enum MailTemplateParameterTypeEnum {
    TEXT,
    HTML;

    public static MailTemplateParameterTypeEnum find(String str) {
        return Arrays.stream(values()).filter(type -> type.toString().toLowerCase().equals(str.toLowerCase())).findFirst().orElse(null);
    }
}
