package com.halal.booking.enums;

public enum MealOptionTypeEnum {
    NOTHING_INCLUDED,
    ALL_INCLUDED,
    OPTIONAL,
}
