package com.halal.booking.enums;

public enum ReviewStatusEnum {
    PENDING,
    ACCEPTED,
    REJECTED
}
