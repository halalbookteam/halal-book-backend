package com.halal.booking.enums;

public enum ReservationStatusEnum {
    APPLIED,
    CANCELLED,
    CONFIRMED,
    CLOSED,
}
