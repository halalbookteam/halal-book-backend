package com.halal.booking.enums;

public enum RecipientEnum {
    NEWSLETTER_SUBSCRIBER,
    ADMIN,
    HOTEL_OWNER,
    DEFINED_USER
}
