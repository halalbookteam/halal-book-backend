package com.halal.booking.enums;

public enum PermissionEnum {
    ALL_HOTELS_CRUD,
    ALL_RESERVATIONS_CRUD,
    HOTEL_RESERVATIONS_CRUD,
}
