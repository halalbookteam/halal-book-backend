package com.halal.booking.enums;

public enum UserTypeEnum {
    SYSTEM,
    ANONYMOUS,
    REAL
}
