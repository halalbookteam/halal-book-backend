package com.halal.booking.enums;

public enum PaymentStatusEnum {
    PENDING_AUTHORIZATION,
    AUTHORIZED,
    PROBLEM_IN_AUTHORIZATION,
    CHARGED
}
