package com.halal.booking.enums;

public enum ReviewPurposeTypeEnum {
    BUSINESS,
    ENTERTAINMENT,
    OTHER
}
