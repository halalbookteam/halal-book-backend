package com.halal.booking.enums;

public enum PaymentOptionTypeEnum {
    NONE_REFUNDABLE,
    FREE_CANCELLATION,
    PARTIALLY_CANCELLATION
}
