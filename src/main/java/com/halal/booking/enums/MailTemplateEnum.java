package com.halal.booking.enums;

public enum MailTemplateEnum {
    RESERVATION("reservation.html"),
    CLICK_LINK("clickLink.html"),
    ;

    private String templateName;

    MailTemplateEnum(String templateName) {
        this.templateName = templateName;
    }

    public String getTemplateName() {
        return templateName;
    }
}
