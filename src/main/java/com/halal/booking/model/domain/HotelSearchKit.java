package com.halal.booking.model.domain;

import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Room;
import com.halal.booking.model.entity.RoomAlternative;
import com.halal.booking.model.entity.RoomReservation;

import java.util.*;
import java.util.stream.Collectors;

public class HotelSearchKit {
    private List<RoomAlternative> alternativeList;
    private Map<Room, Integer> availableRoomCountMap;
    private HotelSearchQuery hotelSearchQuery;
    private Integer minRoomCount;
    private Set<RoomSetAlternative> possibilities;
    private RoomSetAlternative bestPossibility;

    public RoomSetAlternative findBestMatch(Hotel hotel, HotelSearchQuery hotelSearchQuery) {
        this.hotelSearchQuery = hotelSearchQuery;
        this.alternativeList = hotel.getRooms().stream()
                .filter(room -> room.getTotalBasePrice() != null)
                .flatMap(room -> room.getAlternatives().stream().filter(alternative -> alternative.getPrice() != null))
                .sorted(Comparator.comparing(RoomAlternative::getPrice)).collect(Collectors.toList());

        this.availableRoomCountMap = hotel.getRooms().stream()
                .filter(room -> room.getTotalBasePrice() != null)
                .collect(Collectors.toMap(room -> room, Room::getAvailableCount));

        Integer adultCount = hotelSearchQuery.getAdultCount(hotel.getMaxChildAge());
        Integer childCount = hotelSearchQuery.getChildCount(hotel.getMaxChildAge());
        if (this.calculateMinRoomCount(adultCount, childCount) == null) {
            return null;
        }

        this.possibilities = new HashSet<>();
        RoomSetAlternative possibility = new RoomSetAlternative();
        possibility.setAvailableRooms(new HashMap<>(this.availableRoomCountMap));
        possibility.setAlternativeList(this.alternativeList);
        this.findPossibilities(adultCount, childCount, this.minRoomCount, possibility);

        return this.findBestPossibility();
    }

    private RoomSetAlternative findBestPossibility() {
        for (RoomSetAlternative possibility : possibilities) {
            possibility.calculatePrice();
            this.bestPossibility = this.bestPossibility == null
                    ? possibility : this.bestPossibility.min(possibility);
        }
        return this.bestPossibility;
    }

    private void findPossibilities(Integer adultCount, Integer childCount, Integer roomCount, RoomSetAlternative possibility) {
        for (int currentAdultCount = adultCount / roomCount; currentAdultCount > 0; currentAdultCount--) {
            int currentChildCount = childCount / roomCount;
            RoomAlternative alternative = possibility.findAlternative(currentAdultCount, currentChildCount);
            if(alternative != null) {
                RoomSetAlternative fork = possibility.fork();
                possibility.addRoomReservation(RoomReservation.builder()
                        .alternative(alternative)
                        .adultCount(currentAdultCount)
                        .childCount(currentChildCount)
                        .build());
                if(roomCount == 1) {
                    this.possibilities.add(possibility);
                    break;
                }
                this.findPossibilities(adultCount - currentAdultCount, childCount - currentChildCount, roomCount - 1, possibility);
                possibility = fork;
            }
            else {
                break;
            }
        }
    }

    private Integer calculateMinRoomCount(Integer adultCount, Integer childCount) {
        int totalAdultCount = 0;
        int totalChildCount = 0;
        int currentRoomCount = 0;
        Set<Room> usedRooms = new HashSet<>();
        ListIterator<RoomAlternative> listIterator = alternativeList.listIterator(alternativeList.size());
        while (listIterator.hasPrevious()) {
            RoomAlternative alternative = listIterator.previous();
            if (!usedRooms.add(alternative.getRoom())) {
                continue;
            }
            Integer availableCount = availableRoomCountMap.get(alternative.getRoom());
            for (int i = 1; i <= availableCount; i++) {
                totalAdultCount += alternative.getMaxAdult();
                totalChildCount += alternative.getMaxChild();
                currentRoomCount++;
                if (currentRoomCount >= hotelSearchQuery.getRoomCount() &&
                        totalAdultCount >= adultCount &&
                        totalAdultCount + totalChildCount >= adultCount + childCount) {
                    return minRoomCount = currentRoomCount;
                }
            }
        }
        return null;
    }
}
