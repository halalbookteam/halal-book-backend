package com.halal.booking.model.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.model.dto.Price;
import com.halal.booking.model.entity.AbstractModel;
import com.halal.booking.model.entity.Room;
import com.halal.booking.model.entity.RoomAlternative;
import com.halal.booking.model.entity.RoomReservation;
import lombok.Data;

import java.util.*;
import java.util.stream.Collectors;

@Data
public class RoomSetAlternative {
    @JsonIgnore
    private Map<RoomAlternative, Integer> roomAlternativeCountMap = new HashMap<>();

    private List<RoomReservation> roomReservations = new ArrayList<>();
    private Price totalPrice;
    @JsonIgnore
    private Map<Room, Integer> availableRooms;
    @JsonIgnore
    private List<RoomAlternative> alternativeList;

    public RoomSetAlternative min(RoomSetAlternative that) {
        return this.totalPrice.compareTo(that.totalPrice) > 0 ? that : this;
    }

    public RoomSetAlternative fork() {
        RoomSetAlternative fork = new RoomSetAlternative();
        fork.availableRooms = new HashMap<>(this.availableRooms);
        fork.roomReservations = new ArrayList<>(this.roomReservations);
        fork.alternativeList = this.alternativeList;
        return fork;
    }

    public void calculatePrice() {
        this.totalPrice = this.roomReservations.stream()
                .map(roomReservation -> roomReservation.getAlternative().getPrice())
                .reduce(new Price(), Price::add);
    }

    public void addRoomReservation(RoomReservation roomReservation) {
        Room room = roomReservation.getAlternative().getRoom();
        this.availableRooms.put(room, this.availableRooms.get(room) - 1);
        this.roomReservations.add(roomReservation);

        Integer count = this.roomAlternativeCountMap.get(roomReservation.getAlternative());
        if(count == null) {
            count = 0;
        }
        this.roomAlternativeCountMap.put(roomReservation.getAlternative(), count + 1);
    }

    public RoomAlternative findAlternative(int adultCount, int childCount) {
        return alternativeList.stream()
                .filter(roomAlternative -> this.availableRooms.get(roomAlternative.getRoom()) > 0)
                .filter(roomAlternative -> roomAlternative.isAvailable(adultCount, childCount))
                .findFirst().orElse(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoomSetAlternative that = (RoomSetAlternative) o;

        return this.calculateMapString().equals(that.calculateMapString());
    }

    private String calculateMapString() {
        return this.roomAlternativeCountMap.keySet().stream()
                .sorted(Comparator.comparing(AbstractModel::getId))
                .map(alternative -> alternative.getId() + " " + this.roomAlternativeCountMap.get(alternative))
                .collect(Collectors.joining(";"));
    }

    @Override
    public int hashCode() {
        return this.calculateMapString().hashCode();
    }
}
