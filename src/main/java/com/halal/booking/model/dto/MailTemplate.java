package com.halal.booking.model.dto;

import com.halal.booking.exception.BookingRuntimeException;
import lombok.Data;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

@Data
public class MailTemplate {
    private String name;
    private List<MailTemplateParameter> parameters;
    private static final String templatePath = "/templates/";

    public static MailTemplateBuilder builder() {
        return new MailTemplateBuilder();
    }

    public static class MailTemplateBuilder {
        private MailTemplate instance = new MailTemplate();

        public MailTemplateBuilder file(String fileName) {
            try {
                return file(new File(MailTemplate.class.getResource(templatePath + fileName).toURI()));
            } catch (URISyntaxException e) {
                throw new BookingRuntimeException(e);
            }
        }

        public MailTemplateBuilder file(File file) {
            String fileName = file.getName();
            instance.setName(fileName.substring(0, fileName.lastIndexOf(".")));
            List<MailTemplateParameter> parameterList = new ArrayList<>();
            try {
                BufferedReader reader = new BufferedReader(new FileReader(file));
                while (reader.ready()) {
                    String line = reader.readLine();
                    int parameterIndexes[];
                    while ((parameterIndexes = findParameter(line)) != null) {
                        int beginIndex = parameterIndexes[0];
                        int endIndex = parameterIndexes[1];
                        parameterList.add(new MailTemplateParameter(line.substring(beginIndex, endIndex)));
                        line = line.substring(endIndex);
                    }
                }
            } catch (IOException e) {
                throw new BookingRuntimeException(e);
            }
            instance.setParameters(parameterList);
            return this;
        }

        public MailTemplateBuilder parameter(String key, Object value) {
            this.instance.parameters.stream().filter(param -> param.getKey().equals(key)).findFirst()
                    .ifPresent(parameter -> parameter.setValue(value));
            return this;
        }

        private int[] findParameter(String str) {
            int startIndex = str.indexOf("${");
            if(startIndex != -1) {
                return new int[] {startIndex + 2, str.indexOf('}', startIndex)};
            }
            return null;
        }

        public MailTemplate build() {
            return instance;
        }
    }
}