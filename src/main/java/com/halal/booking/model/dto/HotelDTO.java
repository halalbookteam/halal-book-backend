package com.halal.booking.model.dto;

import com.halal.booking.model.entity.*;
import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class HotelDTO {
    private Long id;
    private String name;
    private String description;
    private Integer star;
    private Address address;
    private HotelEvaluation evaluation;
    private Integer primaryPhotoIndex;
    private List<Photo> photos;
    private List<Room> rooms;
    private List<Facility> facilities;
}
