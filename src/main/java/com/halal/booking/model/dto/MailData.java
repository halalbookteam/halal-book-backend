package com.halal.booking.model.dto;

import com.halal.booking.enums.RecipientEnum;
import lombok.Builder;
import lombok.Data;

import java.util.Set;

@Data
@Builder
public class MailData {
    private RecipientEnum recipientType;
    private Set<String> recipientList;
    private String subject;
    private MailTemplate template;
}
