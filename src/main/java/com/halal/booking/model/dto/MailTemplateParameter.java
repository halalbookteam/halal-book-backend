package com.halal.booking.model.dto;

import com.halal.booking.enums.MailTemplateParameterTypeEnum;
import lombok.Data;

@Data
public class MailTemplateParameter {
    private String name;
    private String key;
    private MailTemplateParameterTypeEnum type;
    private Object value;

    public MailTemplateParameter(String key) {
        this.key = key;
        int index = key.lastIndexOf("_");
        if(index != -1) {
            String typeStr = key.substring(index + 1);
            MailTemplateParameterTypeEnum type = MailTemplateParameterTypeEnum.find(typeStr);
            if(type != null) {
                this.name = key.substring(0, index);
                this.type = type;
                return;
            }
        }
        this.name = key;
        this.type = MailTemplateParameterTypeEnum.TEXT;
    }
}
