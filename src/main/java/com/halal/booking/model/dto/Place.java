package com.halal.booking.model.dto;

import com.halal.booking.model.entity.AddressTranslation;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Data
@Valid
public class Place {
    private String hotelName;
    private String district;
    private String city;
    private String county;
    @NotNull
    private String country;

    public Place(String hotelName, AddressTranslation addressTranslation) {
        this.hotelName = hotelName;
        if(addressTranslation != null) {
            this.district = addressTranslation.getDistrict();
            this.city = addressTranslation.getCity();
            this.county = addressTranslation.getCounty();
            this.country = addressTranslation.getCountry();
        }
    }

    public Place(String hotelName, String district, String city, String county, String country) {
        this.hotelName = hotelName;
        this.district = district;
        this.city = city;
        this.county = county;
        this.country = country;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Place)) return false;

        Place place = (Place) o;

        if (hotelName != null ? !hotelName.equals(place.hotelName) : place.hotelName != null) return false;
        if (district != null ? !district.equals(place.district) : place.district != null) return false;
        if (city != null ? !city.equals(place.city) : place.city != null) return false;
        if (county != null ? !county.equals(place.county) : place.county != null) return false;
        return country.equals(place.country);
    }

    @Override
    public int hashCode() {
        int result = hotelName != null ? hotelName.hashCode() : 0;
        result = 31 * result + (district != null ? district.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (county != null ? county.hashCode() : 0);
        result = 31 * result + country.hashCode();
        return result;
    }
}
