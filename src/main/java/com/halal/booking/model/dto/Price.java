package com.halal.booking.model.dto;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingRuntimeException;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Embeddable
@Data
@Builder
public class Price implements Comparable<Price>{
    @Positive
    private BigDecimal amount = BigDecimal.ZERO;
    @NotNull
    private String currency;

    public Price() {}

    public Price(BigDecimal amount, String currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public Price add(Price other) {
        if(this.amount.compareTo(BigDecimal.ZERO) > 0 && !this.currency.equals(other.currency)) {
            throw new BookingRuntimeException(MessageTypeEnum.DIFFERENT_CURRENCIES, this.currency, other.currency);
        }
        return new Price(this.amount.add(other.amount), other.currency);
    }

    public Price multiply(BigDecimal count) {
        return new Price(this.amount.multiply(count), this.currency);
    }

    @Override
    public int compareTo(Price o) {
        if(!this.currency.equals(o.currency)) {
            throw new BookingRuntimeException(MessageTypeEnum.DIFFERENT_CURRENCIES);
        }
        return this.amount.compareTo(o.getAmount());
    }

    @Override
    public String toString() {
        return this.amount + " " + this.currency;
    }

    public boolean isPositive() {
        return this.amount.compareTo(BigDecimal.ZERO) > 0;
    }
}
