package com.halal.booking.model.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ChangePasswordDTO {
    @NotNull
    @Size(min = 6, max = 20)
    private String currentPassword;
    @NotNull
    @Size(min = 6, max = 20)
    private String newPassword;
}
