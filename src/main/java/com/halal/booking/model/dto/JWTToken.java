package com.halal.booking.model.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class JWTToken {
    @JsonProperty("id_token")
    private String idToken;

    public JWTToken(String idToken) {
        this.idToken = idToken;
    }
}
