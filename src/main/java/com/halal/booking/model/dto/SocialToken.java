package com.halal.booking.model.dto;

import lombok.Data;

@Data
public class SocialToken {
    private String token;
}
