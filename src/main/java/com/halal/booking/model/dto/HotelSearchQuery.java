package com.halal.booking.model.dto;

import com.halal.booking.service.DateTimeUtils;
import lombok.Builder;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.PositiveOrZero;
import java.util.Arrays;
import java.util.Date;

@Data
@Builder
public class HotelSearchQuery {
    @NotNull
    @Valid
    private Place place;
    @Positive
    private Integer roomCount;
    @PositiveOrZero
    private Integer childCount;
    @Positive
    private Integer adultCount;
    private Date checkIn;
    private Date checkOut;

    private int[] childAges;

    public boolean hasCheckInCheckOut() {
        return this.checkIn != null && this.checkOut != null;
    }

    public int getNightCount() {
        return hasCheckInCheckOut() ? new DateTimeUtils().getNightCount(checkIn, checkOut) : -1;
    }

    public Integer getChildCount(Integer maxChildAge) {
        if(childCount == 0 || childAges == null) {
            return 0;
        }
        return (int) Arrays.stream(this.childAges).filter(value -> value <= maxChildAge).count();
    }

    public Integer getAdultCount(Integer maxChildAge) {
        return this.adultCount + this.childCount - getChildCount(maxChildAge);
    }
}
