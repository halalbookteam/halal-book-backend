package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@Data
@Entity
@Table(name = "evaluation_category_score")
public class EvaluationCategoryScore extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private HotelEvaluation evaluation;

    @ManyToOne
    private ReviewCategory category;

    private BigDecimal score;

    @JsonIgnore
    private long count;

    @Override
    public boolean equals(Object o) {
        EvaluationCategoryScore other = (EvaluationCategoryScore) o;
        return this.category.equals(other.category);
    }

    @Override
    public int hashCode() {
        return this.category.hashCode();
    }
}
