package com.halal.booking.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "payment_option")
@Data
public class PaymentOption extends AbstractModel {
    private Integer freeCancellationDays;
    private Integer penaltyCancellationDays;
    private BigDecimal penaltyPercentage;
    private Boolean noPrepayment;

    public boolean isAvailable(int nightCount) {
        return Integer.max(this.freeCancellationDays == null ? 0 : this.freeCancellationDays,
                this.penaltyCancellationDays == null ? 0 : this.penaltyCancellationDays) <= nightCount;

    }

    public String label() {
        return this.isNonRefundable() ? "Non-Refundable" : "Refundable";
    }

    public boolean isNonRefundable() {
        return this.freeCancellationDays == null && this.penaltyCancellationDays == null;
    }
}
