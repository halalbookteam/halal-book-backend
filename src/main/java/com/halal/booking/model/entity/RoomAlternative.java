package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.enums.PropertyStatusEnum;
import com.halal.booking.model.dto.Price;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.math.BigDecimal;

@Entity
@Table(name = "room_alternative")
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoomAlternative extends AbstractModel implements Comparable<RoomAlternative> {
    @JsonIgnore
    @ManyToOne
    private Room room;

    @Positive
    private int maxAdult;
    private int maxChild;

    @NotNull
    private BigDecimal basePriceRatio;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @NotNull
    @Valid
    private MealOption mealOption;

    @OneToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REMOVE})
    @NotNull
    private PaymentOption paymentOption;

    @Enumerated
    private PropertyStatusEnum status;

    @Transient
    private Price price;

    public Long getHotelId() {
        return this.room != null ? this.room.getHotelId() : null;
    }

    public Long getRoomId() {
        return this.room != null ? this.room.getId() : null;
    }

    public void setRoomId(Long id) {
        this.room = new Room();
        this.room.setId(id);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public boolean isAvailable(int nightCount) {
        return this.paymentOption.isAvailable(nightCount);
    }

    public boolean isAvailable(int adultCount, int childCount) {
        return adultCount <= this.maxAdult && (adultCount + childCount) <= (this.maxAdult + this.maxChild);
    }

    @Override
    public String toString() {
        return "RoomAlternative{" +
                "room=" + room.getId() +
                ", maxAdult=" + maxAdult +
                ", maxChild=" + maxChild +
                ", price=" + price +
                '}';
    }

    @Override
    public int compareTo(RoomAlternative o) {
        return this.getBasePriceRatio().compareTo(o.getBasePriceRatio());
    }

    public String label() {
        String mealOptionLabel = this.mealOption.label();
        return this.paymentOption.label() + (mealOptionLabel.isEmpty() ? "" : ("/" + mealOptionLabel)) + "/" + this.maxAdult + "+" + this.maxChild;
    }

    public Price calculatePrice(Price basePrice) {
        return basePrice.multiply(this.basePriceRatio);
    }
}
