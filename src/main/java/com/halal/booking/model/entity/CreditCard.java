package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "credit_card")
public class CreditCard extends AbstractModel {
    @JsonIgnore
    @OneToOne
    private Reservation reservation;

    private String sourceId;

    public Long getReservationId() {
        return this.reservation.getId();
    }

    public void setReservationId(Long id) {
        this.reservation = new Reservation();
        this.reservation.setId(id);
    }
}
