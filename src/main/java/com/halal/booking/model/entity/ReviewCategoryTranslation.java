package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "review_category_translation")
public class ReviewCategoryTranslation extends AbstractTranslationModel {
    @JsonIgnore
    @ManyToOne
    private ReviewCategory category;

    @Column(nullable = false)
    @NotNull
    private String name;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
