package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Optional;
import java.util.Set;

@Entity
@Table(name = "hotel_evaluation")
@Data
public class HotelEvaluation extends AbstractModel {
    @JsonIgnore
    @OneToOne
    private Hotel hotel;

    @OneToMany(mappedBy = "evaluation", cascade = CascadeType.ALL)
    private Set<EvaluationCategoryScore> evaluationCategoryScores;

    private BigDecimal score;

    private Long reviewCount;

    public void calculateScore() {
        this.score = BigDecimal.ZERO;
        this.evaluationCategoryScores.forEach(categoryScore -> this.score = this.score.add(categoryScore.getScore()));
        this.score = this.score.divide(new BigDecimal(this.evaluationCategoryScores.size()), 1, BigDecimal.ROUND_HALF_UP);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public Optional<EvaluationCategoryScore> findScoreByCategory(ReviewCategory category) {
        return this.evaluationCategoryScores.stream().filter(score -> score.getCategory().equals(category)).findFirst();
    }
}
