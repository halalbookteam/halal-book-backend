package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.enums.PropertyStatusEnum;
import com.halal.booking.model.dto.Price;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Positive;
import java.util.List;
import java.util.Set;

@Entity
@Table
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Room extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private Hotel hotel;
    @Positive
    private int size;
    private Integer primaryPhotoIndex;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private Set<RoomAlternative> alternatives;

    @OneToMany(mappedBy = "room")
    private List<RoomRate> rates;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private List<Photo> photos;

    @ManyToMany
    @JoinTable(
            name = "rooms_facilities",
            joinColumns = @JoinColumn(
                    name = "room_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "facility_id", referencedColumnName = "id"))
    private List<Facility> facilities;

    @OneToMany(mappedBy = "room", cascade = CascadeType.ALL)
    private Set<RoomTranslation> translations;

    @Enumerated
    private PropertyStatusEnum status;

    @Transient
    private Price totalBasePrice;

    @Transient
    private int availableCount;

    public void setHotelId(Long hotelId) {
        this.hotel = new Hotel();
        this.hotel.setId(hotelId);
        this.setPhotos(this.photos);
    }

    public Long getHotelId() {
        return this.hotel.getId();
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
        if(this.photos != null) {
            this.photos.forEach(photo -> {
                photo.setRoom(this);
                photo.setHotel(this.hotel);
            });
        }
    }

    @Override
    public String toString() {
        return "Room{" +
                ", size=" + size +
                ", primaryPhotoIndex=" + primaryPhotoIndex +
                ", hotelId=" + hotel.getId() +
                ", photos=" + photos +
                ", facilities=" + facilities +
                '}';
    }
    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void updateTranslations() {
        this.translations.forEach(translation -> translation.setRoom(this));
    }

    public void prepareSearch(List<RoomRate> roomRatesForSearch) {
        this.setTotalBasePrice(new Price());
        this.setAvailableCount(Integer.MAX_VALUE);

        roomRatesForSearch.forEach(roomRate -> {
            this.setTotalBasePrice(this.getTotalBasePrice().add(roomRate.getBasePrice()));
            this.setAvailableCount(Integer.min(this.getAvailableCount(), roomRate.getAvailableRoomCount()));
        });

        this.getAlternatives().forEach(roomAlternative -> {
            roomAlternative.setPrice(this.getTotalBasePrice().multiply(roomAlternative.getBasePriceRatio()));
        });
    }

    @JsonIgnore
    public RoomTranslation getDefaultTranslation() {
        return this.translations.stream()
                .filter(roomTranslation -> roomTranslation.getLanguage().equals(this.hotel.getDefaultLanguage()))
                .findFirst().orElse(null);
    }

    public RoomAlternative findAlternativeById(Long id) {
        return this.alternatives.stream().filter(alternative -> alternative.getId().equals(id)).findFirst().orElse(null);
    }
}
