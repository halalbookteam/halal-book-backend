package com.halal.booking.model.entity;

import com.halal.booking.enums.ActionTypeEnum;
import com.halal.booking.enums.UserTypeEnum;
import com.halal.booking.usermanagement.model.User;
import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "user_action")
@Builder
public class UserAction extends AbstractModel {
    @Column(nullable = false)
    private Date time;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private ActionTypeEnum actionType;

    private Long entityId;

    private String oldValue;

    private String newValue;

    @ManyToOne
    private User user;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private UserTypeEnum userType;

    private String ip;

    public void setNewEntity(AbstractModel entity) {
        this.setEntityId(entity.getId());
        this.setNewValue(entity.toString());
    }

    public void setOldEntity(AbstractModel entity) {
        if(entity != null) {
            this.setOldValue(entity.toString());
        }
    }
}
