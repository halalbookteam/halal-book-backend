package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table
@Data
public class Facility extends AbstractModel {
    private String iconUrl;

    @JsonIgnore
    @ManyToOne
    private FacilityCategory category;

    private boolean chargeable;

    @OneToMany(mappedBy = "facility", cascade = CascadeType.ALL)
    private Set<FacilityTranslation> translations;

    public Long getCategoryId() {
        return category.getId();
    }

    public void setCategoryId(Long categoryId) {
        this.category = new FacilityCategory();
        this.category.setId(categoryId);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void updateTranslations() {
        this.translations.forEach(translation -> translation.setFacility(this));
    }
}
