package com.halal.booking.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
public class Currency extends AbstractModel {
    private String code;
    private String shortName;

    public String getSymbol() {
        return getCurrency().getSymbol();
    }

    public String getName() {
        return getCurrency().getDisplayName();
    }

    public int getFractionDigits() {
        return getCurrency().getDefaultFractionDigits();
    }

    private java.util.Currency getCurrency() {
        return java.util.Currency.getInstance(this.code);
    }
}
