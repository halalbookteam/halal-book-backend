package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.model.dto.Price;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.Date;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "room_rate", uniqueConstraints={@UniqueConstraint(columnNames={"room_id", "date"})})
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class RoomRate extends AbstractModel {
    @JsonIgnore
    @ManyToOne(optional = false)
    private Room room;

    @NotNull
    @Temporal(TemporalType.DATE)
    @OrderBy
    private Date date;

    private int availableRoomCount;

    @Embedded
    @NotNull
    @Valid
    private Price basePrice;

    private int minNightCount;

    public RoomRate copy(RoomRate rate) {
        this.date = rate.date;
        this.availableRoomCount = rate.availableRoomCount;
        this.minNightCount = rate.minNightCount;
        this.basePrice = rate.basePrice;

        return this;
    }
}
