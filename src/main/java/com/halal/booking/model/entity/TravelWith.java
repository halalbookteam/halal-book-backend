package com.halal.booking.model.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "travel_with")
public class TravelWith extends AbstractModel {
    private boolean alone;
    private boolean friends;
    private boolean partner;
    private boolean family;
    private boolean colleague;
    private boolean pet;

    public boolean hasInfo() {
        return alone || friends || partner || family || colleague || pet;
    }
}
