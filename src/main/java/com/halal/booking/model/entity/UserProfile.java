package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.usermanagement.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "user_profile")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class UserProfile extends AbstractModel {
    @JsonIgnore
    @OneToOne
    private User user;

    private String displayName;

    private boolean newsletter;

    @OneToOne(cascade = CascadeType.ALL)
    private Photo avatar;

    @ManyToOne
    private Currency defaultCurrency;

    @ManyToOne
    private Language defaultLanguage;
}
