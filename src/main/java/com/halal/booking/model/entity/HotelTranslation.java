package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "hotel_translation")
public class HotelTranslation extends AbstractTranslationModel {
    @JsonIgnore
    @ManyToOne
    private Hotel hotel;

    @Column(nullable = false)
    @NotNull
    private String name;
    private String description;
}
