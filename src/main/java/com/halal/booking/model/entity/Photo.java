package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
public class Photo extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private Hotel hotel;

    @JsonIgnore
    @ManyToOne
    private Room room;

    @Column(nullable = false)
    @NotNull
    private String url;

    @Override
    public String toString() {
        return "Photo{" +
                "hotelId=" + hotel.getId() +
                ", roomId=" + room.getId() +
                ", url='" + url + '\'' +
                '}';
    }
}
