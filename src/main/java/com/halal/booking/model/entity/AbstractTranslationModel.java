package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.MappedSuperclass;
import javax.persistence.OneToOne;

@MappedSuperclass
public abstract class AbstractTranslationModel extends AbstractModel {
    @OneToOne
    @JsonIgnore
    private Language language;

    public Language getLanguage() {
        return language;
    }

    public void setLanguage(Language language) {
        this.language = language;
    }

    public Long getLanguageId() {
        return this.language.getId();
    }

    public void setLanguageId(Long languageId) {
        this.language = new Language();
        this.language.setId(languageId);
    }
}
