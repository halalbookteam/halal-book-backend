package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Builder;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Data
@Entity
@Table(name = "room_reservation")
@Builder
public class RoomReservation extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private Reservation reservation;

    @JsonIgnore
    @ManyToOne
    private RoomAlternative alternative;

    private int adultCount;

    private int childCount;

    private Date cancellationTime;

    public Long getReservationId() {
        return this.reservation == null ? null : this.reservation.getId();
    }

    public void setReservationId(Long id) {
        this.reservation = new Reservation();
        this.reservation.setId(id);
    }

    public Long getAlternativeId() {
        return this.alternative.getId();
    }

    public void setAlternativeId(Long id) {
        this.alternative = new RoomAlternative();
        this.alternative.setId(id);
    }

    @Override
    public boolean equals(Object o) {
        return !super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
