package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.enums.ReviewPurposeTypeEnum;
import com.halal.booking.enums.ReviewStatusEnum;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Review extends AbstractModel {
    @JsonIgnore
    @OneToOne(optional = false)
    private Reservation reservation;

    @Enumerated(EnumType.STRING)
    private ReviewPurposeTypeEnum purpose;

    @OneToMany(mappedBy = "review", cascade = CascadeType.ALL)
    private List<ReviewCategoryScore> categoryScores;

    private String likeComment;

    private String dislikeComment;

    @Enumerated(EnumType.STRING)
    private ReviewStatusEnum status;

    @OneToOne(cascade = CascadeType.ALL)
    private TravelWith travelWith;

    private boolean includeMyName;

    @ManyToOne
    private Language language;

    private Date time;

    private String title;

    public ReviewCategoryScore findCategoryScoreByCategoryId(Long id) {
        if(this.categoryScores != null) {
            return this.categoryScores.stream().filter(reviewCategoryScore -> reviewCategoryScore.getCategory().getId().equals(id)).findFirst().orElse(null);
        }
        return null;
    }

    public void setReservationId(Long id) {
        if(this.reservation == null) {
            this.reservation = new Reservation();
        }
        this.reservation.setId(id);
    }

    public Long getReservationId() {
        return this.reservation == null ? null : this.reservation.getId();
    }

    public String getReviewerName() {
        if(!this.includeMyName) {
            return "Anonymous";
        }
        return this.reservation.getUser().getProfile().getDisplayName();
    }

    public String getReviewerCountry() {
        return this.reservation.getPerson().getCountry();
    }

    public void setLanguageId(long languageId) {
        this.language = new Language();
        this.language.setId(languageId);
    }

    public long getLanguageId() {
        return this.language.getId();
    }
}
