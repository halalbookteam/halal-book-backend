package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.math.BigDecimal;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "review_category_score")
public class ReviewCategoryScore extends AbstractModel {
    @JsonIgnore
    @ManyToOne
    private Review review;

    @ManyToOne
    private ReviewCategory category;

    private BigDecimal score;
}
