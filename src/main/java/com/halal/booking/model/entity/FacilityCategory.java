package com.halal.booking.model.entity;

import com.halal.booking.enums.CategoryTypeEnum;
import lombok.Data;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "facility_category")
@Data
public class FacilityCategory extends AbstractModel {
    private boolean halal;

    @Enumerated(EnumType.STRING)
    private CategoryTypeEnum type;

    private boolean filter;

    @OneToMany(mappedBy = "category")
    private List<Facility> facilities;

    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private Set<FacilityCategoryTranslation> translations;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public void updateTranslations() {
        this.translations.forEach(translation -> translation.setCategory(this));
    }
}
