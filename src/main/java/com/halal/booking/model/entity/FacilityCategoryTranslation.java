package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "facility_category_translation")
public class FacilityCategoryTranslation extends AbstractTranslationModel {
    @JsonIgnore
    @ManyToOne
    private FacilityCategory category;

    @Column(nullable = false)
    @NotNull
    private String name;
    private String description;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
