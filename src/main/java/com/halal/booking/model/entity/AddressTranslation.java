package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "address_translation")
public class AddressTranslation extends AbstractTranslationModel {
    @JsonIgnore
    @ManyToOne
    private Address address;

    private String street;
    private String district;
    private String city;
    private String county;
    private String country;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        AddressTranslation that = (AddressTranslation) o;

        if (street != null ? !street.equals(that.street) : that.street != null) return false;
        if (district != null ? !district.equals(that.district) : that.district != null) return false;
        if (city != null ? !city.equals(that.city) : that.city != null) return false;
        if (county != null ? !county.equals(that.county) : that.county != null) return false;
        return country != null ? country.equals(that.country) : that.country == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (street != null ? street.hashCode() : 0);
        result = 31 * result + (district != null ? district.hashCode() : 0);
        result = 31 * result + (city != null ? city.hashCode() : 0);
        result = 31 * result + (county != null ? county.hashCode() : 0);
        result = 31 * result + (country != null ? country.hashCode() : 0);
        return result;
    }

    @JsonIgnore
    public String getShort() {
        String str = this.country;
        if(this.county != null) {
            str = this.county + ", " + str;
        }
        if(this.city != null) {
            str = this.city + ", " + str;
        }
        if(this.district != null) {
            str = this.district + ", " + str;
        }
        return str;
    }
}
