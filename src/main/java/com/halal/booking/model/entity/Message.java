package com.halal.booking.model.entity;


import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;

@Data
@Entity
public class Message extends AbstractTranslationModel {
    private String message;
    @Column(nullable = false)
    private String type;

    public String format(Object[] parameters) {
        return String.format(this.message, parameters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Message message = (Message) o;

        return type.equals(message.type);
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }
}
