package com.halal.booking.model.entity;


import lombok.Data;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.Set;

@Data
@Entity
@Table(name = "review_category")
public class ReviewCategory extends AbstractModel {
    @OneToMany(mappedBy = "category", cascade = CascadeType.ALL)
    private Set<ReviewCategoryTranslation> translations;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
