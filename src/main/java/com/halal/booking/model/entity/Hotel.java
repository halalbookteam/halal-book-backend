package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.enums.ChannelManagerEnum;
import com.halal.booking.model.domain.HotelSearchKit;
import com.halal.booking.model.domain.RoomSetAlternative;
import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.usermanagement.model.User;
import lombok.Data;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Entity
@Table
@Data
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Hotel extends AbstractModel {
    @Max(5)
    private Integer star;

    private Integer primaryPhotoIndex;

    @OneToOne(mappedBy = "hotel", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @NotFound(action = NotFoundAction.IGNORE)
    @NotNull
    @Valid
    private Address address;

    @OneToMany(mappedBy = "hotel", cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Photo> photos;

    @OneToMany(mappedBy = "hotel")
    private Set<Room> rooms;

    @ManyToMany
    @JoinTable(
            name = "hotels_facilities",
            joinColumns = @JoinColumn(
                    name = "hotel_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "facility_id", referencedColumnName = "id"))
    private List<Facility> facilities;

    @JsonIgnore
    @ManyToMany
    @JoinTable(
            name = "hotels_users",
            joinColumns = @JoinColumn(
                    name = "hotel_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"))
    private List<User> users;

    @OneToOne(mappedBy = "hotel", fetch = FetchType.LAZY)
    private HotelEvaluation evaluation;

    @OneToOne(optional = false)
    @JsonIgnore
    private Language defaultLanguage;

    @OneToMany(mappedBy = "hotel", cascade = CascadeType.ALL)
    private Set<HotelTranslation> translations;

    private int maxChildAge;

    @Transient
    private RoomSetAlternative recommendedRooms;

    private String currency;

    @Enumerated(EnumType.STRING)
    private ChannelManagerEnum channelManager;

    public Long getDefaultLanguageId() {
        return this.defaultLanguage == null ? null : this.defaultLanguage.getId();
    }

    public void setDefaultLanguageId(Long defaultLanguageId) {
        this.defaultLanguage = new Language();
        this.defaultLanguage.setId(defaultLanguageId);
    }

    public void setPhotos(List<Photo> photos) {
        this.photos = photos;
        this.photos.forEach(photo -> photo.setHotel(this));
    }

    public List<Photo> getPhotos() {
        return photos.stream().filter(photo -> photo.getRoom() == null).collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Hotel)) return false;
        if (!super.equals(o)) return false;

        Hotel hotel = (Hotel) o;

        return this.getId() != null ? getId().equals(hotel.getId()) : hotel.getId() == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (getId() != null ? getId().hashCode() : 0);
        return result;
    }

    public String getName() {
        return this.translations == null ? null : this.translations.stream()
                .filter(hotelTranslation -> hotelTranslation.getLanguage().equals(this.defaultLanguage))
                .map(HotelTranslation::getName)
                .findFirst()
                .orElse(null);
    }

    public HotelTranslation getTranslation(String langCode) {
        return this.translations.stream()
                .filter(hotelTranslation -> hotelTranslation.getLanguage().getCode().equals(langCode))
                .findFirst().orElse(null);
    }

    @JsonIgnore
    public HotelTranslation getDefaultTranslation() {
        return this.getTranslation(this.defaultLanguage.getCode());
    }

    public void updateTranslations() {
        this.translations.forEach(translation -> translation.setHotel(this));
        this.rooms.forEach(Room::updateTranslations);
        this.address.updateTranslations();
    }

    public void prepareSearch(HotelSearchQuery hotelSearchQuery) {
        HotelSearchKit hotelSearchKit = new HotelSearchKit();
        this.recommendedRooms = hotelSearchKit.findBestMatch(this, hotelSearchQuery);
    }

    public Room getRoomById(Long id) {
        return this.rooms.stream().filter(room -> room.getId().equals(id)).findFirst().orElse(null);
    }
}
