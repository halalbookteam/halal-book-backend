package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.halal.booking.enums.ReservationStatusEnum;
import com.halal.booking.external.payment.stripe.entity.StripeCharge;
import com.halal.booking.external.payment.stripe.entity.StripeSource;
import com.halal.booking.model.dto.Price;
import com.halal.booking.service.DateTimeUtils;
import com.halal.booking.usermanagement.model.User;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Reservation extends AbstractModel {
    @OneToMany(mappedBy = "reservation", cascade = CascadeType.ALL)
    private Set<RoomReservation> roomReservations;

    @ManyToOne(optional = false)
    private Hotel hotel;

    @OneToOne(optional = false, cascade = CascadeType.ALL)
    private Person person;

    @OneToOne
    private StripeSource source;

    private String specialRequests;

    @ManyToOne
    private User user;

    private String reservationNo;

    @NotNull
    @Enumerated(EnumType.STRING)
    private ReservationStatusEnum status;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date checkIn;

    @NotNull
    @Temporal(TemporalType.DATE)
    private Date checkOut;

    private Date creationTime;
    private Date cancellationTime;

    private Price price;

    @OneToOne
    private StripeCharge nonRefundableCharge;

    @OneToOne
    private StripeCharge refundableCharge;

    @ManyToOne
    private Language language;

    public Long getHotelId() {
        return this.hotel.getId();
    }

    public void setHotelId(Long id) {
        this.hotel = new Hotel();
        this.hotel.setPhotos(Collections.emptyList());
        this.hotel.setId(id);
    }

    @Override
    public String toString() {
        return "Reservation{" +
                "id=" + this.getId() +
                ", hotelId=" + hotel.getId() +
                ", personId=" + (person == null ? "null" : person.getId()) +
                ", specialRequests=" + specialRequests +
                ", userId=" + (user == null ? "null" : user.getId()) +
                ", reservationNo=" + reservationNo +
                ", status=" + status +
                ", checkIn=" + checkIn +
                ", checkOut=" + checkOut +
                ", creationTime=" + creationTime +
                ", price=" + price +
                '}';
    }

    @JsonIgnore
    public String getCheckInOutDates() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        return dateFormat.format(this.checkIn) + " - " + dateFormat.format(this.checkOut);
    }

    public boolean canBeReviewed() {
        return this.status.equals(ReservationStatusEnum.CLOSED) && new DateTimeUtils().getNightCount(this.checkOut, new Date()) <= 30;
    }

    @JsonIgnore
    public Price getTotalNonRefundablePrice() {
        return this.roomReservations.stream()
                .map(RoomReservation::getAlternative)
                .filter(alternative -> alternative.getPaymentOption().isNonRefundable())
                .map(RoomAlternative::getPrice)
                .reduce(new Price(), Price::add);
    }

    @JsonIgnore
    public Date getLastNight() {
        return new DateTimeUtils().add(this.checkOut, Calendar.DATE, -1);
    }
}
