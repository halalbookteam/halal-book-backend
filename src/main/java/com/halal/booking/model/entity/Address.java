package com.halal.booking.model.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import java.util.Set;

@Data
@Entity
@Table
public class Address extends AbstractModel {
    @JsonIgnore
    @OneToOne
    private Hotel hotel;

    @Min(-180)
    @Max(180)
    private double lon;

    @Min(-90)
    @Max(90)
    private double lat;

    private String postalCode;

    @OneToMany(mappedBy = "address", cascade = CascadeType.ALL)
    private Set<AddressTranslation> translations;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Address address = (Address) o;

        if (Double.compare(address.lon, lon) != 0) return false;
        if (Double.compare(address.lat, lat) != 0) return false;
        return postalCode != null ? postalCode.equals(address.postalCode) : address.postalCode == null;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(lon);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lat);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (postalCode != null ? postalCode.hashCode() : 0);
        return result;
    }

    public void updateTranslations() {
        this.translations.forEach(translation -> translation.setAddress(this));
    }

    public AddressTranslation getTranslation(String langCode) {
        return this.translations.stream()
                .filter(hotelTranslation -> hotelTranslation.getLanguage().getCode().equals(langCode))
                .findFirst().orElse(null);
    }

    @JsonIgnore
    public AddressTranslation getDefaultTranslation() {
        return this.getTranslation(this.hotel.getDefaultLanguage().getCode());
    }
}
