package com.halal.booking.model.entity;

import com.halal.booking.enums.MealOptionTypeEnum;
import com.halal.booking.enums.MealTypeEnum;
import com.halal.booking.model.dto.Price;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.joining;

@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "meal_option")
@Data
public class MealOption extends AbstractModel {
    @Enumerated(EnumType.STRING)
    @NotNull
    private MealOptionTypeEnum type;
    @Column(name = "meals")
    private String mealsAsStr;
    @Embedded
    private Price price;

    public List<MealTypeEnum> getMeals() {
        if(this.mealsAsStr == null || this.mealsAsStr.isEmpty()) {
            return null;
        }
        return Arrays.stream(this.mealsAsStr.split(";")).map(MealTypeEnum::valueOf).collect(Collectors.toList());
    }

    public void setMeals(List<MealTypeEnum> mealList) {
        if(mealList == null || mealList.isEmpty()) {
            this.mealsAsStr = null;
            return;
        }
        this.mealsAsStr = mealList.stream().sorted().map(Enum::toString).collect(joining(";"));
    }

    public String label() {
        if(this.type.equals(MealOptionTypeEnum.ALL_INCLUDED)) {
            return "All Included";
        }
        if(this.type.equals(MealOptionTypeEnum.OPTIONAL) && this.price == null) {
            return this.mealsAsStr + " Included";
        }
        return "";
    }
}
