package com.halal.booking.controller;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.model.entity.UserProfile;
import com.halal.booking.service.AuthenticationService;
import com.halal.booking.usermanagement.model.User;
import com.halal.booking.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/user")
public class UserController {
    private UserService userService;
    private AuthenticationService authenticationService;

    @Autowired
    public UserController(UserService userService, AuthenticationService authenticationService) {
        this.userService = userService;
        this.authenticationService = authenticationService;
    }

    @GetMapping
    public List<User> getUsers() {
        return this.userService.getAllUsers();
    }

    @PostMapping
    public User createUser(@Valid @RequestBody User user) {
        return this.userService.saveUser(user);
    }

    @PutMapping("{userId}")
    public User updateUser(@PathVariable("userId") Long userId, @Valid @RequestBody User user) {
        if(userId == null || !ObjectUtils.nullSafeEquals(userId, user.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return this.userService.saveUser(user);
    }

    @DeleteMapping("{userId}")
    public void deleteUser(@PathVariable("userId") Long userId) {
        this.userService.deleteUser(userId);
    }

    @PutMapping("profile")
    public void saveProfile(@Valid @RequestBody UserProfile userProfile) throws BookingUnauthorizedUserException {
        this.userService.saveProfile(userProfile, authenticationService.getCurrentUser());
    }
}
