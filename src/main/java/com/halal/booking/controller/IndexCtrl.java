package com.halal.booking.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexCtrl
{
	@GetMapping("/")
	public String redirectToSwaggerUi() {
		return "redirect:/swagger-ui.html";
	}
}
