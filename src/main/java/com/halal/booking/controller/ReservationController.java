package com.halal.booking.controller;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.enums.ReviewStatusEnum;
import com.halal.booking.exception.BookingException;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.external.payment.stripe.entity.StripeSource;
import com.halal.booking.model.entity.Reservation;
import com.halal.booking.model.entity.Review;
import com.halal.booking.service.ReservationService;
import com.halal.booking.service.ReviewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;

@RestController
@RequestMapping("api/reservation")
public class ReservationController {
    private final ReservationService reservationService;
    private final ReviewService reviewService;

    @Autowired
    public ReservationController(ReservationService reservationService, ReviewService reviewService) {
        this.reservationService = reservationService;
        this.reviewService = reviewService;
    }

    @PostMapping
    public Reservation createReservation(@Valid @RequestBody Reservation reservation) {
        return this.reservationService.saveReservation(reservation);
    }

    @PutMapping("{reservationId}")
    public Reservation updateReservation(@PathVariable("reservationId") Long reservationId, @Valid @RequestBody Reservation reservation) {
        if(reservationId == null || !ObjectUtils.nullSafeEquals(reservationId, reservation.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return this.reservationService.saveReservation(reservation);
    }

    @PutMapping("cancel/{reservationId}")
    public Reservation cancelReservation(@PathVariable("reservationId") Long reservationId) throws BookingUnauthorizedUserException {
        return this.reservationService.cancelReservation(reservationId);
    }

    @PostMapping("{reservationId}/paymentDetail")
    public StripeSource savePaymentDetail(@PathVariable("reservationId") Long reservationId, @Valid @RequestBody StripeSource stripeSource) throws BookingException {
        return this.reservationService.savePaymentDetail(reservationId, stripeSource);
    }

    @GetMapping
    public Collection<Reservation> getReservations() throws BookingUnauthorizedUserException {
        return this.reservationService.getReservations();
    }

    @GetMapping("myReservations")
    public Collection<Reservation> getMyReservations() {
        return this.reservationService.getMyReservations();
    }

    @GetMapping("review/{reservationId}")
    public Review getReview(@PathVariable("reservationId") Long reservationId) throws BookingUnauthorizedUserException {
        return this.reviewService.getReview(reservationId);
    }

    @PostMapping("review")
    public Review createReview(@Valid @RequestBody Review review) throws BookingUnauthorizedUserException {
        this.reviewService.saveReview(review);
        return this.getReview(review.getReservationId());
    }

    @PutMapping("review/{reviewId}")
    public Review updateReview(@PathVariable("reviewId") Long reviewId, @Valid @RequestBody Review review) throws BookingUnauthorizedUserException {
        if(reviewId == null || !ObjectUtils.nullSafeEquals(reviewId, review.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        this.reviewService.saveReview(review);
        return this.getReview(review.getReservationId());
    }

    @GetMapping("review/pending")
    public Collection<Review> getPendingReviews() {
        return this.reviewService.getPendingReviews();
    }

    @PutMapping("review/accept/{reviewId}")
    public Review acceptReview(@PathVariable("reviewId") Long reviewId) {
        return this.reviewService.changeState(reviewId, ReviewStatusEnum.ACCEPTED);
    }

    @PutMapping("review/reject/{reviewId}")
    public Review rejectReview(@PathVariable("reviewId") Long reviewId) {
        return this.reviewService.changeState(reviewId, ReviewStatusEnum.REJECTED);
    }
}
