package com.halal.booking.controller;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingException;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.model.dto.Place;
import com.halal.booking.model.entity.*;
import com.halal.booking.service.HotelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Collection;
import java.util.List;

@RestController
@RequestMapping("api/hotel")
public class HotelController {
    private final HotelService hotelService;

    @Autowired
    public HotelController(HotelService hotelService) {
        this.hotelService = hotelService;
    }

    @PostMapping("search")
    public Collection<Hotel> searchHotels(@Valid @RequestBody HotelSearchQuery hotelSearchQuery) {
        return hotelService.searchHotels(hotelSearchQuery);
    }

    @GetMapping("places")
    public List<Place> getPlaces() {
        return this.hotelService.loadPlaces();
    }

    @GetMapping("{hotelId}")
    public Hotel getHotel(@PathVariable("hotelId") Long hotelId) {
        return hotelService.getHotel(hotelId);
    }


    @GetMapping
    public Collection<Hotel> hotels() {
        return hotelService.getHotels();
    }

    @PostMapping
    public Hotel createHotel(@Valid @RequestBody Hotel hotel) throws BookingException {
        return hotelService.saveHotel(hotel);
    }

    @PutMapping("{hotelId}")
    public Hotel updateHotel(@PathVariable("hotelId") Long hotelId, @Valid @RequestBody Hotel hotel) throws BookingException {
        if(hotelId == null || !ObjectUtils.nullSafeEquals(hotelId, hotel.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return hotelService.saveHotel(hotel);
    }

    @DeleteMapping("{hotelId}")
    public void deleteHotel(@PathVariable("hotelId") Long hotelId) throws BookingException {
        hotelService.deleteHotel(hotelId);
    }

    @PostMapping("room")
    public Room createRoom(@Valid @RequestBody Room room) throws BookingException {
        return hotelService.saveRoom(room);
    }

    @PutMapping("room/{roomId}")
    public Room updateRoom(@PathVariable("roomId") Long roomId, @Valid @RequestBody Room room) throws BookingException {
        if(roomId == null || !ObjectUtils.nullSafeEquals(roomId, room.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return hotelService.saveRoom(room);
    }

    @DeleteMapping("room/{roomId}")
    public void deleteRoom(@PathVariable("roomId") Long roomId) throws BookingException {
        hotelService.deleteRoom(roomId);
    }

    @PostMapping("alternative")
    public RoomAlternative createAlternative(@Valid @RequestBody RoomAlternative alternative) throws BookingException {
        return hotelService.saveAlternative(alternative);
    }

    @PutMapping("alternative/{alternativeId}")
    public RoomAlternative updateAlternative(@PathVariable("alternativeId") Long alternativeId, @Valid @RequestBody RoomAlternative alternative) throws BookingException {
        if(alternativeId == null || !ObjectUtils.nullSafeEquals(alternativeId, alternative.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return hotelService.saveAlternative(alternative);
    }

    @DeleteMapping("alternative/{alternativeId}")
    public void deleteAlternative(@PathVariable("alternativeId") Long alternativeId) throws BookingException {
        hotelService.deleteAlternative(alternativeId);
    }

    @GetMapping("reviews/{hotelId}")
    public List<Review> getHotelReviews(@PathVariable("hotelId") Long hotelId) {
        return hotelService.getHotelReviews(hotelId);
    }

    @PostMapping("rates/{roomId}")
    public Collection<RoomRate> saveRoomRates(@PathVariable("roomId") Long roomId, @RequestBody RoomRate[] rates) {
        return hotelService.saveRoomRates(roomId, rates);
    }
}
