package com.halal.booking.controller;

import com.halal.booking.model.dto.ChangePasswordDTO;
import com.halal.booking.model.dto.RecoverPasswordDTO;
import com.halal.booking.model.dto.SocialToken;
import com.halal.booking.service.AuthenticationService;
import com.halal.booking.usermanagement.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("api/authentication")
public class AuthenticationController {

    private final AuthenticationService authenticationService;

    @Autowired
    public AuthenticationController(AuthenticationService authenticationService) {
        this.authenticationService = authenticationService;
    }

    @PostMapping("authenticate")
    public ResponseEntity<User> authenticate(@Valid @RequestBody User user) {
        UsernamePasswordAuthenticationToken authenticationToken =
                new UsernamePasswordAuthenticationToken(user.getEmail(), user.getPassword());
        return this.authenticationService.authenticateUser(authenticationToken);
    }

    @PostMapping("register")
    public ResponseEntity<User> register(@Valid @RequestBody User user) {
        return this.authenticationService.registerBasicUser(user, false);
    }

    @GetMapping("activate")
    public ResponseEntity<User> activate(@RequestParam("ticket") String ticket) {
        return this.authenticationService.activateUser(ticket);
    }

    @PostMapping("google")
    public ResponseEntity<User> googleSignIn(@Valid @RequestBody SocialToken socialToken) {
        return this.authenticationService.signInOrRegisterGoogleUser(socialToken.getToken());
    }

    @PostMapping("facebook")
    public ResponseEntity<User> facebookSignIn(@Valid @RequestBody SocialToken socialToken) {
        return this.authenticationService.signInOrRegisterFacebookUser(socialToken.getToken());
    }

    @GetMapping("user")
    public User getUser() {
        return this.authenticationService.getCurrentUser();
    }

    @PutMapping("password")
    public void changePassword(@Valid @RequestBody ChangePasswordDTO changePasswordDTO) {
        this.authenticationService.changePassword(changePasswordDTO.getCurrentPassword(), changePasswordDTO.getNewPassword(), authenticationService.getCurrentUser());
    }

    @PostMapping("forgotPassword")
    public void forgotPassword(@RequestBody String email) {
        this.authenticationService.sendRecoverPasswordLink(email);
    }

    @PostMapping("recoverPassword")
    public void recoverPassword(@Valid @RequestBody RecoverPasswordDTO recoverPasswordDTO) {
        this.authenticationService.recoverPassword(recoverPasswordDTO);
    }
}
