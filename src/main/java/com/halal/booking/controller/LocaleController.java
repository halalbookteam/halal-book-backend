package com.halal.booking.controller;

import com.halal.booking.model.entity.Currency;
import com.halal.booking.model.entity.Language;
import com.halal.booking.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Collection;

@RestController
@RequestMapping("api/locale")
public class LocaleController {
    private CacheService cacheService;

    @Autowired
    public LocaleController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping("languages")
    public Collection<Language> getLanguages() {
        return this.cacheService.getLanguages();
    }

    @GetMapping("currencies")
    public Collection<Currency> getCurrencies() {
        return this.cacheService.getCurrencies();
    }
}
