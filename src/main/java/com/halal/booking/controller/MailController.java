package com.halal.booking.controller;

import com.halal.booking.model.dto.MailData;
import com.halal.booking.model.dto.MailTemplate;
import com.halal.booking.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/mail")
public class MailController {

    private final MailService mailService;

    @Autowired
    public MailController(MailService mailService) {
        this.mailService = mailService;
    }

    @PostMapping
    public void sendEmail(@Valid @RequestBody MailData mailData) {
      this.mailService.sendMail(mailData);
    }

    @GetMapping
    public List<MailTemplate> getMailTemplates() {
        return this.mailService.getMailTemplates();
    }
}
