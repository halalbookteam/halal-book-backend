package com.halal.booking.controller;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.model.entity.Facility;
import com.halal.booking.model.entity.FacilityCategory;
import com.halal.booking.service.FacilityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/facility")
public class FacilityController {
    private final FacilityService facilityService;

    @Autowired
    public FacilityController(FacilityService facilityService) {
        this.facilityService = facilityService;
    }

    @PostMapping("facility")
    public Facility createFacility(@Valid @RequestBody Facility facility) {
        return facilityService.saveFacility(facility);
    }

    @PutMapping("facility/{id}")
    public Facility updateFacility(@PathVariable("id") Long id, @Valid @RequestBody Facility facility) {
        if(id == null || !ObjectUtils.nullSafeEquals(id, facility.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return facilityService.saveFacility(facility);
    }

    @DeleteMapping("facility/{id}")
    public void deleteFacility(@PathVariable("id") Long id) {
        facilityService.deleteFacility(id);
    }

    @GetMapping("categories")
    public List<FacilityCategory> getCategories() {
        return facilityService.getCategoryList();
    }

    @PostMapping("category")
    public FacilityCategory createCategory(@Valid @RequestBody FacilityCategory facilityCategory) {
        return facilityService.saveCategory(facilityCategory);
    }

    @PutMapping("category/{id}")
    public FacilityCategory updateCategory(@PathVariable("id") Long id, @Valid @RequestBody FacilityCategory facilityCategory) {
        if(id == null || !ObjectUtils.nullSafeEquals(id, facilityCategory.getId())) {
            throw new BookingRuntimeException(MessageTypeEnum.INVALID_PATH_FOR_OBJECT_ID);
        }
        return facilityService.saveCategory(facilityCategory);
    }

    @DeleteMapping("category/{id}")
    public void deleteCategory(@PathVariable("id") Long id) {
        facilityService.deleteCategory(id);
    }



}
