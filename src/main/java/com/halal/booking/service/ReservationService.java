package com.halal.booking.service;

import com.halal.booking.enums.*;
import com.halal.booking.exception.BookingException;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.external.payment.stripe.entity.StripeCharge;
import com.halal.booking.external.payment.stripe.entity.StripeCustomer;
import com.halal.booking.external.payment.stripe.entity.StripeSource;
import com.halal.booking.external.payment.stripe.service.StripeService;
import com.halal.booking.model.dto.MailData;
import com.halal.booking.model.dto.MailTemplate;
import com.halal.booking.model.dto.Price;
import com.halal.booking.model.entity.*;
import com.halal.booking.repository.ReservationRepository;
import com.halal.booking.repository.RoomAlternativeRepository;
import com.halal.booking.repository.RoomRateRepository;
import com.halal.booking.usermanagement.model.User;
import lombok.Builder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class ReservationService {
    private ReservationRepository reservationRepository;
    private AuthenticationService authenticationService;
    private RequestService requestService;
    private RoomAlternativeRepository alternativeRepository;
    private HotelService hotelService;
    private UserActionService userActionService;
    private MailService mailService;
    private CacheService cacheService;
    private DateTimeUtils dateTimeUtils;
    private RoomRateRepository roomRateRepository;
    private ChannelManagerService channelManagerService;
    private StripeService stripeService;

    @Value("${mail.templatePath}")
    private String templatePath;

    @Value("${schedule.cancelTimeToFixProblem}")
    private Long cancelTimeToFixProblem;

    @Value("${schedule.daysToAuthorize}")
    private Long daysToAuthorize;

    @Autowired
    public ReservationService(ReservationRepository reservationRepository,
                              AuthenticationService authenticationService, RequestService requestService,
                              RoomAlternativeRepository alternativeRepository, HotelService hotelService,
                              UserActionService userActionService, MailService mailService, CacheService cacheService,
                              DateTimeUtils dateTimeUtils, RoomRateRepository roomRateRepository,
                              ChannelManagerService channelManagerService, StripeService stripeService) {
        this.reservationRepository = reservationRepository;
        this.authenticationService = authenticationService;
        this.requestService = requestService;
        this.alternativeRepository = alternativeRepository;
        this.hotelService = hotelService;
        this.userActionService = userActionService;
        this.mailService = mailService;
        this.cacheService = cacheService;
        this.dateTimeUtils = dateTimeUtils;
        this.roomRateRepository = roomRateRepository;
        this.channelManagerService = channelManagerService;
        this.stripeService = stripeService;
    }

    @Transactional
    public Reservation saveReservation(Reservation reservation) {
        User currentUser = this.authenticationService.getCurrentUser();
        reservation.getRoomReservations().forEach(roomReservation -> roomReservation.setReservation(reservation));
        loadAlternatives(reservation);
        Price price = calculatePrice(reservation);
        reservation.setPrice(price);
        UserAction userAction;
        if (reservation.getId() != null) {
            Reservation existingReservation = this.reservationRepository.findByIdAndUser(reservation.getId(), currentUser);
            if (existingReservation == null) {
                throw new BookingRuntimeException(MessageTypeEnum.RESERVATION_NOT_FOUND);
            }
            existingReservation.setRoomReservations(reservation.getRoomReservations());
            existingReservation.setSource(reservation.getSource());
            existingReservation.setPerson(reservation.getPerson());
            existingReservation.setSpecialRequests(reservation.getSpecialRequests());
            userAction = UserAction.builder().actionType(ActionTypeEnum.RESERVATION_MODIFIED).user(currentUser).build();
            userAction.setEntityId(reservation.getId());
            this.userActionService.saveUserAction(userAction);

            return this.reservationRepository.save(existingReservation);
        }
        reservation.setCreationTime(new Date());
        reservation.setStatus(ReservationStatusEnum.APPLIED);
        reservation.setLanguage(this.cacheService.getCurrentLanguage());
        userAction = UserAction.builder().actionType(ActionTypeEnum.RESERVATION_STATE_CHANGED).user(currentUser).newValue(reservation.getStatus().toString()).build();

        if (currentUser != null) {
            reservation.setUser(currentUser);
        }
        this.reservationRepository.save(reservation);
        reservation.setReservationNo(reservation.getId().toString());
        this.reservationRepository.save(reservation);
        userAction.setEntityId(reservation.getId());
        this.userActionService.saveUserAction(userAction);

        return reservation;
    }

    private Price calculatePrice(Reservation reservation) {
        Price totalPrice = new Price();
        Map<Room, Long> roomCountMap = reservation.getRoomReservations().stream()
                .map(roomReservation -> roomReservation.getAlternative().getRoom())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Map<Room, List<RoomRate>> roomRatesMap = new HashMap<>();
        return reservation.getRoomReservations().stream()
                .map(roomReservation -> {
                    RoomAlternative alternative = roomReservation.getAlternative();
                    Room room = alternative.getRoom();
                    List<RoomRate> roomRates = roomRatesMap.get(room);
                    if (roomRates == null) {
                        roomRates = fetchRates(room, reservation.getCheckIn(), reservation.getCheckOut(), roomCountMap.get(room).intValue());
                        roomRatesMap.put(room, roomRates);
                    }
                    Price totalBasePrice = new Price();
                    return roomRates.stream().map(RoomRate::getBasePrice)
                            .reduce(totalBasePrice, Price::add)
                            .multiply(alternative.getBasePriceRatio());
                })
                .reduce(totalPrice, Price::add);
    }

    private List<RoomRate> fetchRates(Room room, Date checkIn, Date checkOut, int roomCount) {
        int nightCount = dateTimeUtils.getNightCount(checkIn, checkOut);
        List<RoomRate> roomRates = this.roomRateRepository.findRates(room, checkIn, checkOut, nightCount, roomCount);
        if (roomRates.size() != nightCount) {
            String languageCode = this.requestService.getLanguageCode();
            throw new BookingRuntimeException(MessageTypeEnum.UNAVAILABLE_INTERVALS, room.getId(), dateTimeUtils.formatDate(languageCode, checkIn), dateTimeUtils.formatDate(languageCode, checkOut));
        }

        return roomRates;
    }

    @Transactional
    public Reservation cancelReservation(Long id) throws BookingUnauthorizedUserException {
        User currentUser = this.authenticationService.getCurrentUser();
        Reservation reservation = this.reservationRepository.getOne(id);
        UserAction userAction = UserAction.builder().entityId(id).actionType(ActionTypeEnum.RESERVATION_STATE_CHANGED)
                .user(currentUser).oldValue(reservation.getStatus().toString())
                .newValue(ReservationStatusEnum.CANCELLED.toString()).build();
        boolean isAdmin = currentUser.hasPermission(PermissionEnum.ALL_RESERVATIONS_CRUD);
        boolean isOwner = reservation.getUser().getId().equals(currentUser.getId());
        boolean hotelOwner = currentUser.hasPermission(PermissionEnum.HOTEL_RESERVATIONS_CRUD);
        if (isAdmin || isOwner || (hotelOwner && reservation.getStatus().equals(ReservationStatusEnum.CONFIRMED))) {
            reservation.setStatus(ReservationStatusEnum.CANCELLED);
        } else {
            throw new BookingUnauthorizedUserException();
        }

        this.userActionService.saveUserAction(userAction);

        reservation.setCancellationTime(userAction.getTime());
        this.channelManagerService.sendBooking(reservation);

        Map<Room, Long> roomCountMap = reservation.getRoomReservations().stream()
                .map(roomReservation -> roomReservation.getAlternative().getRoom())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        roomCountMap.keySet().forEach(room -> {
            Date lastNight = new DateTimeUtils().add(reservation.getCheckOut(), Calendar.DATE, -1);
            List<RoomRate> rates = this.roomRateRepository.findRatesByRoomAndDateBetween(room, reservation.getCheckIn(), lastNight);
            Long roomCount = roomCountMap.get(room);
            rates.forEach(roomRate -> {
                roomRate.setAvailableRoomCount(roomRate.getAvailableRoomCount() + roomCount.intValue());
                this.roomRateRepository.save(roomRate);
            });
        });

        this.reservationRepository.save(reservation);

        String reservationNo = reservation.getReservationNo();
        Person person = reservation.getPerson();
        Hotel hotel = reservation.getHotel();
        String message = cacheService.getMessage(MessageTypeEnum.RESERVATION_CANCELLED, reservationNo);
        this.mailService.sendMail(MailData.builder()
                .subject(message)
                .recipientType(RecipientEnum.DEFINED_USER)
                .recipientList(new HashSet<>(Collections.singletonList(person.getEmail())))
                .template(MailTemplate.builder()
                        .file(MailTemplateEnum.RESERVATION.getTemplateName())
                        .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, person.getFullName()))
                        .parameter("tableRows", getReservationDetails(reservation))
                        .parameter("messages", new String[]{message})
                        .build())
                .build());

        if (reservation.getStatus().equals(ReservationStatusEnum.CONFIRMED)) {
            hotel.getUsers().forEach(user ->
                    this.mailService.sendMail(MailData.builder()
                            .subject(message)
                            .recipientType(RecipientEnum.DEFINED_USER)
                            .recipientList(new HashSet<>(Collections.singletonList(user.getEmail())))
                            .template(MailTemplate.builder()
                                    .file(MailTemplateEnum.RESERVATION.getTemplateName())
                                    .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, user.getProfile().getDisplayName()))
                                    .parameter("tableRows", getReservationDetails(reservation))
                                    .parameter("messages", new String[]{message})
                                    .build())
                            .build()));
        }

        return reservation;
    }

    private Object getReservationDetails(Reservation reservation) {
        List<TableRow> rows = new ArrayList<>();
        Hotel hotel = reservation.getHotel();
        Address address = hotel.getAddress();
        Language currentLanguage = this.cacheService.getCurrentLanguage();
        if (currentLanguage == null) {
            currentLanguage = reservation.getLanguage();
        }
        AddressTranslation addressTranslation = Optional.ofNullable(address.getTranslation(currentLanguage.getCode()))
                .orElse(address.getDefaultTranslation());

        rows.add(TableRow.builder().label(this.cacheService.getMessage(currentLanguage, MessageTypeEnum.GUEST)).value(reservation.getPerson().getFullName()).build());
        rows.add(TableRow.builder().label(this.cacheService.getMessage(currentLanguage, MessageTypeEnum.HOTEL)).value(hotel.getName()).build());
        rows.add(TableRow.builder().label(this.cacheService.getMessage(currentLanguage, MessageTypeEnum.PLACE)).value(addressTranslation.getShort()).build());
        rows.add(TableRow.builder().label(this.cacheService.getMessage(currentLanguage, MessageTypeEnum.CHECK_IN)).value(this.dateTimeUtils.formatDate(currentLanguage.getCode(), reservation.getCheckIn())).build());
        rows.add(TableRow.builder().label(this.cacheService.getMessage(currentLanguage, MessageTypeEnum.CHECK_OUT)).value(this.dateTimeUtils.formatDate(currentLanguage.getCode(), reservation.getCheckOut())).build());
        return rows;
    }

    private void loadAlternatives(Reservation reservation) {
        reservation.getRoomReservations().forEach(roomReservation ->
                roomReservation.setAlternative(alternativeRepository.getOne(roomReservation.getAlternative().getId())));
    }

    @Transactional
    public StripeSource savePaymentDetail(Long reservationId, StripeSource stripeSource) throws BookingException {
        User currentUser = this.authenticationService.getCurrentUser();
        Reservation existingReservation = this.reservationRepository.findByIdAndUser(reservationId, currentUser);
        if (existingReservation == null) {
            throw new BookingRuntimeException(MessageTypeEnum.RESERVATION_NOT_FOUND);
        }
        Person person = existingReservation.getPerson();
        String reservationNo = existingReservation.getReservationNo();

        if (stripeSource.getId() != null) {
            existingReservation.setSource(this.stripeService.getSourceById(stripeSource.getId()));
        } else {
            StripeCustomer customer = null;
            if (existingReservation.getUser() != null) {
                customer = this.stripeService.getCustomer(existingReservation.getUser());
            }
            if (customer == null) {
                existingReservation.setSource(this.stripeService.createSourceWithCustomer(stripeSource.getSourceId(), existingReservation.getPerson().getEmail(), existingReservation.getUser()));
            } else {
                existingReservation.setSource(this.stripeService.createSource(stripeSource.getSourceId(), customer));
            }
        }

        Map<Room, List<RoomRate>> roomRatesMap = this.roomRateRepository.findRatesByRoom_HotelAndDateBetweenOrderByDate(existingReservation.getHotel(), existingReservation.getCheckIn(), existingReservation.getLastNight())
                .stream().collect(Collectors.groupingBy(RoomRate::getRoom));

        Map<Room, Long> requiredRoomCountMap = existingReservation.getRoomReservations().stream()
                .map(roomReservation -> roomReservation.getAlternative().getRoom())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        Map<Room, Long> availableRoomCountMap = roomRatesMap.keySet().stream()
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        for (Room room : requiredRoomCountMap.keySet()) {
            Long requiredRoomCount = requiredRoomCountMap.get(room);
            Long availableRoomCount = availableRoomCountMap.get(room);
            if(availableRoomCount < requiredRoomCount) {
                throw new BookingRuntimeException(MessageTypeEnum.NOT_ENOUGH_ROOM, room.getDefaultTranslation().getName(), requiredRoomCount, availableRoomCount);
            }
        }

        Map<Room, Price> roomPriceMap = roomRatesMap.keySet().stream()
                .collect(Collectors.toMap(e -> e, e -> roomRatesMap.get(e).stream()
                        .map(RoomRate::getBasePrice).reduce(new Price(), Price::add)));

        Map<Boolean, List<RoomReservation>> roomReservationsPaymentMap = existingReservation.getRoomReservations().stream()
                .collect(Collectors.groupingBy(roomReservation -> roomReservation.getAlternative().getPaymentOption().isNonRefundable()));

        StripeCharge refundableCharge = null;
        if (isReservationReadyForAuthorization(existingReservation)) {
            refundableCharge = this.chargeRoomReservations(existingReservation, roomReservationsPaymentMap.get(false), roomPriceMap);
            existingReservation.setRefundableCharge(refundableCharge);
            if(refundableCharge.isFailed()) {
                throw new BookingException(refundableCharge.getLastSuccessfulAction().getErrorCode());
            }
        }
        StripeCharge nonRefundableCharge = this.chargeRoomReservations(existingReservation, roomReservationsPaymentMap.get(true), roomPriceMap);
        existingReservation.setNonRefundableCharge(nonRefundableCharge);
        if(nonRefundableCharge != null && nonRefundableCharge.isFailed()) {
            if(refundableCharge != null) {
                this.stripeService.refund(refundableCharge);
            }
            throw new BookingException(nonRefundableCharge.getLastSuccessfulAction().getErrorCode());
        }

        if (!existingReservation.getStatus().equals(ReservationStatusEnum.CONFIRMED)) {
            UserAction userAction = UserAction.builder().actionType(ActionTypeEnum.RESERVATION_STATE_CHANGED)
                    .user(currentUser).entityId(existingReservation.getId()).oldValue(existingReservation.getStatus().toString())
                    .newValue(ReservationStatusEnum.CONFIRMED.toString()).build();
            this.userActionService.saveUserAction(userAction);
        }
        existingReservation.setStatus(ReservationStatusEnum.CONFIRMED);


        this.channelManagerService.sendBooking(existingReservation);

        Map<Room, Long> roomCountMap = existingReservation.getRoomReservations().stream()
                .map(roomReservation -> roomReservation.getAlternative().getRoom())
                .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));

        roomCountMap.keySet().forEach(room -> {
            Long roomCount = roomCountMap.get(room);
            roomRatesMap.get(room).forEach(roomRate -> {
                roomRate.setAvailableRoomCount(roomRate.getAvailableRoomCount() - roomCount.intValue());
                this.roomRateRepository.save(roomRate);
            });
        });

        String message = cacheService.getMessage(MessageTypeEnum.RESERVATION_CONFIRMED, reservationNo);
        String thanks = cacheService.getMessage(MessageTypeEnum.THANKS_FOR_CHOOSE_US);
        this.mailService.sendMail(MailData.builder()
                .subject(message)
                .recipientType(RecipientEnum.DEFINED_USER)
                .recipientList(new HashSet<>(Collections.singletonList(person.getEmail())))
                .template(MailTemplate.builder()
                        .file(MailTemplateEnum.RESERVATION.getTemplateName())
                        .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, person.getFullName()))
                        .parameter("tableRows", getReservationDetails(existingReservation))
                        .parameter("messages", new String[]{message, thanks})
                        .build())
                .build());

        existingReservation.getHotel().getUsers().forEach(user ->
                this.mailService.sendMail(MailData.builder()
                        .subject(message)
                        .recipientType(RecipientEnum.DEFINED_USER)
                        .recipientList(new HashSet<>(Collections.singletonList(user.getEmail())))
                        .template(MailTemplate.builder()
                                .file(MailTemplateEnum.RESERVATION.getTemplateName())
                                .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, user.getProfile().getDisplayName()))
                                .parameter("tableRows", getReservationDetails(existingReservation))
                                .parameter("messages", new String[]{message})
                                .build())
                        .build()));

        return existingReservation.getSource();
    }

    private boolean isReservationReadyForAuthorization(Reservation reservation) {
        return this.dateTimeUtils.getNightCount(new Date(), reservation.getCheckIn()) <= this.daysToAuthorize;
    }

    private StripeCharge chargeRoomReservations(Reservation reservation, List<RoomReservation> roomReservations, Map<Room, Price> roomPriceMap) {
        StripeCharge stripeCharge = null;
        if (roomReservations != null && !roomReservations.isEmpty()) {
            Price totalPrice = roomReservations.stream()
                    .map(RoomReservation::getAlternative)
                    .map(alternative -> alternative.calculatePrice(roomPriceMap.get(alternative.getRoom())))
                    .reduce(new Price(), Price::add);
            boolean capture = roomReservations.get(0).getAlternative().getPaymentOption().isNonRefundable();
            stripeCharge = this.stripeService.charge(totalPrice, reservation.getSource(), capture);
            stripeCharge.setRoomReservationList(roomReservations);
        }
        return stripeCharge;
    }

    @Transactional(readOnly = true)
    public Collection<Reservation> getReservations() throws BookingUnauthorizedUserException {
        User currentUser = this.authenticationService.getCurrentUser();
        if (currentUser.hasPermission(PermissionEnum.ALL_RESERVATIONS_CRUD)) {
            return this.reservationRepository.findAll();
        } else if (currentUser.hasPermission(PermissionEnum.HOTEL_RESERVATIONS_CRUD)) {
            Set<Hotel> userHotels = this.hotelService.getUserHotels();
            return this.reservationRepository.findByHotelInAndStatusIn(userHotels, Collections.singletonList(ReservationStatusEnum.CONFIRMED));
        } else {
            throw new BookingUnauthorizedUserException();
        }
    }

    @Transactional(readOnly = true)
    public Collection<Reservation> getMyReservations() {
        User currentUser = this.authenticationService.getCurrentUser();
        return this.reservationRepository.findByUserOrderByCheckInDesc(currentUser);
    }

    @Scheduled(fixedRateString = "${schedule.checkReservationsForPayment}")
    @Transactional
    public void checkReservationsForPayment() {
        this.checkReservationsForAuthorization();
    }

    private void checkReservationsForAuthorization() {
        List<Reservation> reservations = this.reservationRepository.getReservationsTobeAuthorized(this.daysToAuthorize.doubleValue());
        reservations.forEach(reservation -> {
            Map<Room, Price> roomPriceMap = this.roomRateRepository.findRatesByRoom_HotelAndDateBetweenOrderByDate(reservation.getHotel(), reservation.getCheckIn(), reservation.getLastNight())
                    .stream().collect(Collectors.groupingBy(RoomRate::getRoom, Collectors.reducing(new Price(), RoomRate::getBasePrice, Price::add)));
            List<RoomReservation> roomReservations = reservation.getRoomReservations().stream()
                    .filter(roomReservation -> !roomReservation.getAlternative().getPaymentOption().isNonRefundable())
                    .collect(Collectors.toList());

            reservation.setRefundableCharge(this.chargeRoomReservations(reservation, roomReservations, roomPriceMap));
        });
    }

    @Scheduled(fixedRateString = "${schedule.checkReservationsToBeClosedInterval}")
    @Transactional
    public void checkReservationsToBeClosed() {
        List<Reservation> reservationList = this.reservationRepository.findByStatusAndCheckOutBefore(ReservationStatusEnum.CONFIRMED, new Date());
        reservationList.forEach(reservation -> {
            reservation.setStatus(ReservationStatusEnum.CLOSED);
            UserAction userAction = UserAction.builder().actionType(ActionTypeEnum.RESERVATION_STATE_CHANGED)
                    .entityId(reservation.getId()).oldValue(ReservationStatusEnum.CONFIRMED.toString())
                    .newValue(ReservationStatusEnum.CLOSED.toString()).build();
            this.reservationRepository.save(reservation);
            this.userActionService.saveSystemAction(userAction);

            String reservationNo = reservation.getReservationNo();
            Person person = reservation.getPerson();
            String message = cacheService.getMessage(reservation.getLanguage(), MessageTypeEnum.RESERVATION_COMPLETED, reservationNo);
            String reviewMessage = cacheService.getMessage(reservation.getLanguage(), MessageTypeEnum.PLEASE_REVIEW_HOTEL);
            String thanks = cacheService.getMessage(reservation.getLanguage(), MessageTypeEnum.THANKS_FOR_CHOOSE_US);
            this.mailService.sendMail(MailData.builder()
                    .subject(message)
                    .recipientType(RecipientEnum.DEFINED_USER)
                    .recipientList(new HashSet<>(Collections.singletonList(person.getEmail())))
                    .template(MailTemplate.builder()
                            .file(MailTemplateEnum.RESERVATION.getTemplateName())
                            .parameter("greetings", cacheService.getMessage(reservation.getLanguage(), MessageTypeEnum.GREETINGS, person.getFullName()))
                            .parameter("tableRows", getReservationDetails(reservation))
                            .parameter("messages", new String[]{message, thanks, reviewMessage})
                            .build())
                    .build());
        });
    }

    @Builder
    @Data
    private static class TableRow {
        private String label;
        private String value;
    }
}
