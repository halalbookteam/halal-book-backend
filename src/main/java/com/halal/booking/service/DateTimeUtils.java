package com.halal.booking.service;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.DurationFieldType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

@Service
public class DateTimeUtils {
    private RequestService requestService;

    public DateTimeUtils() {
    }

    @Autowired
    public DateTimeUtils(RequestService requestService) {
        this.requestService = requestService;
    }

    public Date min(Date dateTime1, Date dateTime2) {
        return dateTime1.before(dateTime2) ? dateTime1 : dateTime2;
    }

    public int getDifference(Date start, Date end, DurationFieldType type) {
        return Days.daysBetween(new DateTime(start), new DateTime(end)).get(type);
    }

    public int getNightCount(Date start, Date end) {
        return this.getDifference(start, end, DurationFieldType.days());
    }

    public String formatDate(Date date) {
        Locale locale = new Locale(this.requestService.getLanguageCode());
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        String pattern       = ((SimpleDateFormat)formatter).toPattern();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern, locale);
        return new Date(date.getTime()).toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime().format(dtf);
    }

    public String formatDate(String languageCode, Date date) {
        Locale locale = new Locale(languageCode);
        DateFormat formatter = DateFormat.getDateInstance(DateFormat.SHORT, locale);
        String pattern       = ((SimpleDateFormat)formatter).toPattern();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern(pattern, locale);
        return new Date(date.getTime()).toInstant().atOffset(ZoneOffset.UTC).toLocalDateTime().format(dtf);
    }

    public Date add(Date date, int unit, int amount) {
        Calendar instance = Calendar.getInstance();
        instance.setTime(date);
        instance.add(unit, amount);
        return instance.getTime();
    }

}
