package com.halal.booking.service;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.List;

public interface StorageService {

    void init();

    String store(MultipartFile file);

    List<String> store(MultipartFile[] files);

    Path load(String filename);

    Resource loadAsResource(String filename);

    void deleteAll();

}