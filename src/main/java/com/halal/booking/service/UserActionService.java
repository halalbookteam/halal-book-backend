package com.halal.booking.service;


import com.halal.booking.enums.UserTypeEnum;
import com.halal.booking.model.entity.UserAction;
import com.halal.booking.repository.UserActionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Service
public class UserActionService {
    private final HttpServletRequest request;
    private UserActionRepository userActionRepository;

    @Autowired
    public UserActionService(HttpServletRequest request, UserActionRepository userActionRepository) {
        this.request = request;
        this.userActionRepository = userActionRepository;
    }

    public UserAction saveUserAction(UserAction userAction) {
        if(userAction.getUserType() == null) {
            userAction.setUserType(userAction.getUser() == null ? UserTypeEnum.ANONYMOUS : UserTypeEnum.REAL);
        }
        if(userAction.getTime() == null) {
            userAction.setTime(new Date());
        }
        if(userAction.getIp() == null && !userAction.getUserType().equals(UserTypeEnum.SYSTEM)) {
            userAction.setIp(request.getRemoteHost());
        }

        return this.userActionRepository.save(userAction);
    }

    public UserAction saveSystemAction(UserAction userAction) {
        userAction.setUser(null);
        userAction.setUserType(UserTypeEnum.SYSTEM);
        return this.saveUserAction(userAction);
    }
}
