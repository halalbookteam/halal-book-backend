package com.halal.booking.service;

import com.google.api.client.googleapis.auth.oauth2.GoogleIdToken;
import com.google.api.client.googleapis.auth.oauth2.GoogleIdTokenVerifier;
import com.google.api.client.http.apache.ApacheHttpTransport;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.gson.Gson;
import com.halal.booking.enums.MailTemplateEnum;
import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.enums.RecipientEnum;
import com.halal.booking.enums.RoleEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.model.dto.MailData;
import com.halal.booking.model.dto.MailTemplate;
import com.halal.booking.model.dto.RecoverPasswordDTO;
import com.halal.booking.model.entity.Photo;
import com.halal.booking.security.UserNotActivatedException;
import com.halal.booking.security.jwt.JWTConfigurer;
import com.halal.booking.security.jwt.TokenProvider;
import com.halal.booking.usermanagement.model.Role;
import com.halal.booking.usermanagement.model.User;
import com.halal.booking.usermanagement.service.UserService;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.social.facebook.api.Facebook;
import org.springframework.social.facebook.api.impl.FacebookTemplate;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class AuthenticationService {
    private final UserService userService;
    private final AuthenticationManager authenticationManager;
    private PasswordEncoder passwordEncoder;
    private TokenProvider tokenProvider;
    private CacheService cacheService;

    @Value("${security.google.clientId}")
    private String googleClientId;

    @Value("${spring.application.hostURL}")
    private String hostURL;
    private MailService mailService;

    @Autowired
    public AuthenticationService(UserService userService, AuthenticationManager authenticationManager, PasswordEncoder passwordEncoder, TokenProvider tokenProvider, CacheService cacheService, MailService mailService) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.passwordEncoder = passwordEncoder;
        this.tokenProvider = tokenProvider;
        this.cacheService = cacheService;
        this.mailService = mailService;
    }

    public User getCurrentUser() {
        String currentUserEmail = this.tokenProvider.getCurrentUserEmail();
        if(currentUserEmail == null) {
            return null;
        }
        return this.userService.getUserByEmail(currentUserEmail);
    }

    public ResponseEntity<User> authenticateUser(Authentication authentication) {
        if(!authentication.isAuthenticated()) {
            authentication = this.authenticationManager.authenticate(authentication);
        }
        SecurityContextHolder.getContext().setAuthentication(authentication);

        User userFromDb = userService.findUserByEmail(authentication.getName());
        if (!userFromDb.getActive()) {
            throw new UserNotActivatedException("User " + userFromDb.getEmail() + " was not activated");
        }

        String jwt = this.tokenProvider.createToken(authentication);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add(JWTConfigurer.AUTHORIZATION_HEADER, "Bearer " + jwt);

        return new ResponseEntity<>(userFromDb, httpHeaders, HttpStatus.OK);
    }

    public ResponseEntity<User> registerBasicUser(User user, boolean active) {
        Role role = userService.findRolesByName(Collections.singletonList(RoleEnum.BASIC.name())).get(0);
        user.setRoles(new ArrayList<>());
        user.getRoles().add(role);
        user.setActive(active);
        user = userService.saveUser(user);
        if(user.getPassword() != null) {
            user.setPasswordHash(new BCryptPasswordEncoder().encode(user.getPassword()));
            this.sendActivationToken(user);
        }

        return ResponseEntity.ok(user);
    }

    public ResponseEntity<User> activateUser(String ticket) {
        String email = tokenProvider.resolveActivationToken(ticket);
        User user = this.userService.findUserByEmail(email);
        if(user == null) {
            throw new UsernameNotFoundException("User not found");
        }
        user.setActive(true);

        Authentication authenticationToken = createAuthenticationToken(user);

        return this.authenticateUser(authenticationToken);
    }

    public void sendActivationToken(User user) {
        String token = tokenProvider.createActivationToken(user.getEmail());
        this.mailService.sendMail(MailData.builder()
                .subject(cacheService.getMessage(MessageTypeEnum.REGISTRATION_TITLE))
                .recipientType(RecipientEnum.DEFINED_USER)
                .recipientList(new HashSet<>(Collections.singletonList(user.getEmail())))
                .template(MailTemplate.builder()
                        .file(MailTemplateEnum.CLICK_LINK.getTemplateName())
                        .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, user.getProfile().getDisplayName()))
                        .parameter("message", cacheService.getMessage(MessageTypeEnum.CLICK_TO_ACTIVATE))
                        .parameter("link1", this.hostURL + "/activateAccount?ticket=" + token)
                        .parameter("link2", this.hostURL + "/activateAccount?ticket=" + token)
                        .build())
                .build());
    }

    private Authentication createAuthenticationToken(User user) {
        List<GrantedAuthority> grantedAuthorities = user.getRoles().stream()
                .map(role -> new SimpleGrantedAuthority("ROLE_" + role.getName()))
                .collect(Collectors.toList());

        return new UsernamePasswordAuthenticationToken
                (user.getEmail(), user.getPassword(), grantedAuthorities);
    }

    public ResponseEntity<User> signInOrRegisterGoogleUser(String token) {
        GoogleIdToken.Payload payload = verifyGoogleToken(token);
        String email = payload.getEmail();
        User user = this.userService.findUserByEmail(email);
        if (user == null) {
            user = new User();
            user.setEmail(email);
            user.setName(payload.get("given_name").toString());
            user.setSurname(payload.get("family_name").toString());
            user.setPasswordHash(new BCryptPasswordEncoder().encode(UUID.randomUUID().toString()));
            this.registerBasicUser(user, true);
            String pictureUrl = (String) payload.get("picture");
            if(pictureUrl != null) {
                Photo avatar = new Photo();
                avatar.setUrl(pictureUrl);
                user.getProfile().setAvatar(avatar);
                userService.saveUser(user);
            }
        }

        Authentication authenticationToken = createAuthenticationToken(user);

        return this.authenticateUser(authenticationToken);
    }

    private GoogleIdToken.Payload verifyGoogleToken(String token) {
        GoogleIdTokenVerifier verifier = new GoogleIdTokenVerifier.Builder(new ApacheHttpTransport(), new JacksonFactory())
                .setAudience(Collections.singletonList(this.googleClientId))
                .build();
        try {
            GoogleIdToken idToken = verifier.verify(token);
            if (idToken != null) {
                return idToken.getPayload();
            } else {
                throw new BookingRuntimeException(MessageTypeEnum.INVALID_TOKEN_ID);
            }
        } catch (GeneralSecurityException | IOException e) {
            throw new BookingRuntimeException(e);
        }
    }


    public ResponseEntity<User> signInOrRegisterFacebookUser(String token) {
        Facebook facebook = new FacebookTemplate(token);
        String[] fields = {"email", "first_name", "last_name"};
        String fbResult = facebook.fetchObject("me", String.class, fields);
        Gson g = new Gson();
        FBUser fbUser = g.fromJson(fbResult, FBUser.class);
        String email = fbUser.email;
        User user = this.userService.findUserByEmail(email);
        if (user == null) {
            user = new User();
            user.setEmail(email);
            user.setName(fbUser.first_name);
            user.setSurname(fbUser.last_name);
            user.setPasswordHash(new BCryptPasswordEncoder().encode(UUID.randomUUID().toString()));
            return this.registerBasicUser(user, true);
        }
        Authentication authenticationToken = createAuthenticationToken(user);
        return this.authenticateUser(authenticationToken);
    }

    private class FBUser {
            String email;
            String first_name;
            String last_name;
    }

    public void changePassword(String currentPassword, String newPassword, User user) {
        if(!this.passwordEncoder.matches(currentPassword, user.getPasswordHash())) {
            throw new BookingRuntimeException(MessageTypeEnum.UNMATCHED_USERNAME_PASSWORD);
        }
        user.setPasswordHash(this.passwordEncoder.encode(newPassword));
        this.userService.saveUser(user);
    }

    public void sendRecoverPasswordLink(String email) {
        User user = this.userService.findUserByEmail(email);
        if(user == null) {
            throw new BookingRuntimeException(MessageTypeEnum.EMAIL_NOT_EXISTS, email);
        }
        user.setForgotPasswordLinkTime(System.currentTimeMillis());
        this.userService.saveUser(user);
        this.sendRecoverPasswordToken(user);
    }

    public void sendRecoverPasswordToken(User user) {
        String token = tokenProvider.createRecoverPasswordToken(user);
        this.mailService.sendMail(MailData.builder()
                .subject(cacheService.getMessage(MessageTypeEnum.RECOVER_PASSWORD_TITLE))
                .recipientType(RecipientEnum.DEFINED_USER)
                .recipientList(new HashSet<>(Collections.singletonList(user.getEmail())))
                .template(MailTemplate.builder()
                        .file(MailTemplateEnum.CLICK_LINK.getTemplateName())
                        .parameter("greetings", cacheService.getMessage(MessageTypeEnum.GREETINGS, user.getProfile().getDisplayName()))
                        .parameter("message", cacheService.getMessage(MessageTypeEnum.CLICK_TO_REFRESH_PASSWORD))
                        .parameter("link1", this.hostURL + "/recoverPassword?ticket=" + token)
                        .parameter("link2", this.hostURL + "/recoverPassword?ticket=" + token)
                        .build())
                .build());
    }

    public void recoverPassword(RecoverPasswordDTO recoverPasswordDTO) {
        try {
            Claims claims = tokenProvider.parseToken(recoverPasswordDTO.getToken());
            String email = claims.getSubject();
            User user = this.userService.findUserByEmail(email);
            if(user == null) {
                throw new BookingRuntimeException(MessageTypeEnum.EMAIL_NOT_EXISTS, email);
            }
            if(user.getForgotPasswordLinkTime() != null && !user.getForgotPasswordLinkTime().equals(tokenProvider.getTime(claims))) {
                throw new BookingRuntimeException(MessageTypeEnum.RECOVER_PASSWORD_LINK_EXPIRED);
            }
            user.setPasswordHash(this.passwordEncoder.encode(recoverPasswordDTO.getPassword()));
            user.setForgotPasswordLinkTime(null);
            this.userService.saveUser(user);
        } catch (ExpiredJwtException e) {
            throw new BookingRuntimeException(e, MessageTypeEnum.RECOVER_PASSWORD_LINK_EXPIRED);
        }
    }
}
