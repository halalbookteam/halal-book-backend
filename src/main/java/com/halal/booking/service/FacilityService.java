package com.halal.booking.service;

import com.halal.booking.model.entity.Facility;
import com.halal.booking.model.entity.FacilityCategory;
import com.halal.booking.repository.FacilityCategoryRepository;
import com.halal.booking.repository.FacilityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.List;

@Service
@Transactional
public class FacilityService {
    private FacilityCategoryRepository categoryRepository;
    private FacilityRepository facilityRepository;

    @Autowired
    public FacilityService(FacilityCategoryRepository categoryRepository, FacilityRepository facilityRepository) {
        this.categoryRepository = categoryRepository;
        this.facilityRepository = facilityRepository;
    }

    public List<FacilityCategory> getCategoryList() {
        return categoryRepository.findAll();
    }

    public List<Facility> findFacilitiesByIds(Collection<Long> ids) {
        return facilityRepository.findAllById(ids);
    }

    public Facility saveFacility(Facility facility)
    {
        facility.updateTranslations();
        return facilityRepository.save(facility);
    }

    public void deleteFacility(Long id)
    {
        facilityRepository.deleteById(id);
    }

    public FacilityCategory saveCategory(FacilityCategory facilityCategory)
    {
        facilityCategory.updateTranslations();
        return categoryRepository.save(facilityCategory);
    }

    public void deleteCategory(Long id)
    {
        facilityRepository.deleteByCategory_id(id);
        categoryRepository.deleteById(id);
    }
}
