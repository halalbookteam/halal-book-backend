package com.halal.booking.service;

import com.halal.booking.model.entity.Currency;
import com.halal.booking.model.entity.Language;
import com.halal.booking.repository.CurrencyRepository;
import com.halal.booking.repository.LanguageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class LanguageService {
    private LanguageRepository languageRepository;
    private CurrencyRepository currencyRepository;

    @Autowired
    public LanguageService(LanguageRepository languageRepository, CurrencyRepository currencyRepository) {
        this.languageRepository = languageRepository;
        this.currencyRepository = currencyRepository;
    }

    public Collection<Language> getLanguages() {
        return this.languageRepository.findAll();
    }

    public Collection<Currency> getCurrencies() {
        return this.currencyRepository.findAll();
    }
}
