package com.halal.booking.service;

import com.halal.booking.enums.MealOptionTypeEnum;
import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.enums.PermissionEnum;
import com.halal.booking.exception.BookingException;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.model.dto.Place;
import com.halal.booking.model.entity.*;
import com.halal.booking.repository.*;
import com.halal.booking.usermanagement.model.User;
import com.halal.booking.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@Transactional
public class HotelService {
    private final HotelRepository hotelRepository;
    private final AuthenticationService authenticationService;
    private final UserService userService;
    private PhotoRepository photoRepository;
    private FacilityService facilityService;
    private RoomRepository roomRepository;
    private RoomAlternativeRepository alternativeRepository;
    private RoomRateRepository roomRateRepository;
    private ReviewService reviewService;

    @Autowired
    public HotelService(HotelRepository hotelRepository, AuthenticationService authenticationService, UserService userService, PhotoRepository photoRepository, FacilityService facilityService, RoomRepository roomRepository, RoomAlternativeRepository alternativeRepository, ReviewService reviewService, RoomRateRepository roomRateRepository) {
        this.hotelRepository = hotelRepository;
        this.authenticationService = authenticationService;
        this.userService = userService;
        this.photoRepository = photoRepository;
        this.facilityService = facilityService;
        this.roomRepository = roomRepository;
        this.alternativeRepository = alternativeRepository;
        this.reviewService = reviewService;
        this.roomRateRepository = roomRateRepository;
    }

    public List<Hotel> getAllHotelList() {
        return this.hotelRepository.findAll();
    }

    public Hotel getHotel(Long id) {
        return this.hotelRepository.findById(id).orElse(null);
    }

    public Hotel saveHotel(Hotel hotel) throws BookingException {
        hotel.setEvaluation(null);
        if (hotel.getId() != null) {
            checkUserHotel(hotel.getId(), this::isUserHotel);
            Hotel hotelFromDb = hotelRepository.getOne(hotel.getId());
            List<String> urlList = hotel.getPhotos().stream().map(Photo::getUrl).collect(Collectors.toList());
            if (urlList.isEmpty()) {
                photoRepository.deleteByHotelAndRoomIsNull(hotelFromDb);
            } else {
                photoRepository.deleteByHotelAndRoomIsNullAndUrlNotIn(hotelFromDb, urlList);
            }
        } else {
            checkAllHotelsPermission();
        }

        hotel.updateTranslations();
        hotel.getAddress().setHotel(hotel);

        //set users
        List<User> users = hotel.getUsers();
        if (users != null && !users.isEmpty()) {
            List<User> userList = userService.findUserByIds(users.stream().map(AbstractModel::getId).collect(Collectors.toSet()));
            hotel.setUsers(userList);
        } else {
            User currentUser = authenticationService.getCurrentUser();
            hotel.setUsers(Collections.singletonList(currentUser));
        }

        //set facilities
        List<Facility> facilities = hotel.getFacilities();
        if (facilities != null && !facilities.isEmpty()) {
            List<Facility> facilityList = facilityService.findFacilitiesByIds(facilities.stream().map(AbstractModel::getId).collect(Collectors.toList()));
            hotel.setFacilities(facilityList);
        }

        if (hotel.getRooms() != null) {
            hotel.getRooms().forEach(room -> room.setHotel(hotel));
        }

        return hotelRepository.save(hotel);
    }

    public void deleteHotel(Long hotelId) throws BookingException {
        checkAllHotelsPermission();
        Hotel hotel = hotelRepository.getOne(hotelId);
        hotel.getUsers().clear();
        hotel.getFacilities().clear();
        hotelRepository.save(hotel);
        photoRepository.deleteByHotel(hotel);
        roomRepository.deleteByHotel(hotel);
        hotelRepository.deleteById(hotelId);
    }

    public Room saveRoom(Room room) throws BookingException {
        if (room.getId() != null) {
            checkUserHotel(room.getId(), this::isUserRoom);
            Room roomFromDb = this.roomRepository.getOne(room.getId());
            List<String> urlList = room.getPhotos().stream().map(Photo::getUrl).collect(Collectors.toList());
            photoRepository.deleteByRoomAndUrlNotIn(roomFromDb, urlList);
            room.setHotel(roomFromDb.getHotel());
        } else {
            checkUserHotel(room.getHotelId(), this::isUserHotel);
            room.setHotel(hotelRepository.getOne(room.getHotelId()));
        }

        room.updateTranslations();

        //set facilities
        List<Facility> facilities = room.getFacilities();
        if (facilities != null && !facilities.isEmpty()) {
            List<Facility> facilityList = facilityService.findFacilitiesByIds(facilities.stream().map(AbstractModel::getId).collect(Collectors.toList()));
            room.setFacilities(facilityList);
        }
        return roomRepository.save(room);
    }

    public void deleteRoom(Long roomId) throws BookingException {
        checkUserHotel(roomId, this::isUserRoom);
        roomRepository.deleteById(roomId);
    }

    public Set<Hotel> getUserHotels() {
        return hotelRepository.findByUsers(this.authenticationService.getCurrentUser());
    }

    public boolean isUserHotel(Long id) {
        return hotelRepository.existsByUsersAndId(this.authenticationService.getCurrentUser(), id);
    }

    public boolean isUserRoom(Long id) {
        return hotelRepository.existsByUsersAndRooms_id(this.authenticationService.getCurrentUser(), id);
    }

    public boolean isUserAlternative(Long id) {
        return hotelRepository.existsByUsersAndRooms_alternatives_id(this.authenticationService.getCurrentUser(), id);
    }

    public void checkUserHotel(Long id, Predicate<Long> test) throws BookingException {
        Optional.ofNullable(id).orElseThrow(() -> new BookingRuntimeException(MessageTypeEnum.NOT_FOUND, "[id]"));
        if (!userHasAllHotelsCrudPermission() && !test.test(id)) {
            throw new BookingUnauthorizedUserException();
        }
    }

    public RoomAlternative saveAlternative(RoomAlternative alternative) throws BookingException {
        if (alternative.getId() != null) {
            checkUserHotel(alternative.getId(), this::isUserAlternative);
            alternative.setRoom(alternativeRepository.getOne(alternative.getId()).getRoom());
        } else {
            checkUserHotel(alternative.getRoomId(), this::isUserRoom);
            alternative.setRoom(roomRepository.getOne(alternative.getRoomId()));
        }
        MealOption mealOption = alternative.getMealOption();
        if (mealOption.getType().equals(MealOptionTypeEnum.OPTIONAL)) {
            if (mealOption.getPrice().getAmount().equals(BigDecimal.ZERO)) {
                mealOption.setPrice(null);
            }
        } else {
            mealOption.setMeals(null);
            mealOption.setPrice(null);
        }

        PaymentOption paymentOption = alternative.getPaymentOption();
        if (paymentOption.getFreeCancellationDays() != null && paymentOption.getFreeCancellationDays() == 0) {
            paymentOption.setFreeCancellationDays(null);
        }
        if (paymentOption.getPenaltyPercentage() != null && paymentOption.getPenaltyPercentage().compareTo(BigDecimal.ZERO) == 0 ||
                paymentOption.getPenaltyCancellationDays() != null && paymentOption.getPenaltyCancellationDays() == 0) {
            paymentOption.setPenaltyPercentage(null);
            paymentOption.setPenaltyCancellationDays(null);
        }
        return this.alternativeRepository.save(alternative);
    }

    public void deleteAlternative(Long id) throws BookingException {
        checkUserHotel(id, this::isUserAlternative);
        this.alternativeRepository.deleteById(id);
    }

    @Transactional(readOnly = true)
    public Collection<Hotel> searchHotels(HotelSearchQuery hotelSearchQuery) {
        Set<Hotel> hotelSet = hotelRepository.search(hotelSearchQuery);
        int nightLeft = new DateTimeUtils().getNightCount(new Date(), hotelSearchQuery.getCheckIn());
        hotelSet.forEach(hotel -> {
            hotel.getRooms().forEach(room -> {
                room.getAlternatives().removeIf(alternative -> !alternative.isAvailable(nightLeft));
                List<RoomRate> rates = this.roomRateRepository.findRates(room, hotelSearchQuery.getCheckIn(), hotelSearchQuery.getCheckOut(), hotelSearchQuery.getNightCount(), 1);
                if (rates.size() == hotelSearchQuery.getNightCount()) {
                    room.prepareSearch(rates);
                }
            });
            hotel.prepareSearch(hotelSearchQuery);
        });
        return hotelSet;
    }

    public List<Place> loadPlaces() {
        List<Hotel> hotels = this.hotelRepository.findAll();
        Set<Place> countrySet = new HashSet<>();
        Set<Place> countySet = new HashSet<>();
        Set<Place> citySet = new HashSet<>();
        Set<Place> distinctSet = new HashSet<>();
        List<Place> hotelPlaceList = new ArrayList<>();
        hotels.forEach(hotel -> hotel.getAddress().getTranslations().forEach(addressTranslation -> {
            HotelTranslation hotelTranslation = Optional.ofNullable(hotel.getTranslation(addressTranslation.getLanguage().getCode())).orElse(hotel.getDefaultTranslation());
            hotelPlaceList.add(new Place(hotelTranslation.getName(), addressTranslation));
            countrySet.add(new Place(null, null, null, null, addressTranslation.getCountry()));
            if (addressTranslation.getCounty() != null) {
                countySet.add(new Place(null, null, null, addressTranslation.getCounty(), addressTranslation.getCountry()));
            }
            if (addressTranslation.getCity() != null) {
                citySet.add(new Place(null, null, addressTranslation.getCity(), addressTranslation.getCounty(), addressTranslation.getCountry()));
            }
            if (addressTranslation.getDistrict() != null) {
                distinctSet.add(new Place(null, addressTranslation.getDistrict(), addressTranslation.getCity(), addressTranslation.getCounty(), addressTranslation.getCountry()));
            }
        }));
        List<Place> list = new ArrayList<>(countrySet);
        list.addAll(countySet);
        list.addAll(citySet);
        list.addAll(distinctSet);
        list.addAll(hotelPlaceList);
        return list;
    }

    public Collection<Hotel> getHotels() {
        if (userHasAllHotelsCrudPermission()) {
            return this.getAllHotelList();
        }
        return this.getUserHotels();
    }

    private void checkAllHotelsPermission() throws BookingException {
        if (!userHasAllHotelsCrudPermission()) {
            throw new BookingUnauthorizedUserException();
        }
    }

    private boolean userHasAllHotelsCrudPermission() {
        return this.authenticationService.getCurrentUser().hasPermission(PermissionEnum.ALL_HOTELS_CRUD);
    }

    public List<Review> getHotelReviews(Long hotelId) {
        return this.reviewService.getHotelReviews(hotelId);
    }

    public Collection<RoomRate> saveRoomRates(Long roomId, RoomRate[] roomRates) {
        Room room = this.roomRepository.getOne(roomId);
        if(room.getHotel().getChannelManager() != null) {
            throw new BookingRuntimeException(MessageTypeEnum.HAS_CHANNEL_MANAGER);
        }
        return Arrays.stream(roomRates).map(roomRate -> {
            roomRate.setRoom(room);
            if (roomRate.getId() != null) {
                RoomRate rate = this.roomRateRepository.getOne(roomRate.getId());
                roomRate = rate.copy(roomRate);
            }
            return this.roomRateRepository.save(roomRate);
        }).collect(Collectors.toList());
    }
}
