package com.halal.booking.service;

import com.halal.booking.enums.RecipientEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.model.dto.MailData;
import com.halal.booking.model.dto.MailTemplate;
import com.halal.booking.usermanagement.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.MailException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

import java.io.File;
import java.net.URISyntaxException;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class MailService {
    private final JavaMailSender mailSender;
    private final TemplateEngine templateEngine;
    private final UserService userService;

    @Value("${mail.templatePath}")
    private String templatePath;

    @Autowired
    public MailService(JavaMailSender mailSender, TemplateEngine templateEngine, UserService userService) {
        this.mailSender = mailSender;
        this.templateEngine = templateEngine;
        this.userService = userService;
    }

    public void sendMail(MailData mailData) {
        new Thread(() -> {
            prepareRecipients(mailData);
            MimeMessagePreparator[] preperators = mailData.getRecipientList().stream().map(recipient -> (MimeMessagePreparator) mimeMessage -> {
                MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, "utf-8");
                messageHelper.setSubject(mailData.getSubject());
                messageHelper.setTo(recipient);
                String content = prepareMail(mailData);
                messageHelper.setText(content, true);
            }).toArray(MimeMessagePreparator[]::new);
            try {
                this.mailSender.send(preperators);
            } catch (MailException e) {
                throw new BookingRuntimeException(e);
            }
        }).start();
    }

    private void prepareRecipients(MailData mailData) {
        RecipientEnum recipientType = mailData.getRecipientType();
        if (recipientType.equals(RecipientEnum.NEWSLETTER_SUBSCRIBER)) {
            mailData.setRecipientList(this.userService.getNewsletterUserMails());
        } else if (recipientType.equals(RecipientEnum.ADMIN) || recipientType.equals(RecipientEnum.HOTEL_OWNER)) {
            mailData.setRecipientList(this.userService.getUserEmailsByRole(recipientType.toString()));
        }
    }

    private String prepareMail(MailData mailData) {
        Context context = new Context();
        Map<String, Object> map = new HashMap<>();
        mailData.getTemplate().getParameters().forEach(parameter -> {
            map.put(parameter.getKey(), parameter.getValue());
        });
        context.setVariables(map);
        return templateEngine.process(mailData.getTemplate().getName(), context);
    }

    public List<MailTemplate> getMailTemplates() {
        File templateDir;
        try {
            templateDir = new File(MailService.class.getResource(this.templatePath).toURI());
        } catch (URISyntaxException e) {
            throw new BookingRuntimeException(e);
        }
        return Arrays.stream(Objects.requireNonNull(templateDir.listFiles()))
                .map(file -> MailTemplate.builder().file(file).build()).collect(Collectors.toList());
    }
}
