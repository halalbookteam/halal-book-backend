package com.halal.booking.service;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.Messagable;
import com.halal.booking.model.entity.Currency;
import com.halal.booking.model.entity.Language;
import com.halal.booking.model.entity.Message;
import com.halal.booking.repository.MessageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
public class CacheService {
    private LanguageService languageService;
    private RequestService requestService;
    private MessageRepository messageRepository;
    private Map<String, Map<Language, Message>> messageMap;
    private Map<String, Language> languageMap;
    private Map<String, Currency> currencyMap;

    @Autowired
    public CacheService(LanguageService languageService, RequestService requestService, MessageRepository messageRepository) {
        this.languageService = languageService;
        this.requestService = requestService;
        this.messageRepository = messageRepository;
    }

    @PostConstruct
    public void init(){
        this.loadMessages();
        this.loadLanguages();
        this.loadCurrencies();
    }

    private void loadCurrencies() {
        this.currencyMap = this.languageService.getCurrencies().stream().collect(Collectors.toMap(Currency::getCode, Function.identity()));
    }

    private void loadLanguages() {
        this.languageMap = this.languageService.getLanguages().stream().collect(Collectors.toMap(Language::getCode, Function.identity()));
    }

    private void loadMessages() {
        this.messageMap = new HashMap<>();
        this.messageRepository.findAll().forEach(message -> {
            Map<Language, Message> languageStringMap = this.messageMap.computeIfAbsent(message.getType(), k -> new HashMap<>());
            languageStringMap.put(message.getLanguage(), message);
        });
    }

    public String getMessage(Messagable messagable) {
        return getMessage(messagable, this.getCurrentLanguage());
    }

    public String getMessage(Messagable messagable, Language language) {
        Map<Language, Message> languageStringMap = this.messageMap.get(messagable.getMessageType());
        if(languageStringMap == null) {
            return null;
        }
        Message message = Optional.ofNullable(languageStringMap.get(language)).orElse(languageStringMap.get(this.getDefaultLanguage()));
        if(message == null) {
            return null;
        }
        return message.format(messagable.getParameters());
    }

    public String getMessage(MessageTypeEnum messageType, Object... parameters) {
        return this.getMessage(getCurrentLanguage(), messageType, parameters);
    }

    public String getMessage(Language language, MessageTypeEnum messageType, Object... parameters) {
        return this.getMessage(new Messagable() {
            @Override
            public String getMessageType() {
                return messageType.toString();
            }

            @Override
            public Object[] getParameters() {
                return parameters;
            }

            @Override
            public Exception cloneException(String message) {
                return null;
            }
        }, language);
    }

    public Language getCurrentLanguage() {
        return this.languageMap.get(this.requestService.getLanguageCode());
    }

    private Language getDefaultLanguage() {
        return this.languageMap.get("en");
    }

    public Collection<Language> getLanguages() {
        return this.languageMap.values();
    }

    public Collection<Currency> getCurrencies() {
        return this.currencyMap.values();
    }
}
