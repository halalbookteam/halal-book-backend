package com.halal.booking.service;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.StorageFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;

    @Autowired
    public FileSystemStorageService(@Value("${spring.servlet.multipart.location}") String locationPath) {
        this.rootLocation = Paths.get(locationPath);
    }

    @Override
    public List<String> store(MultipartFile[] files) {
        return Arrays.stream(files).map(this::store).collect(Collectors.toList());
    }

    @Override
    public String store(MultipartFile file) {
        String filename = StringUtils.cleanPath(Objects.requireNonNull(file.getOriginalFilename()));
        try {
            if (file.isEmpty()) {
                throw new BookingRuntimeException(MessageTypeEnum.EMPTY_FILE, filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new BookingRuntimeException(MessageTypeEnum.CANNOT_STORE_FILE, filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                Files.copy(inputStream, this.rootLocation.resolve(filename),
                        StandardCopyOption.REPLACE_EXISTING);
            }
        }
        catch (IOException e) {
            throw new BookingRuntimeException(e, MessageTypeEnum.EMPTY_FILE, filename);
        }
        return filename;
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        try {
            Path file = load(filename);
            Resource resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            }
            else {
                throw new StorageFileNotFoundException(MessageTypeEnum.FILE_CANT_READ);
            }
        }
        catch (MalformedURLException e) {
            throw new StorageFileNotFoundException(MessageTypeEnum.FILE_CANT_READ, e);
        }
    }

    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new BookingRuntimeException(e, MessageTypeEnum.STORAGE_CANNOT_INITIALIZE);
        }
    }
}