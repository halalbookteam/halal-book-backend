package com.halal.booking.service;

import com.halal.booking.enums.ChannelManagerEnum;
import com.halal.booking.external.channelmanager.reseliva.service.ReselivaService;
import com.halal.booking.model.entity.Reservation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ChannelManagerService {
    private ReselivaService reselivaService;

    @Autowired
    public ChannelManagerService(ReselivaService reselivaService) {
        this.reselivaService = reselivaService;
    }

    public void sendBooking(Reservation reservation) {
        ChannelManagerEnum channelManager = reservation.getHotel().getChannelManager();
        if(channelManager != null) {
           if(channelManager.equals(ChannelManagerEnum.RESELIVA)) {
               this.reselivaService.sendBooking(reservation);
           }
        }
    }
}
