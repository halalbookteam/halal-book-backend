package com.halal.booking.service;

import com.halal.booking.enums.ActionTypeEnum;
import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.enums.ReviewStatusEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.model.entity.*;
import com.halal.booking.repository.*;
import com.halal.booking.usermanagement.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class ReviewService {
    private ReviewCategoryRepository reviewCategoryRepository;
    private ReviewRepository reviewRepository;
    private ReviewCategoryScoreRepository reviewCategoryScoreRepository;
    private HotelEvaluationRepository hotelEvaluationRepository;
    private AuthenticationService authenticationService;
    private ReservationRepository reservationRepository;
    private UserActionService userActionService;
    private CacheService cacheService;

    @Autowired
    public ReviewService(ReviewCategoryRepository reviewCategoryRepository, ReviewRepository reviewRepository, ReviewCategoryScoreRepository reviewCategoryScoreRepository, HotelEvaluationRepository hotelEvaluationRepository, AuthenticationService authenticationService, ReservationRepository reservationRepository, UserActionService userActionService, CacheService cacheService) {
        this.reviewCategoryRepository = reviewCategoryRepository;
        this.reviewRepository = reviewRepository;
        this.reviewCategoryScoreRepository = reviewCategoryScoreRepository;
        this.hotelEvaluationRepository = hotelEvaluationRepository;
        this.authenticationService = authenticationService;
        this.reservationRepository = reservationRepository;
        this.userActionService = userActionService;
        this.cacheService = cacheService;
    }

    @Transactional(readOnly = true)
    public Collection<ReviewCategory> getReviewCategories() {
        return this.reviewCategoryRepository.findAll();
    }

    @Transactional
    public void saveReview(Review review) throws BookingUnauthorizedUserException {
        User currentUser = this.authenticationService.getCurrentUser();
        ReviewStatusEnum oldStatus = null;
        if (review.getId() != null) {
            Review existingReview = this.reviewRepository.findByReservationAndReservation_User(review.getReservation(), currentUser);
            if (existingReview == null) {
                throw new BookingUnauthorizedUserException();
            }
            review.setReservation(existingReview.getReservation());
            oldStatus = existingReview.getStatus();
        } else {
            Reservation reservation = this.reservationRepository.findByIdAndUser(review.getReservationId(), currentUser);
            if (reservation == null) {
                throw new BookingUnauthorizedUserException();
            }
            review.setReservation(reservation);
        }

        if (!review.getReservation().canBeReviewed()) {
            throw new BookingRuntimeException(MessageTypeEnum.RESERVATION_CANNOT_REVIEW);
        }
        if (review.getCategoryScores() != null) {
            review.getCategoryScores().removeIf(reviewCategoryScore -> reviewCategoryScore.getScore() == null);
            review.getCategoryScores().forEach(reviewCategoryScore -> reviewCategoryScore.setReview(review));
        }
        if (review.getTravelWith() != null && !review.getTravelWith().hasInfo()) {
            review.setTravelWith(null);
        }
        review.setStatus(ReviewStatusEnum.PENDING);
        review.setTime(new Date());
        this.reviewRepository.save(review);

        if (oldStatus != null && !oldStatus.equals(ReviewStatusEnum.PENDING)) {
            this.updateHotelReviewScores(review.getReservation().getHotel());
        }

        UserAction userAction = UserAction.builder().actionType(ActionTypeEnum.REVIEW_CHANGED)
                .user(currentUser).entityId(review.getId()).oldValue(oldStatus == null ? null : oldStatus.toString())
                .newValue(ReviewStatusEnum.PENDING.toString()).build();

        this.userActionService.saveUserAction(userAction);
    }

    @Transactional(readOnly = true)
    public Review getReview(Long reservationId) throws BookingUnauthorizedUserException {
        User currentUser = this.authenticationService.getCurrentUser();
        Reservation reservation = this.reservationRepository.findByIdAndUser(reservationId, currentUser);
        if (reservation == null) {
            throw new BookingUnauthorizedUserException();
        }
        Review review = this.reviewRepository.findByReservation(reservation);
        if (review == null) {
            review = new Review();
            review.setReservation(reservation);
            review.setCategoryScores(new ArrayList<>());
            review.setIncludeMyName(true);
        }
        if (review.getTravelWith() == null) {
            review.setTravelWith(new TravelWith());
        }

        review.setLanguage(this.cacheService.getCurrentLanguage());

        Collection<ReviewCategory> reviewCategories = this.getReviewCategories();
        Review finalReview = review;
        reviewCategories.forEach(reviewCategory -> {
            if (finalReview.findCategoryScoreByCategoryId(reviewCategory.getId()) == null) {
                ReviewCategoryScore reviewCategoryScore = new ReviewCategoryScore();
                reviewCategoryScore.setCategory(reviewCategory);
                finalReview.getCategoryScores().add(reviewCategoryScore);
            }
        });
        return review;
    }

    @Transactional(readOnly = true)
    public Collection<Review> getPendingReviews() {
        return this.reviewRepository.findByStatus(ReviewStatusEnum.PENDING);
    }

    @Transactional
    public Review changeState(Long reviewId, ReviewStatusEnum status) {
        Review review = this.reviewRepository.getOne(reviewId);
        ReviewStatusEnum oldStatus = review.getStatus();
        review.setStatus(status);
        this.reviewRepository.save(review);
        User currentUser = this.authenticationService.getCurrentUser();
        UserAction userAction = UserAction.builder().actionType(ActionTypeEnum.REVIEW_CHANGED)
                .user(currentUser).entityId(review.getId()).oldValue(oldStatus == null ? null : oldStatus.toString())
                .newValue(status.toString()).build();

        this.updateHotelReviewScores(review.getReservation().getHotel());

        this.userActionService.saveUserAction(userAction);
        return review;
    }

    private void updateHotelReviewScores(Hotel hotel) {
        HotelEvaluation evaluation = this.hotelEvaluationRepository.findByHotel(hotel);
        if(evaluation == null) {
            evaluation = new HotelEvaluation();
            evaluation.setHotel(hotel);
            evaluation.setEvaluationCategoryScores(new HashSet<>());
        }
        this.reviewCategoryScoreRepository.loadEvaluationCategoryScores(evaluation);

        evaluation.setReviewCount(this.reviewRepository.countByReservation_hotelAndStatus(hotel, ReviewStatusEnum.ACCEPTED));

        evaluation.calculateScore();

        this.hotelEvaluationRepository.save(evaluation);
    }

    public List<Review> getHotelReviews(Long hotelId) {
        return this.reviewRepository.findByReservation_hotel_idAndStatus(hotelId, ReviewStatusEnum.ACCEPTED);
    }
}
