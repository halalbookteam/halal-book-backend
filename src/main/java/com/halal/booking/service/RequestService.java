package com.halal.booking.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class RequestService {
    private final HttpServletRequest request;

    @Autowired
    public RequestService(HttpServletRequest request) {
        this.request = request;
    }

    public String getLanguageCode() {
        try {
            return request.getHeader("applanguagecode");
        } catch (Exception e) {
            return null;
        }
    }
}
