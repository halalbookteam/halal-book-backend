package com.halal.booking.exception;

import com.halal.booking.enums.MessageTypeEnum;

public class BookingUnauthorizedUserException extends BookingException {
    public BookingUnauthorizedUserException() {
        super(MessageTypeEnum.NO_PERMISSION);
    }

    @Override
    public Exception cloneException(String message) {
        return new BookingException(message, this.getCause());
    }
}
