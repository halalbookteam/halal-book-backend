package com.halal.booking.exception;

import com.halal.booking.security.UserNotActivatedException;
import com.halal.booking.service.CacheService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.oauth2.common.exceptions.UnauthorizedUserException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Objects;

@RestControllerAdvice
public class AppExceptionHandler extends ResponseEntityExceptionHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(AppExceptionHandler.class);
    private CacheService cacheService;

    @Autowired
    public AppExceptionHandler(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @ExceptionHandler({BookingRuntimeException.class, BookingException.class})
    public ResponseEntity<?> handleBookingException(Messagable ex, WebRequest request) {
        return handleBookingException(ex, HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException ex, WebRequest request) {
        return handleException(ex, ex.getMessage(), HttpHeaders.EMPTY, HttpStatus.NOT_FOUND, request);
    }

    @ExceptionHandler({UnauthorizedUserException.class})
    public ResponseEntity<?> handleUnauthorizedUserException(Exception ex, WebRequest request) {
        return handleException(ex, ex.getMessage(), HttpHeaders.EMPTY, HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler({BookingUnauthorizedUserException.class})
    public ResponseEntity<?> handleBookingUnauthorizedUserException(BookingUnauthorizedUserException ex, WebRequest request) {
        return handleBookingException(ex, HttpStatus.UNAUTHORIZED, request);
    }

    @ExceptionHandler(UserNotActivatedException.class)
    public ResponseEntity<?> handleUserNotActivatedException(Exception ex, WebRequest request) {
        return handleException(ex, ex.getMessage(), HttpHeaders.EMPTY, HttpStatus.PRECONDITION_REQUIRED, request);
    }

    @ExceptionHandler(AuthenticationException.class)
    public ResponseEntity<?> handleBadCredentialsException(Exception ex, WebRequest request) {
        return handleException(ex, ex.getMessage(), HttpHeaders.EMPTY, HttpStatus.FORBIDDEN, request);
    }

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleUnhandledException(Exception ex, WebRequest request) {
        return handleException(ex, null, HttpHeaders.EMPTY, HttpStatus.INTERNAL_SERVER_ERROR, request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        String body = ex.getMessage();
        if(ex.getBindingResult().hasFieldErrors()) {
            FieldError fieldError = ex.getBindingResult().getFieldError();
            body = Objects.requireNonNull(fieldError).getField() + ": " + Objects.requireNonNull(fieldError).getDefaultMessage();
        }
        return handleException(ex, body, headers, status, request);
    }

    private ResponseEntity<?> handleBookingException(Messagable messagable, HttpStatus status, WebRequest request) {
        String message = this.cacheService.getMessage(messagable);
        Exception exception = messagable.cloneException(message);
        if(message == null) {
            message = exception.getMessage();
        }
        return this.handleException(exception, message, HttpHeaders.EMPTY, status, request);
    }

    private ResponseEntity<Object> handleException (Exception ex, @Nullable String message, HttpHeaders headers, HttpStatus status, WebRequest request) {
        LOGGER.error(message, ex);
        return super.handleExceptionInternal(ex, message, headers, status, request);
    }
}
