package com.halal.booking.exception;

import com.halal.booking.enums.MessageTypeEnum;

import java.util.Locale;

public class BookingRuntimeException extends RuntimeException implements Messagable {
    private String messageType;
    private Object[] parameters;

    public BookingRuntimeException(MessageTypeEnum messageType, Object... parameters) {
        this(null, messageType, parameters);
    }

    public BookingRuntimeException(Throwable throwable, MessageTypeEnum messageType, Object... parameters) {
        this(throwable, messageType.toString(), parameters);
    }

    public BookingRuntimeException(String messageType, Object... parameters) {
        this(null, messageType, parameters);
    }

    public BookingRuntimeException(Throwable throwable, String messageType, Object... parameters) {
        super(messageType, throwable);
        this.messageType = messageType.toUpperCase(Locale.ENGLISH);
        this.parameters = parameters;
    }

    public BookingRuntimeException(BookingException e) {
        this(e.getCause(), e.getMessageType(), e.getParameters());
    }

    public BookingRuntimeException(Exception e) {
        super(e);
    }

    protected BookingRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public String getMessageType() {
        return messageType;
    }

    @Override
    public Object[] getParameters() {
        return parameters;
    }

    @Override
    public Exception cloneException(String message) {
        return new BookingRuntimeException(message == null ? this.getMessage() : message, this.getCause());
    }
}
