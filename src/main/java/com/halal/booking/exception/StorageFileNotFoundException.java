package com.halal.booking.exception;

import com.halal.booking.enums.MessageTypeEnum;

public class StorageFileNotFoundException extends BookingRuntimeException {

    public StorageFileNotFoundException(MessageTypeEnum messageType) {
        super(messageType);
    }

    public StorageFileNotFoundException(MessageTypeEnum messageType, Throwable cause) {
        super(cause, messageType);
    }

    private StorageFileNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    @Override
    public Exception cloneException(String message) {
        return new StorageFileNotFoundException(message, this.getCause());
    }
}