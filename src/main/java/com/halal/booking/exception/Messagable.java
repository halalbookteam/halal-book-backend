package com.halal.booking.exception;

public interface Messagable {
    String getMessageType();
    Object[] getParameters();
    Exception cloneException(String message);
}
