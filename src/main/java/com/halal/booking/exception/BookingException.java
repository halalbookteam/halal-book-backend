package com.halal.booking.exception;

import com.halal.booking.enums.MessageTypeEnum;

import java.util.Locale;

public class BookingException extends Exception implements Messagable {
    private String messageType;
    private Object[] parameters;

    public BookingException(MessageTypeEnum message) {
        this(message.toString());
    }

    public BookingException(Throwable cause, String messageType, Object... parameters) {
        super(messageType, cause);
        this.messageType = messageType.toUpperCase(Locale.ENGLISH);
        this.parameters = parameters;
    }

    protected BookingException(String message) {
        super(message);
    }

    protected BookingException(String message, Throwable throwable) {
        super(message, throwable);
    }

    public BookingException(String messageType, Object... parameters) {
        this(null, messageType, parameters);
    }

    @Override
    public String getMessageType() {
        return messageType;
    }

    @Override
    public Object[] getParameters() {
        return parameters;
    }

    @Override
    public Exception cloneException(String message) {
        return new BookingException(message);
    }
}
