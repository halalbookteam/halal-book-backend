package com.halal.booking.usermanagement.repository;

import com.halal.booking.usermanagement.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 04:48
 */
@Repository
public interface UserRoleRepository extends JpaRepository<Role, Long>
{
    List<Role> findByNameIn(List<String> roleNames);
}
