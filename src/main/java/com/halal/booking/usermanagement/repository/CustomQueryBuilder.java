package com.halal.booking.usermanagement.repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by serhat.yenican on 28.9.2017 16:11.
 */
public class CustomQueryBuilder {
    private StringBuilder queryStr;
    private EntityManager em;
    private Map<String, Object> parameterList;

    public CustomQueryBuilder(EntityManager em, String selectClause) {
        this.em = em;
        this.queryStr = new StringBuilder(selectClause);
    }

    public void appendToWhereClause(String fieldName, Object value) {
        if(parameterList == null) {
            parameterList = new HashMap<>();
            queryStr.append(" where ");
        }
        else {
            queryStr.append(" and ");
        }
        queryStr.append(fieldName).append(" = :").append(fieldName);
        parameterList.put(fieldName, value);
    }

    public <T> TypedQuery<T> build(Class<T> resultClass) {
        TypedQuery<T> query = em.createQuery(queryStr.toString(), resultClass);
        if(parameterList != null) {
            for (String fieldName : parameterList.keySet()) {
                query.setParameter(fieldName, parameterList.get(fieldName));
            }
        }
        return query;
    }
}
