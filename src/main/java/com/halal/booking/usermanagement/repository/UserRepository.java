package com.halal.booking.usermanagement.repository;

import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Set;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 04:48
 */
@Repository
public interface UserRepository extends JpaRepository<User, Long>, UserRepositoryCustom
{
    User findByEmail(String email);

    @Query("select u.email from User u inner join u.roles roles where roles.name = :roleName")
    Set<String> getUserEmailsByRole(@Param("roleName") String roleName);
}
