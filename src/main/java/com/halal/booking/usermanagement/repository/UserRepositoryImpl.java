package com.halal.booking.usermanagement.repository;

import com.halal.booking.usermanagement.model.User;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

/**
 * Created by serhat.yenican on 28.9.2017 15:33.
 */
public class UserRepositoryImpl implements UserRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public User[] findUsers(User user) {
        return prepareQuery(user).build(User.class).getResultList().toArray(new User[0]);
    }

    @Override
    public User findUser(User user) {
        try {
            return prepareQuery(user).build(User.class).getSingleResult();
        } catch (NoResultException e) {
            return null;
        }
    }

    private CustomQueryBuilder prepareQuery(User user) {
        CustomQueryBuilder queryBuilder = new CustomQueryBuilder(em, "from User u");
        if(user.getEmail() != null) {
            queryBuilder.appendToWhereClause("email", user.getEmail());
        }
        if(user.getPasswordHash() != null) {
            queryBuilder.appendToWhereClause("passwordHash", user.getPasswordHash());
        }
        if(user.getActive() != null) {
            queryBuilder.appendToWhereClause("active", user.getActive());
        }
        return queryBuilder;
    }
}
