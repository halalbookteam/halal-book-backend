package com.halal.booking.usermanagement.repository;

import com.halal.booking.usermanagement.model.Privilege;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 04:48
 */
@Repository
public interface UserPrivilegeRepository extends JpaRepository<Privilege, Long>
{
}
