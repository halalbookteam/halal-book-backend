package com.halal.booking.usermanagement.repository;

import com.halal.booking.usermanagement.model.User;

/**
 * Created by serhat.yenican on 28.9.2017 15:33.
 */
public interface UserRepositoryCustom {

    User[] findUsers(User user);

    User findUser(User user);
}
