package com.halal.booking.usermanagement.controller;

import com.halal.booking.usermanagement.model.Privilege;
import com.halal.booking.usermanagement.model.Role;
import com.halal.booking.usermanagement.model.User;
import com.halal.booking.usermanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 01:28
 */
@RestController
@RequestMapping("/api/userManagement")
public class UserManagementRestController
{
    private static final Logger logger = LoggerFactory.getLogger(UserManagementRestController.class);

    private final UserService userService;

    @Autowired
    public UserManagementRestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/privileges")
    public List<Privilege> listPrivileges() {
        return userService.getAllPrivileges();
    }

    @GetMapping("/privilege/{id}")
    public Privilege getPrivilege(@PathVariable("id") Long id) {
        return userService.findPrivilegeById(id);
    }

    @PostMapping("/privilege")
    public ResponseEntity<?> createPrivilege(@Valid @RequestBody Privilege privilege, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        userService.savePrivilege(privilege);

        return new ResponseEntity<>(privilege, HttpStatus.OK);
    }

    @PutMapping("/privilege/{id}")
    public ResponseEntity<?> updatePrivilege(@PathVariable("id") Long id, @Valid @RequestBody Privilege privilege, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        Privilege current = userService.findPrivilegeById(id);
        if(current == null) {
            return new ResponseEntity<>("No Privilege found for id: " + id, HttpStatus.NOT_FOUND);
        }
        current.setName(privilege.getName());
        current.setUrl(privilege.getUrl());

        userService.savePrivilege(current);

        return new ResponseEntity<>(current, HttpStatus.OK);
    }

    @DeleteMapping("/privilege/{id}")
    public ResponseEntity<String> deletePrivilege(@PathVariable("id") Long id)
    {
        Privilege privilege = userService.findPrivilegeById(id);
        if(privilege == null) {
            return new ResponseEntity<>("No Privilege found for id: " + id, HttpStatus.NOT_FOUND);
        }

        try {
            userService.deletePrivilege(privilege);
        } catch (DataIntegrityViolationException e) {
            logger.info("Privilege is in use", e);
            return new ResponseEntity<>("Privilege is related to a role: " + id, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/roles")
    public List<Role> listRoles() {
        return userService.getAllRoles();
    }

    @GetMapping("/role/{id}")
    public Role getRoles(@PathVariable("id") Long id) {
        return userService.findRoleById(id);
    }

    @PostMapping("/role")
    public ResponseEntity<?> createRole(@Valid @RequestBody Role role, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }

        userService.saveRole(role);

        return new ResponseEntity<>(role, HttpStatus.OK);
    }

    @PutMapping("/role/{id}")
    public ResponseEntity<?> updateRole(@PathVariable("id") Long id, @Valid @RequestBody Role role, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        Role current = userService.findRoleById(id);
        if(current == null) {
            return new ResponseEntity<>("No Role found for id: " + id, HttpStatus.NOT_FOUND);
        }
        current.setName(role.getName());
        current.setPrivileges(role.getPrivileges());
        userService.saveRole(current);

        return new ResponseEntity<>(current, HttpStatus.OK);
    }

    @DeleteMapping("/role/{id}")
    public ResponseEntity<String> deleteRole(@PathVariable("id") Long id)
    {
        Role role = userService.findRoleById(id);
        if(role == null) {
            return new ResponseEntity<>("No Role found for id: " + id, HttpStatus.NOT_FOUND);
        }

        try {
            userService.deleteRole(role);
        } catch (DataIntegrityViolationException e) {
            logger.info("Role is in use", e);
            return new ResponseEntity<>("Role is related to a user: " + id, HttpStatus.INTERNAL_SERVER_ERROR);
        }
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/users")
    public List<User> listUsers() {
        return userService.getAllUsers();
    }

    @GetMapping("/user/{id}")
    public User getUser(@PathVariable("id") Long id) {
        return userService.findUserById(id);
    }

    @GetMapping("/findUserByEmail/{email:.+}")
    public User findUserByEmail(@PathVariable("email") String email) {
        return userService.findUserByEmail(email);
    }

    @PostMapping("/findUserByEmailAndPassword")
    public User findUserByEmailAndPassword(@RequestBody User user) {
        return userService.findUserByEmailAndPassword(user);
    }

    @GetMapping("/findRoles")
    public List<Role> findRoles(@RequestParam("roles") List<String> roleNames) {
        return userService.findRolesByName(roleNames);
    }

    @PostMapping("/user")
    public ResponseEntity<?> createUser(@Valid @RequestBody User user, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        userService.saveUser(user);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<?> updateUser(@PathVariable("id") Long id, @Valid @RequestBody User user, BindingResult bindingResult) {
        if(bindingResult.hasFieldErrors()) {
            FieldError fieldError = bindingResult.getFieldErrors().get(0);
            return new ResponseEntity<>("field [" + fieldError.getField() + "] " + fieldError.getDefaultMessage(), HttpStatus.BAD_REQUEST);
        }
        User current = userService.findUserById(id);
        if(current == null) {
            return new ResponseEntity<>("No User found for id: " + id, HttpStatus.NOT_FOUND);
        }
        userService.updateUser(user, current);

        return new ResponseEntity<>(user, HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<String> deleteUser(@PathVariable("id") Long id)
    {
        User user = userService.findUserById(id);
        if(user == null) {
            return new ResponseEntity<>("No User found for id: " + id, HttpStatus.NOT_FOUND);
        }

        userService.deleteUser(user);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PostMapping("/findUsers")
    public User[] findUsers(@RequestBody User user) {
        return userService.findUsers(user);
    }

    @PostMapping("/findUser")
    public User findUser(@RequestBody User user) {
        return userService.findUser(user);
    }
}
