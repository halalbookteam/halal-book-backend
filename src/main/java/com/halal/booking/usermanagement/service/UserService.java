package com.halal.booking.usermanagement.service;

import com.halal.booking.enums.MessageTypeEnum;
import com.halal.booking.exception.BookingRuntimeException;
import com.halal.booking.exception.BookingUnauthorizedUserException;
import com.halal.booking.model.entity.AbstractModel;
import com.halal.booking.model.entity.UserProfile;
import com.halal.booking.service.CacheService;
import com.halal.booking.usermanagement.model.Privilege;
import com.halal.booking.usermanagement.model.Role;
import com.halal.booking.usermanagement.model.User;
import com.halal.booking.usermanagement.repository.UserPrivilegeRepository;
import com.halal.booking.usermanagement.repository.UserRepository;
import com.halal.booking.usermanagement.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;

/**
 * User: serhat.yenican
 * Date: 3.10.2017
 * Time: 15:35
 */
@Service
@Transactional
public class UserService {
    private final UserRepository userRepository;
    private final UserProfileRepository profileRepository;
    private final UserRoleRepository roleRepository;
    private final CacheService cacheService;

    private final UserPrivilegeRepository privilegeRepository;

    @Autowired
    public UserService(UserRepository userRepository, UserProfileRepository profileRepository, UserRoleRepository roleRepository, CacheService cacheService, UserPrivilegeRepository privilegeRepository) {
        this.userRepository = userRepository;
        this.profileRepository = profileRepository;
        this.roleRepository = roleRepository;
        this.cacheService = cacheService;
        this.privilegeRepository = privilegeRepository;
    }

    public User findUserById(Long userId) {
        return userRepository.getOne(userId);
    }

    public List<User> findUserByIds(Collection<Long> userIds) {
        return userRepository.findAllById(userIds);
    }

    public List<Privilege> getAllPrivileges() {
        return privilegeRepository.findAll();
    }

    public Privilege findPrivilegeById(Long id) {
        return privilegeRepository.getOne(id);
    }

    public void savePrivilege(Privilege privilege) {
        privilegeRepository.save(privilege);
    }

    public void deletePrivilege(Privilege privilege) {
        privilegeRepository.delete(privilege);
    }

    public List<Role> getAllRoles() {
        return roleRepository.findAll();
    }

    public Role findRoleById(Long id) {
        return roleRepository.getOne(id);
    }

    public void saveRole(Role role) {
        Collection<Privilege> privileges = role.getPrivileges();
        if (privileges != null && !privileges.isEmpty()) {
            List<Long> idList = privileges.stream().map(AbstractModel::getId).collect(Collectors.toList());
            privileges = privilegeRepository.findAllById(idList);
            role.setPrivileges(privileges);
        }
        roleRepository.save(role);
    }

    public void deleteRole(Role role) {
        roleRepository.delete(role);
    }

    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    public User findUserByEmail(String email) {
        User user = new User();
        user.setEmail(email);
        return userRepository.findUser(user);
    }

    public User findUserByEmailAndPassword(User user) {
        user.setActive(true);
        return userRepository.findUser(user);
    }

    public List<Role> findRolesByName(List<String> roleNames) {
        return roleRepository.findByNameIn(roleNames);
    }

    public User saveUser(User user) {
        Collection<Role> roles = user.getRoles();
        if (roles != null && !roles.isEmpty()) {
            List<Long> idList = roles.stream().map(AbstractModel::getId).collect(Collectors.toList());
            roles = roleRepository.findAllById(idList);
            user.setRoles(roles);
        }
        if(user.getId() == null) {
            user.setCreateTime(new Date());
            UserProfile profile = new UserProfile();
            user.setProfile(profile);
            if(user.getName() != null && user.getSurname() != null) {
                profile.setDisplayName(user.getName() + " " + user.getSurname());
            }
            else {
                profile.setDisplayName(user.getEmail().substring(0, user.getEmail().indexOf("@")));
            }
            profile.setUser(user);
            profile.setDefaultLanguage(this.cacheService.getCurrentLanguage());
            profile.setDefaultCurrency(profile.getDefaultLanguage().getCurrency());
            profileRepository.save(profile);
        }
        try {
            return userRepository.save(user);
        } catch (DataIntegrityViolationException e) {
            throw new BookingRuntimeException(MessageTypeEnum.EMAIL_EXISTS);
        }
    }

    public void updateUser(User user, User current) {
        Collection<Role> roles = user.getRoles();
        if (roles != null) {
            List<Long> idList = roles.stream().map(AbstractModel::getId).collect(Collectors.toList());
            roles = roleRepository.findAllById(idList);
            user.setRoles(roles);
        } else {
            user.setRoles(current.getRoles());
        }
        //if password changed
        if (user.getPasswordHash() == null) {
            user.setPasswordHash(current.getPasswordHash());
        }
        userRepository.save(user);
    }

    public void deleteUser(User user) {
        profileRepository.deleteByUser(user);
        userRepository.delete(user);
    }

    public void deleteUser(Long id) {
        profileRepository.deleteByUser_id(id);
        userRepository.deleteById(id);
    }

    public User[] findUsers(User user) {
        return userRepository.findUsers(user);
    }

    public User findUser(User user) {
        return userRepository.findUser(user);
    }

    public Map<String, Set<String>> getPrivilegeRoles() {
        Map<String, Set<String>> map = new HashMap<>();
        getAllRoles().forEach(
                role -> role.getPrivileges().stream()
                        .filter(privilege -> StringUtils.hasText(privilege.getUrl()))
                        .forEach(privilege ->
                                map.computeIfAbsent(privilege.getUrl(), k -> new HashSet<>()).add(role.getName())
                        ));
        return map;
    }

    public User getUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveProfile(UserProfile profile, User currentUser) throws BookingUnauthorizedUserException {
        UserProfile existingProfile = this.profileRepository.getOne(profile.getId());
        if(currentUser == null || !currentUser.equals(existingProfile.getUser())) {
            throw new BookingUnauthorizedUserException();
        }
        profile.setUser(currentUser);
        this.profileRepository.save(profile);
    }

    public Set<String> getNewsletterUserMails() {
        return this.profileRepository.getNewsletterUserMails();
    }

    public Set<String> getUserEmailsByRole(String roleName) {
        return this.userRepository.getUserEmailsByRole(roleName);
    }
}
