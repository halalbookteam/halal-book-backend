package com.halal.booking.usermanagement.service;

import com.halal.booking.model.entity.UserProfile;
import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {
    void deleteByUser_id(Long userId);

    void deleteByUser(User user);

    @Query("select p.user.email from UserProfile p where p.newsletter = 1")
    Set<String> getNewsletterUserMails();
}
