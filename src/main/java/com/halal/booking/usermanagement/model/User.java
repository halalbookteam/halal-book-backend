package com.halal.booking.usermanagement.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import com.halal.booking.enums.PermissionEnum;
import com.halal.booking.external.payment.stripe.entity.StripeCustomer;
import com.halal.booking.external.payment.stripe.entity.StripeSource;
import com.halal.booking.model.entity.AbstractModel;
import com.halal.booking.model.entity.UserProfile;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 03:17
 */

@Data
@Entity
@Table
public class User extends AbstractModel
{
    @Column(nullable = false, unique = true)
    private String email;

    private String name;

    private String surname;

    @JsonIgnore
    private String passwordHash;

    @Transient
    private String password;

    @Column(nullable = false)
    private Boolean active;

    private Date createTime;

    @OneToOne(mappedBy = "user")
    private UserProfile profile;

    private Long forgotPasswordLinkTime;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
            name = "users_roles",
            joinColumns = @JoinColumn(
                    name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(
                    name = "role_id", referencedColumnName = "id"))
    private Collection<Role> roles;

    @JsonIgnore
    @OneToOne(mappedBy = "user")
    private StripeCustomer customer;

    public List<StripeSource> getCards() {
        if(this.customer == null) {
            return new ArrayList<>();
        }
        return this.customer.getSources();
    }

    public List<String> getPermissions() {
        return this.roles.stream()
                .filter(role -> role.getPrivileges() != null)
                .flatMap(role -> role.getPrivileges().stream())
                .map(Privilege::getName)
                .collect(Collectors.toList());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        User user = (User) o;

        return Objects.equal(getId(), user.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(super.hashCode(), getId());
    }

    public User()
    {

    }
    public User(Role role)
    {
        this.setRoles( new ArrayList<>()) ;
        this.getRoles().add(role);
    }

    public boolean hasPermission(PermissionEnum permission) {
        return this.roles.stream().anyMatch(role ->
                role.getPrivileges().stream().anyMatch(privilege ->
                        privilege.getName().equals(permission.toString())));
    }
}
