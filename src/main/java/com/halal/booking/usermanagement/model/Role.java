package com.halal.booking.usermanagement.model;


import com.fasterxml.jackson.annotation.JsonIgnore;
import com.halal.booking.model.entity.AbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.Collection;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 03:18
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "role")
public class Role extends AbstractModel
{
    @Column(nullable = false)
    private String name;

    private String description;

    @JsonIgnore
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "roles_privileges",
            joinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "privilege_id", referencedColumnName = "id")
    )

    private Collection<Privilege> privileges;

    public Role() { }

    public Role(Long id) {  this.setId(id);  }


}
