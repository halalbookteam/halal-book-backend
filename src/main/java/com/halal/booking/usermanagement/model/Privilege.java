package com.halal.booking.usermanagement.model;


import com.halal.booking.model.entity.AbstractModel;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * User: serhat.yenican
 * Date: 31.03.2017
 * Time: 03:44
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Table(name = "privilege")
public class Privilege extends AbstractModel
{
    private String name;

    private String description;

    private String url;
}
