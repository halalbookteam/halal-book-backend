package com.halal.booking.property;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.web.cors.CorsConfiguration;

@Data
@ConfigurationProperties(prefix = "token")
public class TokenProperties {
    private String jwtSecret;
    private int tokenValidityInSeconds;
    private int tokenValidityInSecondsForForgotPassword;
    private CorsConfiguration cors;
}
