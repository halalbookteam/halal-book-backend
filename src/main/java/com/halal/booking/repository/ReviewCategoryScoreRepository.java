package com.halal.booking.repository;

import com.halal.booking.model.entity.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.util.List;

public interface ReviewCategoryScoreRepository extends JpaRepository<ReviewCategoryScore, Long> {
    default void loadEvaluationCategoryScores(HotelEvaluation evaluation) {
        List<Object[]> resultList = calculateReviewScores(evaluation.getHotel());
        evaluation.getEvaluationCategoryScores()
                .removeIf(score -> resultList.stream()
                        .noneMatch(result -> score.getCategory().equals(result[0])));
        resultList.forEach(result -> {
            ReviewCategory category = (ReviewCategory) result[0];
            EvaluationCategoryScore evaluationCategoryScore = evaluation.findScoreByCategory(category).orElse(new EvaluationCategoryScore());
            evaluationCategoryScore.setCategory(category);
            evaluationCategoryScore.setScore(new BigDecimal(result[1].toString()));
            evaluationCategoryScore.setCount((Long) result[2]);
            evaluationCategoryScore.setEvaluation(evaluation);
            evaluation.getEvaluationCategoryScores().add(evaluationCategoryScore);
        });
    }

    @Query("select rcs.category, avg(rcs.score), count(rcs) from ReviewCategoryScore rcs " +
            "where rcs.review.status = com.halal.booking.enums.ReviewStatusEnum.ACCEPTED " +
            "and rcs.review.reservation.hotel = :hotel " +
            "group by rcs.category")
    List<Object[]> calculateReviewScores(@Param("hotel") Hotel hotel);
}
