package com.halal.booking.repository;

import com.halal.booking.enums.ReviewStatusEnum;
import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Reservation;
import com.halal.booking.model.entity.Review;
import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Collection;
import java.util.List;

public interface ReviewRepository extends JpaRepository<Review, Long> {
    Review findByReservation(Reservation reservation);
    Review findByReservationAndReservation_User(Reservation reservation, User user);

    Collection<Review> findByStatus(ReviewStatusEnum status);

    Long countByReservation_hotelAndStatus(Hotel hotel, ReviewStatusEnum status);

    List<Review> findByReservation_hotel_idAndStatus(Long hotelId, ReviewStatusEnum status);
}
