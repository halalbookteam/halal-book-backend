package com.halal.booking.repository;

import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.HotelEvaluation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelEvaluationRepository extends JpaRepository<HotelEvaluation, Long> {
    HotelEvaluation findByHotel(Hotel hotel);
}
