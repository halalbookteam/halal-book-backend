package com.halal.booking.repository;

import com.halal.booking.model.entity.FacilityCategory;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacilityCategoryRepository extends JpaRepository<FacilityCategory, Long> {
}
