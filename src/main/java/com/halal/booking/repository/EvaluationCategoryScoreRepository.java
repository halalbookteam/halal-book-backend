package com.halal.booking.repository;

import com.halal.booking.model.entity.EvaluationCategoryScore;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EvaluationCategoryScoreRepository extends JpaRepository<EvaluationCategoryScore, Long> {
}
