package com.halal.booking.repository;

import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomRepository extends JpaRepository<Room, Long> {
    void deleteByHotel(Hotel hotel);
}
