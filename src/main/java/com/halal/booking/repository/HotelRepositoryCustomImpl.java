package com.halal.booking.repository;

import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.model.dto.Place;
import com.halal.booking.model.entity.Hotel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class HotelRepositoryCustomImpl implements HotelRepositoryCustom {
    @PersistenceContext
    private EntityManager em;

    @Override
    public Set<Hotel> search(HotelSearchQuery hotelSearchQuery) {
        Map<String, Object> parameterMap = new HashMap<>();

        StringBuilder queryStrBuilder = new StringBuilder("from Hotel h " +
                "join fetch h.translations ht " +
                "join fetch h.address.translations at " +
                "join fetch h.rooms r " +
                "join fetch r.alternatives a " +
                (hotelSearchQuery.hasCheckInCheckOut() ? "join fetch r.rates ra " : "") +
                "where  h in (select at.address.hotel from AddressTranslation at where at.country = :country ");

        Place place = hotelSearchQuery.getPlace();
        parameterMap.put("country", place.getCountry());
        if(place.getDistrict() != null) {
            queryStrBuilder.append(" and at.district = :district");
            parameterMap.put("district", place.getDistrict());
        }
        if(place.getCity() != null) {
            queryStrBuilder.append(" and at.city = :city");
            parameterMap.put("city", place.getCity());
        }
        if(place.getCounty() != null) {
            queryStrBuilder.append(" and at.county = :county");
            parameterMap.put("county", place.getCounty());
        }

        queryStrBuilder.append(")");

        if(hotelSearchQuery.hasCheckInCheckOut()) {
            queryStrBuilder.append(" and ra.date >= :checkIn and ra.date < :checkOut and ra.availableRoomCount > 0 and ra.minNightCount <= :nightCount");
            parameterMap.put("checkIn", hotelSearchQuery.getCheckIn());
            parameterMap.put("checkOut", hotelSearchQuery.getCheckOut());
            parameterMap.put("nightCount", hotelSearchQuery.getNightCount());
        }

        if(place.getHotelName() != null) {
            queryStrBuilder.append(" and h in (select ht.hotel from HotelTranslation ht where ht.name = :hotelName)");
            parameterMap.put("hotelName", place.getHotelName());
        }

        TypedQuery<Hotel> query = em.createQuery(queryStrBuilder.toString(), Hotel.class);
        parameterMap.keySet().forEach(parameterName -> query.setParameter(parameterName, parameterMap.get(parameterName)));

        return new HashSet<>(query.getResultList());
    }
}
