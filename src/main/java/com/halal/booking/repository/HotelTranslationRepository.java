package com.halal.booking.repository;

import com.halal.booking.model.entity.HotelTranslation;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HotelTranslationRepository extends JpaRepository<HotelTranslation, Long> {
}
