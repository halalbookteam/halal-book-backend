package com.halal.booking.repository;

import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Room;
import com.halal.booking.model.entity.RoomRate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;

public interface RoomRateRepository extends JpaRepository<RoomRate, Long> {

    @Query("select rate from RoomRate rate where rate.room = :room " +
            "and rate.date >= :checkIn and rate.date < :checkOut and minNightCount <= :nightCount and availableRoomCount >= :roomCount")
    List<RoomRate> findRates(@Param("room") Room room, @Param("checkIn") Date checkIn, @Param("checkOut") Date checkOut,
                             @Param("nightCount") int nightCount, @Param("roomCount") int roomCount);

    List<RoomRate> findRatesByRoomAndDateBetween(Room room, Date from, Date to);

    List<RoomRate> findRatesByRoom_HotelAndDateBetweenOrderByDate(Hotel hotel, Date from, Date to);
}
