package com.halal.booking.repository;

import com.halal.booking.model.dto.HotelSearchQuery;
import com.halal.booking.model.entity.Hotel;

import java.util.Set;

public interface HotelRepositoryCustom {
    Set<Hotel> search(HotelSearchQuery hotelSearchQuery);
}
