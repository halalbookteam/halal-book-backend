package com.halal.booking.repository;

import com.halal.booking.enums.ReservationStatusEnum;
import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Reservation;
import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Date;
import java.util.List;
import java.util.Set;

public interface ReservationRepository extends JpaRepository<Reservation, Long> {
    Reservation findByIdAndUser(Long reservationId, User currentUser);

    List<Reservation> findByUserOrderByCheckInDesc(User user);

    List<Reservation> findByHotelInAndStatusIn(Set<Hotel> hotels, List<ReservationStatusEnum> statusList);

    List<Reservation> findByStatusAndCheckOutBefore(ReservationStatusEnum status, Date date);

    @Query("select r from Reservation r " +
            "where r.status = com.halal.booking.enums.ReservationStatusEnum.CONFIRMED " +
            "and r.refundableCharge is null " +
            "and r.checkIn - CURRENT_DATE <= :days "
    )
    List<Reservation> getReservationsTobeAuthorized(@Param("days") Double daysToAuthorize);
}
