package com.halal.booking.repository;

import com.halal.booking.model.entity.Facility;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FacilityRepository extends JpaRepository<Facility, Long> {
    void deleteByCategory_id(Long categoryId);
}
