package com.halal.booking.repository;

import com.halal.booking.model.entity.RoomAlternative;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoomAlternativeRepository extends JpaRepository<RoomAlternative, Long> {
}
