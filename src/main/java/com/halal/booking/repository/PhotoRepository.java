package com.halal.booking.repository;

import com.halal.booking.model.entity.Hotel;
import com.halal.booking.model.entity.Photo;
import com.halal.booking.model.entity.Room;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PhotoRepository extends JpaRepository<Photo, Long> {
    void deleteByHotelAndRoomIsNullAndUrlNotIn(Hotel hotel, List<String> urlList);

    void deleteByHotelAndRoomIsNull(Hotel hotelFromDb);

    void deleteByRoomAndUrlNotIn(Room roomFromDb, List<String> urlList);

    void deleteByHotel(Hotel hotel);
}
