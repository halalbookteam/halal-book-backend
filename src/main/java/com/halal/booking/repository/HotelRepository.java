package com.halal.booking.repository;

import com.halal.booking.enums.ChannelManagerEnum;
import com.halal.booking.model.entity.Hotel;
import com.halal.booking.usermanagement.model.User;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Set;

public interface HotelRepository extends JpaRepository<Hotel, Long>, HotelRepositoryCustom {
    Set<Hotel> findByUsers(User user);

    boolean existsByUsersAndId(User user, Long hotelId);

    boolean existsByUsersAndRooms_id(User user, Long id);

    boolean existsByUsersAndRooms_alternatives_id(User user, Long id);

    Hotel findByIdAndChannelManager(Long id, ChannelManagerEnum channelManager);
}
