package com.halal.booking.config;

import com.halal.booking.security.jwt.JWTConfigurer;
import com.halal.booking.security.jwt.TokenProvider;
import com.halal.booking.usermanagement.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.security.oauth2.client.EnableOAuth2Sso;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.filter.CorsFilter;

import javax.annotation.PostConstruct;
import java.util.Map;
import java.util.Set;

@Configuration
@EnableOAuth2Sso
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final Logger log = LoggerFactory.getLogger(SecurityConfiguration.class);

    private final AuthenticationManagerBuilder authenticationManagerBuilder;

    private final UserDetailsService userDetailsService;

    private final TokenProvider tokenProvider;

    private final CorsFilter corsFilter;

    private final UserService userService;

    @Value("${security.ignoredUrls}")
    private String ignoredUrls;

    @Autowired
    public SecurityConfiguration(AuthenticationManagerBuilder authenticationManagerBuilder, UserDetailsService userDetailsService,
                                 TokenProvider tokenProvider, CorsFilter corsFilter, UserService userService
    ) {
        this.authenticationManagerBuilder = authenticationManagerBuilder;
        this.userDetailsService = userDetailsService;
        this.tokenProvider = tokenProvider;
        this.corsFilter = corsFilter;
        this.userService = userService;
    }

    @PostConstruct
    public void init() {
        try {
            authenticationManagerBuilder
                    .userDetailsService(userDetailsService)
                    .passwordEncoder(passwordEncoder());
        } catch (Exception e) {
            throw new BeanInitializationException("Security configuration failed", e);
        }
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/", "/api/authentication/**")
                .and().ignoring().antMatchers(HttpMethod.GET,
                "/api/facility/categories",
                "/api/store/**",
                "/api/hotel/places",
                "/api/hotel/{\\d+}",
                "/api/hotel/reviews/**",
                "/api/locale/**"
                )
                .and().ignoring().antMatchers(HttpMethod.POST, "/api/reservation/", "/api/reservation/creditCard", "/api/hotel/search");
        if (!ignoredUrls.isEmpty()) {
            web.ignoring().antMatchers("/api/reseliva/**");
            web.ignoring().antMatchers(ignoredUrls.split(","));
        }
    }

    @Override
    @Transactional
    protected void configure(HttpSecurity http) throws Exception {
        Map<String, Set<String>> privilegeRoleListMap = userService.getPrivilegeRoles();
        for (String url : privilegeRoleListMap.keySet()) {
            if (StringUtils.hasText(url)) {
                Set<String> roleNameSet = privilegeRoleListMap.get(url);
                http.authorizeRequests().antMatchers(url).hasAnyRole(roleNameSet.toArray(new String[0]));
            }
        }
        http.authorizeRequests().anyRequest().authenticated();

        http.addFilterBefore(corsFilter, UsernamePasswordAuthenticationFilter.class)
                .csrf().disable().headers().frameOptions().disable()
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().apply(securityConfigurerAdapter());

    }

    private JWTConfigurer securityConfigurerAdapter() {
        return new JWTConfigurer(tokenProvider);
    }
}
