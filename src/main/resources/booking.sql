-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.3.8-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             9.4.0.5125
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping data for table booking.address: ~2 rows (approximately)
/*!40000 ALTER TABLE `address` DISABLE KEYS */;
INSERT INTO `address` (`id`, `city`, `country`, `county`, `district`, `lat`, `lon`, `postalCode`, `street`) VALUES
	(8, 'London', 'United Kingdom', 'asdasd', 'Newham', 51.508588, 0.043548, 'E16 2FQ', '1018 Dockside Road'),
	(11, 'London', 'United Kingdom', NULL, 'Newham', 51.507673, 0.01997, 'E16 1FA', 'Western Gateway'),
	(45, NULL, NULL, NULL, NULL, 0, 0, NULL, NULL);
/*!40000 ALTER TABLE `address` ENABLE KEYS */;

-- Dumping data for table booking.facility: ~46 rows (approximately)
/*!40000 ALTER TABLE `facility` DISABLE KEYS */;
INSERT INTO `facility` (`id`, `description`, `iconUrl`, `name`, `category_id`, `chargeable`) VALUES
	(1, NULL, NULL, 'Aqua aerobik', 1, b'0'),
	(2, NULL, NULL, 'Solaryum', 2, b'0'),
	(3, NULL, NULL, 'Masaj', 2, b'0'),
	(4, NULL, NULL, 'Spa merkezi', 2, b'0'),
	(5, NULL, NULL, 'Türk hamamı', 2, b'0'),
	(6, NULL, NULL, 'Sauna', 2, b'0'),
	(7, NULL, NULL, 'Buhar banyosu', 2, b'0'),
	(8, NULL, NULL, 'Fitnes', 2, b'0'),
	(9, NULL, NULL, 'Kapalı Havuz', 3, b'0'),
	(10, NULL, NULL, 'Açık Havuz', 3, b'0'),
	(11, NULL, NULL, 'Restoran', 4, b'0'),
	(12, NULL, NULL, 'Restoran (alakart)', 4, b'0'),
	(13, NULL, NULL, 'Restoran (büfe)', 4, b'0'),
	(14, NULL, NULL, 'Kafe', 4, b'0'),
	(15, NULL, NULL, 'Snack bar', 4, b'0'),
	(16, NULL, NULL, 'Çocuk dostu büfe', 4, b'0'),
	(17, NULL, NULL, 'Çocuklar için menüler', 4, b'0'),
	(18, NULL, NULL, 'Odaya kahvaltı servisi', 4, b'0'),
	(19, NULL, NULL, 'Oda servisi', 4, b'0'),
	(20, NULL, NULL, 'Wi-Fi', 5, b'0'),
	(21, NULL, NULL, 'PARK YERİ', 5, b'0'),
	(22, NULL, NULL, 'Kuaför/güzellik salonu', 5, b'0'),
	(23, NULL, NULL, 'Konsiyerj hizmeti', 5, b'0'),
	(24, NULL, NULL, 'Döviz alım/satım', 5, b'0'),
	(25, NULL, NULL, 'Tesis içi ATM/bankamatik', 5, b'0'),
	(26, NULL, NULL, 'Kilitli dolaplar', 5, b'0'),
	(27, NULL, NULL, 'Emanet kasası', 5, b'0'),
	(28, NULL, NULL, 'Bahçe', 5, b'0'),
	(29, NULL, NULL, 'Teras', 5, b'0'),
	(30, NULL, NULL, 'Klima', 5, b'0'),
	(31, NULL, NULL, 'Isıtma', 5, b'0'),
	(32, NULL, NULL, 'Ortak salon/TV alanı', 5, b'0'),
	(33, NULL, NULL, 'Engelli misafirler için olanaklar', 5, b'0'),
	(34, NULL, NULL, 'Asansör', 5, b'0'),
	(35, NULL, NULL, 'Sigara içilmeyen odalar', 5, b'0'),
	(36, NULL, NULL, 'Günlük temizlik hizmeti', 5, b'0'),
	(37, NULL, NULL, 'Havaalanı özel transfer', 5, b'1'),
	(38, NULL, NULL, 'Araba kiralama', 5, b'1'),
	(39, NULL, NULL, 'Bisiklet kiralama', 5, b'1'),
	(40, 'Tesisteki tüm yiyecekler helaldir', NULL, 'Tüm Yiyecekler', 6, b'0'),
	(41, 'Tesisteki bazı menü seçenekleri helaldir', NULL, 'Bazı Yiyecekler', 6, b'0'),
	(42, 'Tesisin en fazla 500m uzaklığında bulunan restoran/​kafe/​marketlerde helal yiyecekler bulunmaktadır', NULL, 'Yakınlarda', 6, b'0'),
	(43, 'Önceden talep edildiği taktirde helal yiyecek seçenekleri bulunmaktadır', NULL, 'Önceden Talep ile', 6, b'0'),
	(44, 'Tesis sınırları içinde alkol servisi yapılmaz', NULL, 'Bütün Tesiste', 7, b'0'),
	(45, 'Tesiste bulunan bazı restoranlar (en az bir tanesi) alkolsüzdür', NULL, 'Bazı Resteronlarda', 7, b'0'),
	(46, 'Bu web sitesinden yapılan rezervasyonlarda odalarınızda alkol bulunmaz', NULL, 'Odanızda', 7, b'0'),
	(47, 'Tesiste plaj bölümünde sadece bayanlara özel %100 korunaklı yüzme alanı bulunmaktadır', NULL, 'Plajda Yüzme', 8, b'0'),
	(48, 'Tesiste plaj bölümünde sadece bayanlara özel %100 korunaklı güneşlenme alanı bulunmaktadır', NULL, 'Plajda Güneşlenme', 8, b'0'),
	(49, 'Tesiste sadece bayanlara veya ailenize/grubunuza özel %100 korunaklı spa merkezi bulunmaktadır (tüm gün veya belirli saatlerde)', NULL, 'Spa Merkezi', 8, b'0'),
	(50, 'Tesiste sadece bayanlara veya ailenize/grubunuza özel %100 korunaklı açık havuz(lar) bulunmaktadır (tüm gün veya belirli saatlerde)', NULL, 'Açık Havuz', 8, b'0'),
	(51, 'Tesiste sadece bayanlara veya ailenize/grubunuza özel %100 korunaklı kapalı havuz(lar) bulunmaktadır (tüm gün veya belirli saatlerde)', NULL, 'Kapalı Havuz', 8, b'0'),
	(52, 'Bazı odalar veya villada ailenize/grubunuza özel %100 korunaklı havuz (kapalı veya açık) bulunmaktadır', NULL, 'Havuzlu Oda/Villa', 8, b'0'),
	(53, NULL, NULL, 'Tea/Coffee Maker', 9, b'0'),
	(54, NULL, NULL, 'Shower', 9, b'0'),
	(55, NULL, NULL, 'Safety Deposit Box', 9, b'0'),
	(56, NULL, NULL, 'Pay-per-view Channels', 9, b'0'),
	(57, NULL, NULL, 'TV', 9, b'0'),
	(58, NULL, NULL, 'Telephone', 9, b'0'),
	(59, NULL, NULL, 'Air conditioning', 9, b'0'),
	(60, NULL, NULL, 'Hairdryer', 9, b'0'),
	(61, NULL, NULL, 'Iron', 9, b'0'),
	(62, NULL, NULL, 'Ironing Facilities', 10, b'0'),
	(63, NULL, NULL, 'Free toiletries', 9, b'0'),
	(64, NULL, NULL, 'Toilet', 9, b'0'),
	(65, NULL, NULL, 'Private bathroom', 9, b'0'),
	(66, NULL, NULL, 'Heating', 9, b'0'),
	(67, NULL, NULL, 'Satellite Channels', 9, b'0'),
	(68, NULL, NULL, 'Bath or Shower', 9, b'0'),
	(69, NULL, NULL, 'Laptop safe', 9, b'0'),
	(70, NULL, NULL, 'Flat-screen TV', 9, b'0'),
	(71, NULL, NULL, 'Soundproofing', 9, b'0'),
	(72, NULL, NULL, 'Wake-up service', 10, b'0'),
	(73, NULL, NULL, 'Upper floors accessible by lift', 10, b'0'),
	(74, NULL, NULL, 'Entire unit wheelchair accessible', 10, b'0'),
	(75, NULL, NULL, 'Free wi-fi', 10, b'0'),
	(76, NULL, NULL, 'Toilet paper', 9, b'0');
/*!40000 ALTER TABLE `facility` ENABLE KEYS */;

-- Dumping data for table booking.facility_category: ~8 rows (approximately)
/*!40000 ALTER TABLE `facility_category` DISABLE KEYS */;
INSERT INTO `facility_category` (`id`, `description`, `halal`, `name`, `type`, `filter`) VALUES
	(1, NULL, b'0', 'ETKİNLİKLER', 'HOTEL', b'0'),
	(2, NULL, b'0', 'SAĞLIK & SPA', 'HOTEL', b'0'),
	(3, NULL, b'0', 'HAVUZLAR', 'HOTEL', b'0'),
	(4, NULL, b'0', 'YİYECEK & İÇECEK', 'HOTEL', b'0'),
	(5, NULL, b'0', 'GENEL TESİS OLANAKLARI', 'HOTEL', b'0'),
	(6, NULL, b'1', 'HELAL YİYECEKLER', 'HOTEL', b'1'),
	(7, NULL, b'1', 'ALKOLSÜZLÜK KURALI', 'HOTEL', b'1'),
	(8, NULL, b'1', 'BAYANLARA ÖZEL', 'HOTEL', b'1'),
	(9, NULL, b'0', 'İMKANLAR', 'ROOM', b'0'),
	(10, NULL, b'0', 'HİZMETLER', 'ROOM', b'0');
/*!40000 ALTER TABLE `facility_category` ENABLE KEYS */;

-- Dumping data for table booking.hotel: ~2 rows (approximately)
/*!40000 ALTER TABLE `hotel` DISABLE KEYS */;
INSERT INTO `hotel` (`id`, `description`, `name`, `primaryPhotoIndex`, `star`, `address_id`) VALUES
	(1, 'Good Hotel London is a profit for non-profit floating hotel with a waterfront location and rooftop terrace offering panoramic views. It is located within a 10-minute walk from the ExCeL Convention Centre and 2.9 km from London City Airport and Canary Whar', 'Good Hotel Londonn', 0, 4, 11),
	(4, 'Located in the heart of London\'s Docklands, this Holiday Inn Express is a 7-minute walk from the ExCel and a 1.8 km from London City Airport. High-speed free WiFi is accessible throughout, and there are meeting facilities on site.\n\nHoliday Inn Express Lon', 'Holiday Inn Express London - ExCel', 1, 3, 8),
	(38, NULL, 'aaa', NULL, NULL, 45);
/*!40000 ALTER TABLE `hotel` ENABLE KEYS */;

-- Dumping data for table booking.hotels_facilities: ~0 rows (approximately)
/*!40000 ALTER TABLE `hotels_facilities` DISABLE KEYS */;
INSERT INTO `hotels_facilities` (`hotel_id`, `facility_id`) VALUES
	(4, 1),
	(4, 12);
/*!40000 ALTER TABLE `hotels_facilities` ENABLE KEYS */;

-- Dumping data for table booking.hotels_users: ~2 rows (approximately)
/*!40000 ALTER TABLE `hotels_users` DISABLE KEYS */;
INSERT INTO `hotels_users` (`hotel_id`, `user_id`) VALUES
	(4, 2),
	(1, 2),
	(38, 2);
/*!40000 ALTER TABLE `hotels_users` ENABLE KEYS */;

-- Dumping data for table booking.photo: ~0 rows (approximately)
/*!40000 ALTER TABLE `photo` DISABLE KEYS */;
INSERT INTO `photo` (`id`, `url`, `hotel_id`, `room_id`) VALUES
	(8, 'api/store/file/b374ee06-236f-4b29-9557-e78694090fcb', 4, NULL),
	(9, 'api/store/file/1a5140b6-9b94-41b9-b9c0-dab5757dfaac', 4, NULL),
	(13, 'api/store/file/e59a72ce-a8b4-4308-bf94-c1c9e0deac66', 4, 26);
/*!40000 ALTER TABLE `photo` ENABLE KEYS */;

-- Dumping data for table booking.privilege: ~1 rows (approximately)
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
INSERT INTO `privilege` (`id`, `name`, `url`) VALUES
	(1, 'HOTEL_CRUD', NULL);
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;

-- Dumping data for table booking.role: ~4 rows (approximately)
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
INSERT INTO `role` (`id`, `description`, `name`) VALUES
	(1, 'ROLE_ADMIN', 'ROLE_ADMIN'),
	(2, 'HOTEL_OWNER', 'HOTEL_OWNER'),
	(3, 'Basic User', 'BASIC'),
	(4, 'Administration', 'ADMIN');
/*!40000 ALTER TABLE `role` ENABLE KEYS */;

-- Dumping data for table booking.roles_privileges: ~1 rows (approximately)
/*!40000 ALTER TABLE `roles_privileges` DISABLE KEYS */;
INSERT INTO `roles_privileges` (`role_id`, `privilege_id`) VALUES
	(2, 1);
/*!40000 ALTER TABLE `roles_privileges` ENABLE KEYS */;

-- Dumping data for table booking.room: ~0 rows (approximately)
/*!40000 ALTER TABLE `room` DISABLE KEYS */;
INSERT INTO `room` (`id`, `description`, `personCount`, `primaryPhotoIndex`, `type`, `hotel_id`, `size`) VALUES
	(26, '4', 3, NULL, '1', 4, 2),
	(41, NULL, 0, NULL, 'www', 38, 0),
	(42, NULL, 0, NULL, 'ffff', 38, 0);
/*!40000 ALTER TABLE `room` ENABLE KEYS */;

-- Dumping data for table booking.rooms_facilities: ~0 rows (approximately)
/*!40000 ALTER TABLE `rooms_facilities` DISABLE KEYS */;
INSERT INTO `rooms_facilities` (`room_id`, `facility_id`) VALUES
	(41, 62);
/*!40000 ALTER TABLE `rooms_facilities` ENABLE KEYS */;

-- Dumping data for table booking.user: ~2 rows (approximately)
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` (`id`, `active`, `email`, `gsmNo`, `name`, `passwordHash`, `surname`) VALUES
	(1, b'1', 'serhatyenican@gmail.com', '1', 'srt', '$2a$10$NDgW6dtY9hpW6VxOvueb6e7wKkVFS3X6DiiJ/k7qLxcmlCJ3KHQuu', 'ync'),
	(2, b'1', 'owner@gmail.com', '2', 'hotel', '$2a$10$NDgW6dtY9hpW6VxOvueb6e7wKkVFS3X6DiiJ/k7qLxcmlCJ3KHQuu', 'owner');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;

-- Dumping data for table booking.users_roles: ~2 rows (approximately)
/*!40000 ALTER TABLE `users_roles` DISABLE KEYS */;
INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
	(1, 1),
	(2, 2);
/*!40000 ALTER TABLE `users_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
