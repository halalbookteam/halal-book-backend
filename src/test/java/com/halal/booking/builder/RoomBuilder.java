package com.halal.booking.builder;

import com.halal.booking.model.dto.Price;
import com.halal.booking.model.entity.Room;
import com.halal.booking.model.entity.RoomAlternative;

import java.math.BigDecimal;
import java.util.HashSet;

public class RoomBuilder {
    private Room instance;

    public RoomBuilder() {
        this.instance = new Room();
        this.instance.setAlternatives(new HashSet<>());
    }

    public RoomBuilder baseAmount(BigDecimal amount) {
        instance.setTotalBasePrice(Price.builder().amount(amount).currency("$").build());
        return this;
    }

    public RoomBuilder availableCount(int availableCount) {
        instance.setAvailableCount(availableCount);
        return this;
    }

    public RoomBuilder id(long id) {
        instance.setId(id);
        return this;
    }

    public RoomBuilder addAlternative(long id, BigDecimal ratio, int adultCount, int childCount) {
        RoomAlternative alternative = new RoomAlternative();
        alternative.setRoom(instance);
        alternative.setId(id);
        alternative.setBasePriceRatio(ratio);
        alternative.setPrice(this.instance.getTotalBasePrice().multiply(ratio));
        alternative.setMaxAdult(adultCount);
        alternative.setMaxChild(childCount);
        instance.getAlternatives().add(alternative);
        return this;
    }

    public Room build() {
        return instance;
    }
}
