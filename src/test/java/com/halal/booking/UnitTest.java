package com.halal.booking;

import org.junit.Test;

import java.util.Currency;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

public class UnitTest {
    @Test
    public void asd() {
//        System.out.println(getAllCurrencies());
        System.out.println(Currency.getInstance("GBP").getDisplayName());
    }

    public static Set<Currency> getAllCurrencies()
    {
        Set<Currency> toret = new HashSet<Currency>();
        Locale[] locs = Locale.getAvailableLocales();

        for(Locale loc : locs) {
            try {
                Currency currency = Currency.getInstance( loc );

                if ( currency != null ) {
                    toret.add( currency );
                }
            } catch(Exception exc)
            {
                System.out.println(loc + " not found");
            }
        }

        return toret;
    }
}
