package com.halal.booking.model.entity;

import com.halal.booking.builder.RoomBuilder;
import com.halal.booking.model.dto.HotelSearchQuery;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Map;
import java.util.stream.Collectors;

public class HotelSearchTestForAdults {

    private Hotel hotel;

    @Before
    public void before() {
        HashSet<Room> rooms = new HashSet<>();
        rooms.add(new RoomBuilder().id(1L).baseAmount(new BigDecimal("45")).availableCount(3)
                .addAlternative(11L, new BigDecimal("0.9"), 1, 0)
                .addAlternative(12L, new BigDecimal("1"), 1, 0)
                .build());
        rooms.add(new RoomBuilder().id(2L).baseAmount(new BigDecimal("55")).availableCount(3)
                .addAlternative(21L, new BigDecimal("0.8"), 1, 0)
                .addAlternative(22L, new BigDecimal("0.9"), 1, 0)
                .addAlternative(23L, new BigDecimal("0.9"), 2, 0)
                .addAlternative(24L, new BigDecimal("1"), 2, 0)
                .build());
        rooms.add(new RoomBuilder().id(3L).baseAmount(new BigDecimal("80")).availableCount(3)
                .addAlternative(31L, new BigDecimal("0.9"), 3, 0)
                .addAlternative(32L, new BigDecimal("1"), 3, 0)
                .build());
        rooms.add(new RoomBuilder().id(4L).baseAmount(new BigDecimal("100")).availableCount(3)
                .addAlternative(41L, new BigDecimal("0.9"), 4, 0)
                .addAlternative(42L, new BigDecimal("1"), 4, 0)
                .build());
        hotel = new Hotel();
        hotel.setRooms(rooms);
    }

    @Test
    public void prepareSearchForSingleRoom() {
        HotelSearchQuery hotelSearchQuery = HotelSearchQuery.builder().adultCount(1).childCount(0).roomCount(1).build();
        hotel.prepareSearch(hotelSearchQuery);
        Map<Long, Integer> map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(11L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(2).childCount(0).roomCount(1).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(23L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(3).childCount(0).roomCount(1).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(31L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(4).childCount(0).roomCount(1).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(41L).intValue(), 1);
    }

    @Test
    public void prepareSearchFor2Rooms() {
        HotelSearchQuery hotelSearchQuery = HotelSearchQuery.builder().adultCount(2).childCount(0).roomCount(2).build();
        hotel.prepareSearch(hotelSearchQuery);
        Map<Long, Integer> map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(11L).intValue(), 2);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(3).childCount(0).roomCount(2).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(11L).intValue(), 1);
        Assert.assertEquals(map.get(23L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(4).childCount(0).roomCount(2).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(23L).intValue(), 2);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(5).childCount(0).roomCount(2).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(23L).intValue(), 1);
        Assert.assertEquals(map.get(31L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(6).childCount(0).roomCount(2).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(23L).intValue(), 1);
        Assert.assertEquals(map.get(41L).intValue(), 1);
    }

    private Map<Long, Integer> calculateMap() {
        Map<RoomAlternative, Integer> map = hotel.getRecommendedRooms().getRoomAlternativeCountMap();
        return map.keySet().stream()
                .collect(Collectors.toMap(AbstractModel::getId, map::get));
    }

    @Test
    public void prepareSearchFor3Rooms() {
        HotelSearchQuery hotelSearchQuery = HotelSearchQuery.builder().adultCount(4).childCount(0).roomCount(3).build();
        hotel.prepareSearch(hotelSearchQuery);
        Map<Long, Integer> map = calculateMap();
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(11L).intValue(), 2);
        Assert.assertEquals(map.get(23L).intValue(), 1);

        hotelSearchQuery = HotelSearchQuery.builder().adultCount(6).childCount(0).roomCount(3).build();
        hotel.prepareSearch(hotelSearchQuery);
        map = calculateMap();
        Assert.assertEquals(map.size(), 1);
        Assert.assertEquals(map.get(23L).intValue(), 3);
    }

    @Test
    public void prepareSearchFor3Rooms_2availableForSingleRoom() {
        hotel.getRooms().stream().filter(room -> room.getId().equals(1L)).findFirst().ifPresent(room -> room.setAvailableCount(2));

        HotelSearchQuery hotelSearchQuery = HotelSearchQuery.builder().adultCount(3).childCount(0).roomCount(3).build();
        hotel.prepareSearch(hotelSearchQuery);
        Map<Long, Integer> map = calculateMap();
        Assert.assertEquals(map.size(), 2);
        Assert.assertEquals(map.get(11L).intValue(), 2);
        Assert.assertEquals(map.get(21L).intValue(), 1);
    }
}