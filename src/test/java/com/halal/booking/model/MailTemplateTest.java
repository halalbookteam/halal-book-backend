package com.halal.booking.model;

import com.halal.booking.model.dto.MailTemplate;
import org.junit.Test;

import java.io.File;
import java.net.URISyntaxException;

public class MailTemplateTest {
    @Test
    public void testTemplate() throws URISyntaxException {
        MailTemplate mailTemplate = MailTemplate.builder().file(new File(MailTemplateTest.class.getResource("/templates/standard.html").toURI())).build();
        System.out.println(mailTemplate);
    }
}